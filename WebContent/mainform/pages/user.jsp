<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | User</title>
<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="User" name="User" action = "${pageContext.request.contextPath}/user" method="post">

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">User</label>
	</h1>
	</section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
											<c:if test="${condition == 'SuccessInsertUser'}">
									<div class="alert alert-success alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-check"></i> Success
										</h4>
										Insert Success.
									</div>
								</c:if>
<%-- 								<c:if test="${condition == 'FailedInsert'}"> --%>
<!-- 									<div class="alert alert-danger alert-dismissible"> -->
<!-- 										<button type="button" class="close" data-dismiss="alert" -->
<!-- 											aria-hidden="true">&times;</button> -->
<!-- 										<h4> -->
<!-- 											<i class="icon fa fa-ban"></i> Failed -->
<!-- 										</h4> -->
<!-- 										Insert Failed. -->
<%-- 										<c:out value="${errorDescription}" /> --%>
<!-- 										. -->
<!-- 									</div> -->
<%-- 								</c:if> --%>
								<c:if test="${condition == 'SuccessUpdateUser'}">
									<div class="alert alert-success alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-check"></i> Success
										</h4>
										Update Success.
									</div>
								</c:if>
<%-- 								<c:if test="${condition == 'FailedUpdate'}"> --%>
<!-- 									<div class="alert alert-danger alert-dismissible"> -->
<!-- 										<button type="button" class="close" data-dismiss="alert" -->
<!-- 											aria-hidden="true">&times;</button> -->
<!-- 										<h4> -->
<!-- 											<i class="icon fa fa-ban"></i> Failed -->
<!-- 										</h4> -->
<!-- 										Update Failed. -->
<%-- 										<c:out value="${errorDescription}" /> --%>
<!-- 										. -->
<!-- 									</div> -->
<%-- 								</c:if> --%>
								<c:if test="${condition == 'SuccessDelete'}">
									<div class="alert alert-success alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-check"></i> Success
										</h4>
										Delete Success.
									</div>
								</c:if>
								<c:if test="${condition == 'FailedDelete'}">
									<div class="alert alert-danger alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-ban"></i> Failed
										</h4>
										Delete Failed.
										<c:out value="${errorDescription}" />
										.
									</div>
								</c:if>
					
					<!-- modal update insert User -->
					<div class="modal fade" id="ModalUpdateInsert"  role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							
							<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
	     					</div>
	     					
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalUpdateInsert" name="lblTitleModalUpdateInsert"></label></h4>
								</div>
   								<div class="modal-body">
   								
   								<div class="panel panel-primary" id="DropDownPanel" style="width: 200px;">
   								<div class="panel-body fixed-panel">
   									<div id="dvDropDown" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">SELECT</label><label id="mrkDropDown" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slDropDown" name="slDropDown" class="form-control" onchange="FuncShowUserPanel()">
							        		<option value="admin">Admin</option>
							        		<option value="merchant">Merchant</option>
							        	</select>
       								</div>
       							</div>
       							</div>
       							<!-- /.DropDownPanel -->
       								
       								<div class="panel panel-primary" id="UserPanel">
   									<div class="panel-body fixed-panel">
   								
       								<div id="dvUserID" class="form-group col-xs-12">
	         							<label for="recipient-name" class="control-label">USER ID</label><label id="mrkUserID" for="recipient-name" class="control-label"><small>*</small></label>
         								<input type="text" class="form-control" id="txtUserID" name="txtUserID">
         								
         								<select id="slUserID" name="slUserID" class="form-control select2" data-placeholder="Select Merchant Id" style="width: 100%;">
										<c:forEach items="${listmerchant}" var="merchant">
											<option value="<c:out value="${merchant.merchantID}" />"><c:out value="${merchant.merchantName}" /></option>
										</c:forEach>
							        	</select>
       								</div>
       								<div id="dvUserName" class="form-group col-xs-12">
	         							<label for="recipient-name" class="control-label">USERNAME</label><label id="mrkUserName" for="recipient-name" class="control-label"><small>*</small></label>
         								<input type="text" class="form-control" id="txtUserName" name="txtUserName">
       								</div>
       								<div id="dvPassword" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">PASSWORD</label><label id="mrkPassword" for="recipient-name" class="control-label"><small>*</small></label>	
         								<input type="password" class="form-control" id="txtPassword" name="txtPassword">
       								</div>
       								<div id="dvRePassword" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">RE-PASSWORD</label><label id="mrkRePassword" for="recipient-name" class="control-label"><small>*</small></label>	
         								<input type="password" class="form-control" id="txtRePassword" name="txtRePassword">
       								</div>
       								
       								<div id="dvBrand" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">BRAND</label><label id="mrkBrand" for="recipient-name" class="control-label"> <small>*</small></label>	       								
<!--          								<div class="panel panel-primary" id="BrandPanel" style="overflow: auto;	height: 95px;"> -->
<!-- 										<div class="panel-body "> -->
										
<%--          								<c:forEach items="${listbrand}" var="brand"> --%>
<%-- 											<div class="checkbox" id="cbbrand"><label><input type="checkbox" id="<c:out value="${brand.brandID}" />" name="<c:out value="${brand.brandID}" />" value="<c:out value="${brand.brandID}" />"><c:out value="${brand.brandName}" /></label></div> --%>
<%-- 										</c:forEach> --%>
<!-- 										</div> -->
<!-- 										</div>	 -->
										<select id="slBrand" name="slBrand"
														class="form-control select2" multiple="multiple"
														data-placeholder="Select Brand" style="width: 100%;">
														<c:forEach items="${listbrand}" var="brand">
															<option id="optbrand" name="optbrand"
																value="<c:out value="${brand.brandID}" />"><c:out
																	value="${brand.brandID}" /> - <c:out
																	value="${brand.brandName}" /></option>
														</c:forEach>
										</select>					        	
       								</div>

       								<div id="dvArea" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">AREA ID</label><label id="mrkArea" for="recipient-name" class="control-label"> <small>*</small></label>	       								
<!--          								<div class="panel panel-primary" id="AreaPanel" style="overflow: auto;	height: 95px;"> -->
<!-- 										<div class="panel-body fixed-panel"> -->
<%--          								<c:forEach items="${listarea}" var="area"> --%>
<%-- 											<div class="checkbox" id="cbarea" ><label><input type="checkbox" id="<c:out value="${area.areaID}" />" name="<c:out value="${area.areaID}" />" value="<c:out value="${area.areaID}" />"><c:out value="${area.areaName}" /></label></div> --%>
<%-- 										</c:forEach> --%>
<!-- 										</div> -->
<!-- 										</div> -->
										<select id="slArea" name="slArea"
														class="form-control select2" multiple="multiple"
														data-placeholder="Select Area" style="width: 100%;">
														<c:forEach items="${listarea}" var="area">
															<option id="optarea" name="optarea"
																value="<c:out value="${area.areaID}" />"><c:out
																	value="${area.areaID}" /> - <c:out
																	value="${area.areaName}" /></option>
														</c:forEach>
										</select>					        	
       								</div>

       								<div id="dvRole" class="form-group col-xs-12">
         								<label >ROLE ID</label><label id="mrkRole" for="recipient-name" class="control-label"> <small>*</small></label>
         								<select id="slRole" name="slRole" class="form-control select2" data-placeholder="Select Role" style="width: 100%;">
										<c:forEach items="${listrole}" var="role">
											<option value="<c:out value="${role.roleID}" />"><c:out value="${role.roleID}" /> - <c:out value="${role.roleName}" /></option>
										</c:forEach>
							        	</select>
       								</div>
       								
									<!-- <div class="form-group col-xs-12"> -->
									<!-- </div> -->
       								
       								</div>
									</div>
									<!-- /.UserPanel -->

       								<div class="row"></div>	        								
  								</div>
  								
  								<div class="modal-footer">
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
    									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end modal update & Insert -->
					
					<!--modal Delete -->
									<div class="example-modal">
       									<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          									<div class="modal-dialog" role="document">
           										<div class="modal-content">
              										<div class="modal-header">
                										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  										<span aria-hidden="true">&times;</span></button>
                										<h4 class="modal-title">Alert Delete User</h4>
              										</div>
              									<div class="modal-body">
              									<input type="hidden" id="temp_txtUserId" name="temp_txtUserId" >
               	 									<p>Are you sure to delete this User document ?</p>
              									</div>
								              <div class="modal-footer">
								                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
								                <button <c:out value="${buttonstatus}"/> type="submit" id="btnDelete" name="btnDelete" class="btn btn-outline" >Delete</button>
								              </div>
								            </div>
								            <!-- /.modal-content -->
								          </div>
								          <!-- /.modal-dialog -->
								        </div>
								        <!-- /.modal -->
								      </div>
								    <!--modal Delete -->
					
					<div class="col-xs-12">
					<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-gg-small pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> ADD NEW</button>
					</div>
					<div class="col-xs-12"><br></div>	
					<div class="col-xs-12">
					<table id="tb_user" class="table table-bordered table-striped table-hover">
								<thead style="color:#7d0d08;">
									<!-- background-color: #d2d6de; -->
									<tr>
										<th>User Id</th>
										<th>UserName</th>
										<th>Password</th>
										<th>Role</th>
										<th>Area</th>
										<th>Brand</th>
										<th></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listuser}" var="user">
										<tr>
											<td>
											<a href="" style="color:#144471;font-weight: bold;" 
											   onclick="FuncButtonUpdate()"
											   data-toggle="modal" 
											   data-target="#ModalUpdateInsert" 
											   data-luserid='<c:out value="${user.userId}" />'
											   data-lusername='<c:out value="${user.username}" />'
											   data-lpassword='<c:out value="${user.password}" />'
											   data-lrole='<c:out value="${user.role}" />'
											   data-larea='<c:out value="${user.area}" />'
											   data-lbrand='<c:out value="${user.brand}" />'
											   data-ltype='<c:out value="${user.type}" />'>
											<c:out value="${user.userId}" />
											</a>	
											</td>
											<td><c:out value="${user.username}" /></td>
											<td><c:out value="${user.password}" /></td>
											<td><c:out value="${user.role}" /></td>
											<td><c:out value="${user.area}" /></td>
											<td><c:out value="${user.brand}" /></td>
											<td><a href="" 
												   data-toggle="modal" 
												   data-target="#ModalDelete" 
												   data-luserid='<c:out value="${user.userId}" />'
												>Delete</a></td>
										</tr>

									</c:forEach>						
								</tbody>
							</table> 
					</div>
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
 	$(function () {
 		//Initialize Select2 Elements
		$(".select2").select2();
 		
 		$("#tb_user").DataTable({"scrollX": true});
  		$('#M004').addClass('active');
  		//shortcut for button 'new'
  	    Mousetrap.bind('n', function() {
  	    	FuncButtonNew(),
  	    	$('#ModalUpdateInsert').modal('show')
  	    	});
  	  $("#dvErrorAlert").hide();
  	  $('#slUserID').next(".select2-container").hide();
  	 
  	  $("#UserPanel :input").prop("disabled", true);
  	});
 
 	function FuncClear(){
 		$('#mrkUserID').hide();
 		$('#mrkUserName').hide();
 		$('#mrkPassword').hide();
 		$('#mrkRePassword').hide();
 		$('#mrkBrand').hide();
 		$('#mrkArea').hide();
 		$('#mrkRole').hide();
 		$('#mrkDropDown').hide();
 	
 		
 		$('#dvUserID').removeClass('has-error');
 		$('#dvUserName').removeClass('has-error');
 		$('#dvPassword').removeClass('has-error');
 		$('#dvRePassword').removeClass('has-error');
 		$('#dvBrand').removeClass('has-error');
 		$('#dvArea').removeClass('has-error');
 		$('#dvRole').removeClass('has-error');
 		$('#dvDropDown').removeClass('has-error');
 	
 	}
 	
 	function FuncButtonNew() {
 		$('#slDropDown').val('');
 		$('#txtUserID').val('');
 		$('#txtUserName').val('');
 		$('#txtPassword').val('');
 		$('#txtRePassword').val('');
 		$('#slUserID').val('').trigger("change");
 		$('#slRole').val('').trigger("change");
// 		$('#slRole').val($('#slRole option:first-child').val()).trigger("change");
 		$('#slBrand').val(null).trigger("change");
 		$('#slArea').val(null).trigger("change");
 		
//  		$('#txtUserID').prop('disabled', false); nandes
 		
 		$('#btnSave').show();
 		$('#btnUpdate').hide();
 		document.getElementById("lblTitleModalUpdateInsert").innerHTML = "USER - ADD NEW";
 		$('#slDropDown').prop('disabled', false);
 		$("#UserPanel :input").prop("disabled", true);

 		FuncClear();
 	}
 	
 	function FuncButtonUpdate() {
 		$('#btnSave').hide();
 		$('#btnUpdate').show();
 		document.getElementById("lblTitleModalUpdateInsert").innerHTML = 'USER - EDIT';

 		FuncClear();
 		$('#slDropDown').prop('disabled', true);
 		$("#UserPanel :input").prop("disabled", false);
 		
 		$('#txtUserID').prop('disabled', true);
 		$('#slUserID').prop('disabled', true);
 	}
 	
 	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {

 		
 		$("#dvErrorAlert").hide();
 		
 		var button = $(event.relatedTarget);
 		var luserid = button.data('luserid');
 		var lusername = button.data('lusername');
 		var lpassword = button.data('lpassword');
 		var lrole = button.data('lrole');
 		var lbrand = button.data('lbrand');
 		var ltype = button.data('ltype');
 		var listbrand = new Array();
 		var modal = $(this);
 		
 		modal.find(".modal-body #slDropDown").val(ltype);
 		modal.find(".modal-body #txtPassword").val(atob(unescape(encodeURIComponent(lpassword))));
 		modal.find(".modal-body #txtRePassword").val(atob(unescape(encodeURIComponent(lpassword))));
 		
 		if(ltype=='admin')
 			{
 				modal.find(".modal-body #txtUserID").val(luserid);
 				$('#slUserID').next(".select2-container").hide();
 				$('#txtUserID').show();
 			}
 		else
 			{
 				$("#slUserID").val(luserid).trigger('change.select2');
 				$('#slUserID').next(".select2-container").show();
 				$('#txtUserID').hide();
 			}
 		
 		modal.find(".modal-body #txtUserName").val(lusername);
// 		modal.find(".modal-body #slRole").val(lrole);
 		
 		$('#slBrand').val(null).trigger("change");
//  		$("#BrandPanel :input").prop("checked", false);
 		
 		$('#slArea').val(null).trigger("change");
//  		$("#AreaPanel :input").prop("checked", false);
 		
 		
 		if(luserid == null || luserid == '')
 			$('#slDropDown').focus();
 		else
 		{
 			$('#txtUserName').focus();
 			
 			 
 			
//  			$.each(lbrandlist,function(i){			
//  			modal.find(".modal-body #"+lbrandlist[i].toString()).prop('checked', true);
 		
//  			});
 			
 			//select2 Brand
 			var lbrandlist = button.data('lbrand').toString().split(",");
 			$.each(lbrandlist,function(i){			
 				listbrand.push(lbrandlist[i].toString());
 	 		});
 			$('#slRole').val(lrole).trigger('change.select2');
 			
 			$('#slBrand').val(lbrandlist).trigger('change.select2');
 			
//  			$.each(larealist,function(i){
//  				modal.find(".modal-body #"+larealist[i].toString()).prop('checked', true);	
//  			});

 			var larealist = button.data('larea').toString().split(",");
 			$.each(larealist,function(i){			
 				larealist.push(larealist[i].toString());
 	 		});
 			$('#slArea').val(larealist).trigger('change.select2');
 			
 		}	
 	})
 		
 	
	$('#ModalDelete').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var luserid = button.data('luserid');
		$("#temp_txtUserId").val(luserid);
	})
	
 		
 	function FuncValEmptyInput(lParambtn) {
 		FuncClear();
 		var DropDown = document.getElementById('slDropDown').value;
 		var txtUserName = document.getElementById('txtUserName').value;
		var txtPassword = document.getElementById('txtPassword').value;
		var txtRePassword = document.getElementById('txtRePassword').value;
		var slRole = document.getElementById('slRole').value;
		var slBrandId = $("#slBrand").val();
		var slAreaId = $("#slArea").val();
 		
		if(!DropDown.match(/\S/)) {
    		$("#slDropDown").focus();
   		 	$('#dvDropDown').addClass('has-error');
    		$('#mrkDropDown').show();
     	   return false;
    	}
		
 		var txtUserID = "";
 		if(DropDown=='admin')
 			{
 				txtUserID = document.getElementById('txtUserID').value;
 				if(!txtUserID.match(/\S/)) {
 		    		$("#txtUserID").focus();
 		   		 	$('#dvUserID').addClass('has-error');
 		    		$('#mrkUserID').show();
 		     	   return false;
 		    	} 
 			}
 		else
 			{
 				txtUserID = document.getElementById('slUserID').value;
 				if(!txtUserID.match(/\S/)) {
 		    		$("#slUserID").focus();
 		   		 	$('#dvUserID').addClass('has-error');
 		    		$('#mrkUserID').show();
 		     	   return false;
 				}
 			}
    	
    	if(!txtUserName.match(/\S/)) {
    		$("#txtUserName").focus();
   		 	$('#dvUserName').addClass('has-error');
    		$('#mrkUserName').show();
     	   return false;
    	} 
    	
    	if(!txtPassword.match(/\S/)) {
    		$("#txtPassword").focus();
   		 	$('#dvPassword').addClass('has-error');
    		$('#mrkPassword').show();
     	   return false;
    	}
    	
    	if(!txtRePassword.match(/\S/)) {
    		$("#txtRePassword").focus();
   		 	$('#dvRePassword').addClass('has-error');
    		$('#mrkRePassword').show();
     	   return false;
    	}
    	
    	if(txtPassword != txtRePassword){
    		$('#txtPassword').focus();
    		$('#dvPassword').addClass('has-error');
    		$('#mrkPassword').show();
    		$('#dvRePassword').addClass('has-error');
    		$('#mrkRePassword').show();
//     		$('#txtPassword').val('');
//      	$('#txtRePassword').val('');
    		
    		alert("password not match");
    		return false;
    	}
    	
    	if (slBrandId == null) {
			$("#slBrand").focus();
			$('#dvBrand').addClass('has-error');
			$('#mrkBrand').show();
			return false;
		}
    	
    	if (slAreaId == null) {
			$("#slArea").focus();
			$('#dvArea').addClass('has-error');
			$('#mrkArea').show();
			return false;
		}
    	
    	if(!slRole.match(/\S/)) {
    		$("#slRole").focus();
   		 	$('#dvRole').addClass('has-error');
    		$('#mrkRole').show();
     	   return false;
    	}
    	
//     	//Get list brand from checkboxlist
//     	var checkedBrand = new Array();
//  	 	$('#cbbrand input:checked').each(function() {
//  	 	checkedBrand.push(this.value);
//      	});
 	 
//     	if(checkedBrand.length == 0){
//     	  $('#dvBrand').addClass('has-error');
//     	  $('#mrkBrand').show();
//     	  return false;
//     	}
		
// 		//Get list Area from checkboxlist
//     	var checkedArea = new Array();
//  	 	$('#cbarea input:checked').each(function() {
//  	 		checkedArea.push(this.value);
//      	});
 	 
//     	if(checkedArea.length == 0){
//     	  $('#dvArea').addClass('has-error');
//     	  $('#mrkArea').show();
//     	  return false;
//     	}
    
 	   jQuery.ajax({
 	      url:'${pageContext.request.contextPath}/user',	
  	      type:'POST',
  	      data:{"key":lParambtn,"DropDown":DropDown,"txtUserID":txtUserID,"txtUserName":txtUserName,"txtPassword":txtPassword,"slRole":slRole,"arealist":slAreaId, "brandlist":slBrandId},
          dataType : 'text',
    	  success:function(data, textStatus, jqXHR){
    		  if(data.split("--")[0] == 'FailedInsertUser')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Insert User Failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		if(DropDown=='admin')
	        			{
		        			$("#txtUserID").focus();
		        			$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        			}
	        		else
	        			{
	        				$("#slUserID").focus();
	        				$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        			}
	        		
	        		return false;
	        	}
	        	else if(data.split("--")[0] == 'FailedUpdateUser')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Update User Failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtUserName").focus();
	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
	        		return false;
	        	}
	        	else
	        	{
	        		var url = '${pageContext.request.contextPath}/user';  
	             	$(location).attr('href', url);
	        	}
          },
          error:function(data, textStatus, jqXHR){
          console.log('Service call failed!');
        }
        });
 	   
    return true;	
	}	
 	
 	function FuncShowUserPanel() {
 		var DropDown = document.getElementById('slDropDown').value;
 	  	
 		$("#UserPanel :input").prop("disabled", false);
 	  	$("#UserPanel :input").val('');
 	  	
 	  	if(DropDown=='admin')
 	  		{
 	  			$("#txtUserID").show();
 	  			$("#slUserID").next(".select2-container").hide();
 	  		}
 	  	else
 	  		{
 	  			$("#txtUserID").hide();
 	  			$("#slUserID").next(".select2-container").show();
 	  		}
 	  	
 	  	FuncClear();
 	}
 	
//  	function FuncValShowDistrict(){	
// 		var Area = document.getElementById('slArea').value;
	
// 		FuncClear();
	
// 		if(!Province.match(/\S/)) {    	
// 		    	$('#slArea').focus();
// 		    	$('#dvArea').addClass('has-error');
// 		    	$('#mrkArea').show();
		    	
// 		    	alert("Fill Area First ...!!!");
// 		        return false;
// 		    } 
// 		    return true;
// 	}
 	
//  	function FuncValShowSubDistrict(){	
//  		var District = document.getElementById('slRole').value;

//  		FuncClear();

//  		if(!District.match(/\S/)) {    	
//  		    	$('#slRole').focus();
//  		    	$('#dvRole').addClass('has-error');
//  		    	$('#mrkRole').show();
 		    	
//  		    	alert("Fill Role First ...!!!");
//  		        return false;
//  		    } 
//  		    return true;
//  		}
 	</script>
</body>
</html>