<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table id="tb_payment_approval_info" class="table table-bordered table-hover">
<thead style="background-color: #d2d6de;">
              <tr>
                <th>PAYMENT ID</th>
                <th>PAYMENT DATE</th>
                <th>PAY AMOUNT</th>
                <th>STATUS</th>
              </tr>
      </thead>
    <tbody>
      
      <c:forEach items="${listpaymentinfo}" var ="paymentinfo">
        <tr>
        <td><c:out value="${paymentinfo.paymentID}"/></td>
        <td><c:out value="${paymentinfo.paymentDate}"/></td>
        <td><c:out value="${paymentinfo.sPaymentAmount}"/></td>
        <td><c:out value="${paymentinfo.status}"/></td>
      	</tr>
      </c:forEach>
      
      </tbody>
</table>

<script>
 		$(function () {
  	  		$("#tb_payment_approval_info").DataTable({
  	  			"aaSorting": []
  	  		});
  		});
</script>