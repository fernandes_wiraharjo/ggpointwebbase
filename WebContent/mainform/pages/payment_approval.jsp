<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Payment Approval</title>

<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">PAYMENT APPROVAL</label>
	</h1>
	</section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessApproval'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Payment Approval Success.
        				</div>
 					</c:if>
 					<c:if test="${condition == 'FailedApproval'}">
 						<div class="alert alert-danger alert-dismissible">
          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
          				Payment Approval Failed. <c:out value="${errorDescription}"/>.
        				</div>
					</c:if>
					
		<input id="selectedbrand" type="hidden" value="<c:out value="${selectedbrand}"/>">
		<input id="selectedarea" type="hidden" value="<c:out value="${selectedarea}"/>">
		<div id="dvSort" class="row">	
		<div class="col-md-3">
		<label for="message-text" class="control-label">BRAND</label>
			<select id="slBrand" name="slBrand" class="form-control">
				<c:forEach items="${listbrand}" var="brand">
					<option value="<c:out value="${brand.brandID}" />"><c:out value="${brand.brandID}" /> - <c:out value="${brand.brandName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		 <!-- /.col-md-3 -->
		<div class="col-md-3">
		<label for="message-text" class="control-label">AREA</label>
		<select id="slArea" name="slArea" class="form-control">
				<c:forEach items="${listarea}" var="area">
					<option value="<c:out value="${area.areaID}" />"><c:out value="${area.areaID}" /> - <c:out value="${area.areaName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		<!-- /.col-md-3 -->
		<div class="col-md-3">
		<button id="btnSort" name="btnSort" type="button" class="btn btn-gg-small2" onclick="FuncButtonSort('sort')" style="margin-top: 25px">SHOW</button>
		</div>
		<!-- /.col-md-3 -->
		</div>
		<!-- /.row -->
       	<br><br>
					
					<!-- modal Payment Approval -->
					<div class="modal fade" id="ModalPaymentApproval" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
<!-- 								<div class="modal-header"> -->
<!-- 									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalPaymentApproval" name="lblTitleModalPaymentApproval"></label></h4> -->
<!-- 								</div> -->
   								<div class="modal-body">
	   								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	   								<br>
									<!--        								<div id="dvTransactionID" class="form-group"> -->
									<!-- 	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Transaction ID</label> -->
									<!--          								<label id="lblTransactionID" name="lblTransactionID" class="control-label" style="margin-left:150px;font-size:large;"></label> -->
									<!--        								</div>  -->
									<!--        								<div id="dvTransactionDate" class="form-group"> -->
									<!-- 	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Transaction Date</label> -->
									<!--          								<label id="lblTransactionDate" name="lblTransactionDate" class="control-label" style="margin-left:192px;font-size:large;"></label> -->
									<!--        								</div>  -->
       								<div id="dvMerchant" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Merchant</label>
         								<label id="lblMerchantID" name="lblMerchantID" class="control-label" style="display:none"></label>
         								<label id="lblMerchant" name="lblMerchant" class="control-label" style="margin-left:215px;font-size:large;"></label>
       								</div> 
       								<div id="dvMerchantArea" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Merchant Area</label>
         								<label id="lblMerchantArea" name="lblMerchantArea" class="control-label" style="margin-left:215px;font-size:large;"></label>
       								</div> 		
       								<div id="dvTransactionPrice" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Transaction Price</label>
         								<label id="lblTotalTransaction" name="lblTotalTransaction" class="control-label" style="display:none"></label>
         								<label id="lblTransactionPrice" name="lblTransactionPrice" class="control-label" style="margin-left:215px;font-size:large;"></label>
       								</div>
       								<div id="dvPayAmount" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Pay Amount</label>
         								<div class="row">
         								<div class="col-md-8">
	         								<div class="input-group" style="margin-left:170px;">
	         								<span class="input-group-addon">Rp</span>
	         								<input class="form-control number" id="txtPayAmount" name="txtPayAmount">
	       									<span class="input-group-addon">,00</span>
	         								</div>
	         								<label id="mrkPayAmount" for="recipient-name" class="control-label" style="margin-left:170px"><small>*</small></label>
       									</div>
       									</div>
       								</div>								
  								</div>
  								
  								<div class="modal-footer">
    									<button type="button" class="btn btn-gg-small" id="btnApprove" name="btnApprove" onclick="FuncValEmptyInput('pay')">CONFIRM</button>
    									<button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end Modal Payment Approval -->
					
					<!-- Modal Show Payment Approval Detail -->
					<div class="modal fade bs-example-modal-lg" id="ModalPaymentInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalPaymentInfo" name="lblTitleModalPaymentInfo">Payment Approval Detail</label></h4>
									<br>
									<label id="transactiondata" style="color:gray;"></label>	
								</div>
   								<div class="modal-body">
	   								<!-- mysql data load payment info by transaction id will be load here -->                          
									<div id="dynamic-content">
									</div>						
  								</div>
  								
  								<div class="modal-footer">
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end Modal Show Payment Approval Detail -->
				
							<table id="tb_transaction" class="table table-bordered table-striped table-hover">
								<thead style="color:#7d0d08;">
									<!-- background-color: #d2d6de; -->
									<tr>
										<th style="width:70px"></th>
										<!-- <th>TRANSACTION ID</th> -->
										<th>MERCHANT</th>
										<th>AREA</th>
										<!-- <th>TRANSACTION DATE</th> -->
										<!-- <th>TOTAL TRANSACTION</th> -->
										<th>TOTAL REMAIN</th>
										<th>MERCHANT PIC</th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listtransaction}" var="transaction">
										<tr>
											<td>
											
											<button id="btnModalPaymentApproval" name="btnModalPaymentApproval" type="button" class="btn btn-info" data-toggle="modal"
														data-target="#ModalPaymentApproval" 
														data-ltransactionid='<c:out value="${transaction.transactionID}" />'
														data-ltransactiondate='<c:out value="${transaction.transactionDate}" />'
														data-lmerchantid='<c:out value="${transaction.merchantID}" />'
														data-lmerchant='<c:out value="${transaction.merchantName}" />'
														data-lmerchantarea='<c:out value="${transaction.merchantArea}" />'
														<%-- data-ltransactiontotal='<c:out value="${transaction.transactionPrice}" />' --%>
														data-ltransactionprice='<c:out value="${transaction.sLastPrice}" />'
														<c:out value="${transaction.buttonStatus}" />
														style="display:<c:out value="${buttonstatus}"/>"
														>
														Pay</button>
												<button id="btnModalShowPaymentInfo" name="btnModalShowPaymentInfo" type="button" class="btn btn-default" data-toggle="modal"
														title="payment detail"
														data-target="#ModalPaymentInfo" 
														data-ltransactionid='<c:out value="${transaction.transactionID}" />'
														<%-- data-ltransactionprice='<c:out value="${transaction.transactionPrice}" />' --%>
														data-ltransactiondate='<c:out value="${transaction.transactionDate}" />'
														data-ltransactionmerchantid='<c:out value="${transaction.merchantID}" />'
														data-ltransactionmerchant='<c:out value="${transaction.merchantName}" />'
														>
												<i class="fa fa-list-ul"></i>
												</button> 
											</td>
											<%-- <td><c:out value="${transaction.transactionID}" /></td> --%>
											<td><c:out value="${transaction.merchantName}" /></td>
											<td><c:out value="${transaction.merchantArea}" /></td>
											<%-- <td><c:out value="${transaction.transactionDate}" /></td> --%>
											<%-- <td><c:out value="${transaction.transactionPrice}" /></td> --%>
											<td><c:out value="${transaction.sLastPrice}" /></td>
											<td><c:out value="${transaction.merchantPIC}" /></td>
										</tr>

									</c:forEach>						
								</tbody>
							</table> 
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
		
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
$(function () {
		$("#tb_transaction").DataTable({
			"aaSorting": []
		});
		$('#M008').addClass('active');
		$('#M012').addClass('active');
		
		var selectedBrand = document.getElementById('selectedbrand').value;
  		$('#slBrand').val(selectedBrand);
  		var selectedArea = document.getElementById('selectedarea').value;
  		$('#slArea').val(selectedArea);
	});
	
$('#ModalPaymentApproval').on('shown.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  
	  var transactionid = button.data('ltransactionid') // Extract info from data-* attributes
	  var transactiondate = button.data('ltransactiondate')
	  var merchantid = button.data('lmerchantid')
	  var merchant = button.data('lmerchant')
	  var merchantarea = button.data('lmerchantarea')
// 	  var transactiontotal = button.data('ltransactiontotal')
	  var transactionprice = button.data('ltransactionprice')
	  
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)

	// 	  document.getElementById("lblTransactionID").innerHTML = transactionid;
	// 	  document.getElementById("lblTransactionDate").innerHTML = transactiondate;
	  document.getElementById("lblMerchantID").innerHTML = merchantid;
	  document.getElementById("lblMerchant").innerHTML = merchant;
	  document.getElementById("lblMerchantArea").innerHTML = merchantarea;
// 	  document.getElementById("lblTotalTransaction").innerHTML = transactiontotal;
	  document.getElementById("lblTransactionPrice").innerHTML = transactionprice;
	  
	  $('#txtPayAmount').focus();
	  FuncClear();
	})
	
function FuncClear(){
	$('#mrkPayAmount').hide();
	
	$('#dvPayAmount').removeClass('has-error');
}

function FuncValEmptyInput(lParambtn) {
	var PayAmount = document.getElementById('txtPayAmount').value;
// 	var TotalTransaction = document.getElementById('lblTotalTransaction').textContent;
	var TransactionPrice = document.getElementById('lblTransactionPrice').textContent;
	var MerchantID = document.getElementById('lblMerchantID').textContent;
	//var TransactionID = document.getElementById('lblTransactionID').textContent;
	
	FuncClear();
	
	if(!PayAmount.match(/\S/)) {
	    	$("#txtPayAmount").focus();
	    	$('#dvPayAmount').addClass('has-error');
	    	$('#mrkPayAmount').show();
	        return false;
	    }
	    
	 jQuery.ajax({
	        url:'${pageContext.request.contextPath}/payment_approval',	
	        type:'POST',
	        data:{"key":"validatepayamount","payamount":PayAmount,"price":TransactionPrice},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.match('disallow')) {
	    			$('#txtPayAmount').focus();
	    	    	$('#dvPayAmount').addClass('has-error');
	    	    	$('#mrkPayAmount').show();
	    	    	document.getElementById('mrkPayAmount').innerHTML = '&nbspmay not bigger than transaction price';
	    	    	
					// 	    	    	$('#txtUOM').focus();
					// 	    	    	$('#dvUOM').addClass('has-error');
					// 	    	    	$('#mrkUOM').show();
						    	    	
	    	        return false;
	    	    }
	        	else {
					// 	        		jQuery.ajax({
					// 	         	        url:'${pageContext.request.contextPath}/payment_approval',	
					// 	         	        type:'POST',
					// 	         	        data:{"key":lParambtn,"transactionid":TransactionID,"payamount":PayAmount},
					// 	         	        dataType : 'text',
	         	    jQuery.ajax({
	         	        url:'${pageContext.request.contextPath}/payment_approval',	
	         	        type:'POST',
	         	        data:{"key":lParambtn,"payamount":PayAmount,"merchantid":MerchantID,"price":TransactionPrice},
// 	         	        	  "totaltransaction":TotalTransaction},
	         	        dataType : 'text',
	         	        success:function(data, textStatus, jqXHR){
	         	        	var url = '${pageContext.request.contextPath}/payment_approval';  
	         	        	$(location).attr('href', url);
	         	        },
	         	        error:function(data, textStatus, jqXHR){
	         	            console.log('Service call failed!');
	         	        }
	         	    });
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
}

$('#ModalPaymentInfo').on('shown.bs.modal', function (event) {
	 var button = $(event.relatedTarget) // Button that triggered the modal
	 var transactionID = button.data('ltransactionid') // Extract info from data-* attributes
// 	 var transactionPrice = button.data('ltransactionprice');
	 var transactionMerchantID = button.data('ltransactionmerchantid');
	 var transactionMerchant = button.data('ltransactionmerchant');
	 var transactionDate = button.data('ltransactiondate');
	 
	//document.getElementById("transactiondata").innerHTML = "Transaction ID : " + "<label style='color:black;'>" + transactionID + "</label>" + "&nbsp&nbsp&nbsp&nbsp" + "Merchant : " + "<label style='color:black;'>" + transactionMerchant + "</label>" + "&nbsp&nbsp&nbsp&nbsp" + "Transaction Date : " + "<label style='color:black;'>" + transactionDate + "</label>";
	document.getElementById("transactiondata").innerHTML = "Merchant : " + "<label style='color:black;'>" + transactionMerchant + "</label>";
	 
	 $('#dynamic-content').html(''); // leave this div blank
	 
	// 	 $.ajax({
	//          url: '${pageContext.request.contextPath}/getPaymentInfo',
	//          type: 'POST',
	//          data: {"transactionid" : transactionID, "transactionprice": transactionPrice},
	//          dataType: 'html'
	//     })
    $.ajax({
         url: '${pageContext.request.contextPath}/getPaymentInfoMerchant',
         type: 'POST',
         data: {"merchantid" : transactionMerchantID},
         dataType: 'html'
    })
    .done(function(data){
         console.log(data); 
         $('#dynamic-content').html(''); // blank before load.
         $('#dynamic-content').html(data); // load here
    })
    .fail(function(){
         $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
    });
})

$('input.number').keyup(function(event) {

 		  // skip for arrow keys
 		  if(event.which >= 37 && event.which <= 40) return;

 		  // format number
 		  $(this).val(function(index, value) {
 		    return value
 		    .replace(/\D/g, "")
 		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
 		    ;
 		  });
 		});
 		
	function FuncButtonSort(lParambtn) {
	 	var BrandID = document.getElementById('slBrand').value;
	 	var AreaID = document.getElementById('slArea').value;
	 	
	 	jQuery.ajax({
		        url:'${pageContext.request.contextPath}/payment_approval',	
		        type:'POST',
		        data:{"key":lParambtn,"BrandID":BrandID,"AreaID":AreaID},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	var url = '${pageContext.request.contextPath}/payment_approval';  
	 	        	$(location).attr('href', url);
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    return true;
	}
</script>

</body>
</html>