<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Verification Doc</title>

<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <style type="text/css">
  /*  css for loading bar */
 .loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 60px;
  height: 60px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/* end of css loading bar */
</style>

</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="formVerificationDoc" name="formVerificationDoc" action = "${pageContext.request.contextPath}/verification_doc" method="post">

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
	<label style="color:#f6f6f6">VERIFICATION DOCUMENT</label>
	</h1>
	</section>
	
	<!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessVerification'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Document Verification Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedVerification'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Document Verification Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					
		<input id="selectedbrand" type="hidden" value="<c:out value="${selectedbrand}"/>">
		<input id="selectedarea" type="hidden" value="<c:out value="${selectedarea}"/>">
		<div id="dvSort" class="row">	
		<div class="col-md-3">
		<label for="message-text" class="control-label">BRAND</label>
			<select id="slBrand" name="slBrand" class="form-control">
				<c:forEach items="${listbrand}" var="brand">
					<option value="<c:out value="${brand.brandID}" />"><c:out value="${brand.brandID}" /> - <c:out value="${brand.brandName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		 <!-- /.col-md-3 -->
		<div class="col-md-3">
		<label for="message-text" class="control-label">AREA</label>
		<select id="slArea" name="slArea" class="form-control">
				<c:forEach items="${listarea}" var="area">
					<option value="<c:out value="${area.areaID}" />"><c:out value="${area.areaID}" /> - <c:out value="${area.areaName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		<!-- /.col-md-3 -->
		<div class="col-md-3">
		<button id="btnSort" name="btnSort" type="button" class="btn btn-gg-small2" onclick="FuncButtonSort('sort')" style="margin-top: 25px">SHOW</button>
		</div>
		<!-- /.col-md-3 -->
		</div>
		<!-- /.row -->
       	<br><br>
       	
       	<!-- modal Payment Verification -->
					<div class="modal fade" id="ModalVerificationDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							
							<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
	     					</div>
<!-- 								<div class="modal-header"> -->
<!-- 									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalPaymentApproval" name="lblTitleModalPaymentApproval"></label></h4> -->
<!-- 								</div> -->
   								<div class="modal-body">
	   								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	   								<br>
       								<div id="dvPaymentID" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Payment Doc ID</label>
         								<label id="lblPaymentID" name="lblPaymentID" class="control-label" style="margin-left:150px;font-size:large;"></label>
       								</div> 
       								<div id="dvTransactionID" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Transaction ID</label>
         								<label id="lblTransactionID" name="lblTransactionID" class="control-label" style="margin-left:150px;font-size:large;"></label>
       								</div>
       								<div id="dvMerchant" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Merchant</label>
         								<label id="lblMerchant" name="lblMerchant" class="control-label" style="margin-left:215px;font-size:large;"></label>
       									<label style="display:none" id="lblMerchantID" name="lblMerchantID"></label>
	         							<label style="display:none" id="lblMerchantUserName" name="lblMerchantUserName"></label>
       								</div> 	
       								<div id="dvPaymentAmount" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Pay Amount</label>
         								<label id="lblPaymentAmount" name="lblPaymentAmount" class="control-label" style="margin-left:215px;font-size:large;"></label>
       								</div>
       								<div id="dvRefNo" class="form-group">
	         							<label for="recipient-name" class="control-label" style="margin-left:225px;color:gray;">Reference No</label><label id="mrkRefNo" for="recipient-name" class="control-label"><small>*</small></label>
	         							<div class="row">
         								<div class="col-md-8">
	         							<input type="text" class="form-control" id="txtRefNo" name="txtRefNo" style="margin-left:100px;">
	         							</div>
	         							</div>
       								</div>								
  								</div>
  								
  								<div class="modal-footer">
  										<!-- loading bar -->
			  							<div id="dvloader" class="loader" style></div>
    									<button type="button" class="btn btn-gg-small" id="btnVerify" name="btnVerify" onclick="FuncValEmptyInput('verify')">VERIFY</button>
    									<button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end Modal Payment Verification -->
				
			<table id="tb_verification_doc" class="table table-bordered table-striped table-hover">
				<thead style="color:#7d0d08;">
					<tr>
						<th></th>
						<th>PAYMENT DOC ID</th>
						<th>PAYMENT DATE</th>
						<!-- <th>TRANSACTION ID</th> -->
						<th>AREA</th>
						<th>MERCHANT</th>
						<th>PIC</th>
						<th>PAYMENT AMOUNT</th>
						<th>STATUS</th>
						<th>NO REF</th>
						<th>VERIFIED BY</th>
						<th>VERIFIED DATE</th>
						<!-- style="width:20px" -->
					</tr>
				</thead>

				<tbody>

					<c:forEach items="${listverification}" var="verification">
						<tr>
							<td>
							<button style="display:<c:out value="${buttonstatus}"/>"
									id="btnModalVerificationDoc" name="btnModalVerificationDoc" type="button" class="btn btn-info" data-toggle="modal"
										data-target="#ModalVerificationDoc" 
										data-lpaymentdocid='<c:out value="${verification.paymentID}" />'
										data-ltransactionid='<c:out value="${verification.transactionID}" />'
										data-lmerchantid='<c:out value="${verification.merchantID}" />'
										data-lmerchant='<c:out value="${verification.merchantName}" />'
										data-lmerchantusername='<c:out value="${verification.merchantUserName}" />'
										data-lpaymentamount='<c:out value="${verification.sPaymentAmount}" />'
										<c:out value="${verification.buttonStatus}" />
										>
										Verify</button>
							</td>
							<td><c:out value="${verification.paymentID}" /></td>
							<td><c:out value="${verification.paymentDate}" /></td>
							<%-- <td><c:out value="${verification.transactionID}" /></td> --%>
							<td><c:out value="${verification.merchantArea}" /></td>
							<td><c:out value="${verification.merchantName}" /></td>
							<td><c:out value="${verification.merchantPIC}" /></td>
							<td><c:out value="${verification.sPaymentAmount}" /></td>
							<td><c:out value="${verification.status}" /></td>
							<td><c:out value="${verification.refNo}" /></td>
							<td><c:out value="${verification.verifiedBy}" /></td>
							<td><c:out value="${verification.verifiedDate}" /></td>
						</tr>

					</c:forEach>						
				</tbody>
			</table> 
			
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
		
    </section>
    <!-- /.content -->
	
</div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->
	
</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
$(function () {
	$("#tb_verification_doc").DataTable({
		"aaSorting": [],
		"scrollX": true
	});
	$('#M008').addClass('active');
	$('#M014').addClass('active');
	
	var selectedBrand = document.getElementById('selectedbrand').value;
		$('#slBrand').val(selectedBrand);
	var selectedArea = document.getElementById('selectedarea').value;
		$('#slArea').val(selectedArea);
		
	$("#dvErrorAlert").hide();
	$('#dvloader').hide();
});

function FuncClear(){
	$('#mrkRefNo').hide();
	
	$('#dvRefNo').removeClass('has-error');
}

$('#ModalVerificationDoc').on('shown.bs.modal', function (event) {
	 $("#dvErrorAlert").hide();
	
	 var button = $(event.relatedTarget) // Button that triggered the modal
	  
	  var paymentdocid = button.data('lpaymentdocid')
	  var transactionid = button.data('ltransactionid') // Extract info from data-* attributes
	  var merchant = button.data('lmerchant')
	  var paymentamount = button.data('lpaymentamount')
	  var merchantid = button.data('lmerchantid')
	  var merchantusername = button.data('lmerchantusername');
	  
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this)

	  document.getElementById("lblPaymentID").innerHTML = paymentdocid;
	  document.getElementById("lblTransactionID").innerHTML = transactionid;
	  document.getElementById("lblMerchantID").innerHTML = merchantid;
	  document.getElementById("lblMerchant").innerHTML = merchant;
	  document.getElementById("lblPaymentAmount").innerHTML = paymentamount;
	  document.getElementById("lblMerchantUserName").innerHTML = merchantusername;
	  
	  $('#txtRefNo').focus();
	  FuncClear();
})

function FuncButtonSort(lParambtn) {
	 	var BrandID = document.getElementById('slBrand').value;
	 	var AreaID = document.getElementById('slArea').value;
	 	
	 	jQuery.ajax({
		        url:'${pageContext.request.contextPath}/verification_doc',	
		        type:'POST',
		        data:{"key":lParambtn,"BrandID":BrandID,"AreaID":AreaID},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	var url = '${pageContext.request.contextPath}/verification_doc';  
	 	        	$(location).attr('href', url);
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    return true;
	}
	
function FuncValEmptyInput(lParambtn) {
	var RefNo = document.getElementById('txtRefNo').value;
	var PaymentDocID = document.getElementById('lblPaymentID').textContent;
	var TransactionID = document.getElementById('lblTransactionID').textContent;
	var MerchantID = document.getElementById('lblMerchantID').textContent;
	var MerchantName = document.getElementById('lblMerchant').textContent;
	var MerchantUserName = document.getElementById('lblMerchantUserName').textContent;
	
	FuncClear();
	
	if(!RefNo.match(/\S/)) {
	    	$("#txtRefNo").focus();
	    	$('#dvRefNo').addClass('has-error');
	    	$('#mrkRefNo').show();
	        return false;
	    }
	
	$('#dvloader').show();
	    
	 jQuery.ajax({
	        url:'${pageContext.request.contextPath}/verification_doc',	
	        type:'POST',
	        data:{"key":lParambtn,"refno":RefNo,"paymentdocid":PaymentDocID,"transactionid":TransactionID,"merchantid":MerchantID,"merchantname":MerchantName,
	        		"merchantusername":MerchantUserName},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
		        	if(data.split("--")[0] == 'FailedVerification')
		        	{
		        		$("#dvErrorAlert").show();
		        		$('#dvloader').hide();
		        		document.getElementById("lblAlert").innerHTML = "Document Verification Failed";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtRefNo").focus();
		        		return false;
		        	}
		        	else
		        	{
		        		var url = '${pageContext.request.contextPath}/verification_doc';  
	      	        	$(location).attr('href', url);
		        	}
      	        },
  	        error:function(data, textStatus, jqXHR){
  	            console.log('Service call failed!');
  	          	$('#dvloader').hide();
  	        }
  	    });
	    return true;
}
</script>

</body>
</html>