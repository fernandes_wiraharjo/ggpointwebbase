<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GG Merchant | Feedback Detail</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
 <style type="text/css">
  /*  css for loading bar */
 .loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 60px;
  height: 60px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/* end of css loading bar */

/* for image modal pop up */
/* Style the Image Used to Trigger the Modal */
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption { 
    animation-name: zoom;
    animation-duration: 0.6s;
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.closespan {
    position: absolute;
    top: 80px;
    right: 200px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.closespan:hover,
.closespan:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
/* end of image modal pop up */
</style>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>
<form id="closefeedbackform" name="" action="feedback_detail" method="post">

<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
    
    <c:if test="${condition == 'FailedCloseFeedback'}">
			<div class="alert alert-danger alert-dismissible">
       			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       			<h4><i class="icon fa fa-ban"></i> Failed</h4>
       				Save and Close Feedback Failed. <c:out value="${errorDescription}"/>.
     		</div>
	</c:if>

    <input  type="hidden" id="feedbackid" name="feedbackid" value="<c:out value="${feedbackid}"/>" />
	<%-- <input  type="hidden" id="merchantid" name="merchantid" value="<c:out value="${merchantid}"/>" /> --%>
			
			 <!-- Default box -->
		      <div class="box box-default box-solid">
		        <div class="box-header with-border">
		          <h3 class="box-title" style="font-weight: bold;"><c:out value="${merchantname}" /></h3>
		        </div>
		        <div class="box-body">
		          <label style="width: 100px">Waktu</label> <label>:</label>&nbsp&nbsp <c:out value="${date}" /> <br>
		          <label style="width: 100px">Jenis Feedback</label> <label>:</label>&nbsp&nbsp <c:out value="${type}" /> <br>
		          <label style="width: 100px">Nama</label> <label>:</label>&nbsp&nbsp <c:out value="${pic}" /> <br>
		          <label style="width: 100px">Status</label> <label>:</label>&nbsp&nbsp <c:out value="${status}" /> <br>
		        </div>
		        <!-- /.box-body -->
		        <div class="box-footer">
		        <%
				//get description from servlet
				String description = null;
		        description = (String) session.getAttribute("description");
				%>
		          <label> <%=description%> </label>
		        </div>
		        <!-- /.box-footer-->
		      </div>
		      <!-- /.box -->
		      
		      <div class="row">
			  <div class="col-md-3">
			      <div class="box box-default box-solid">
			      	<div class="box-body">
			      	<!-- Trigger the Modal -->
				      <img id="myImg" class="img-responsive" src="<c:out value="${fn:replace(file,'GGMerchantWeb/','')}" />" alt="Photo" style="width:228px;height:110px">
				    </div>
			      </div>
		      </div>
			  </div>
			  
			  <!-- The Modal -->
			<div id="myModal" class="modal">
						
				<!-- The Close Button -->
 				 <span class="closespan">&times;</span>
 				 
			  <!-- Modal Content (The Image) -->
			  <img class="modal-content" id="img01">
			
			  <!-- Modal Caption (Image Text) -->
			  <div id="caption"></div>
			</div>
		      
		      <div class="row">
		      	<div id="dvReason" class="col-md-8">
			      <h3 class="box-title" style="font-weight: bold;color:#f6f6f6;">CLOSED FEEDBACK</h3>
			      <label id="mrkReason" for="recipient-name" class="control-label"><small>*</small></label>
				  <textarea id="txtReason" name="txtReason" class="form-control" rows="3" placeholder="Reason ..."></textarea>
				</div>
			  </div>
			  <br>
			  
			  <div class="row">
			  <div class="col-md-2">
			  <button id="btnSave" name="btnSave" type="submit" class="btn btn-gg-small2">SAVE & CLOSE</button>
			  <!-- onclick="FuncSave()" -->
    		  </div>
    		  <div class="col-md-2">
    		  <!-- loading bar -->
			  <div id="dvloader" class="loader" style></div>
			  </div>
    		  </div>
    		  	
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>

	//general page script
 	$(function () {
  		$('#M025').addClass('active');
  		FuncClear();
  		$('#dvloader').hide();
  	});
	
 	function FuncClear(){
 		$('#mrkReason').hide(); 		
 		$('#dvReason').removeClass('has-error');
 	}
 	
 	//validate input before submit
 	$('#closefeedbackform').submit(function() {
 	    // DO STUFF...
		//var FeedbackID = document.getElementById('feedbackid').value;
 		var Reason = document.getElementById('txtReason').value;
		//var File = document.getElementById('InputFile').value;
		//var RedeemValue = document.getElementById('redeemvalue').value;
		//var MerchantID = document.getElementById('merchantid').value;
 		
 		FuncClear();
 	    
 	    if(!Reason.match(/\S/)) {    	
 	    	$('#txtReason').focus();
 	    	$('#dvReason').addClass('has-error');
 	    	$('#mrkReason').show();
 	        return false;
 	    } 
 	    
 	   $('#dvloader').show();
 	    
 	    return true;
 	});
 	
 	//IMAGE MODAL POP UP
 	// Get the modal
 	var modal = document.getElementById('myModal');

 	// Get the image and insert it inside the modal - use its "alt" text as a caption
 	var img = document.getElementById('myImg');
 	var modalImg = document.getElementById("img01");
 	var captionText = document.getElementById("caption");
 	img.onclick = function(){
 	    modal.style.display = "block";
 	    modalImg.src = this.src;
 	    captionText.innerHTML = this.alt;
 	}
 	
 	// Get the <span> element that closes the modal
 	var span = document.getElementsByClassName("closespan")[0];

 	// When the user clicks on <span> (x), close the modal
 	span.onclick = function() { 
 	  modal.style.display = "none";
 	}
</script>

</body>
</html>
