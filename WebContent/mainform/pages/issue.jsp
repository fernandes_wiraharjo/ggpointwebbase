<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GG Merchant | Issue</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<div class="wrapper">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<!-- <h1> -->
	<!-- <label style="color:#f6f6f6">ISSUE</label> -->
	<input id="selectedbrand" type="hidden" value="<c:out value="${selectedbrand}"/>">
		<label for="message-text" class="control-label" style="color:#f6f6f6">BRAND</label>
		<div id="dvBrand" class="row">	
		<div class="col-md-3">
			<select id="slBrand" name="slBrand" class="form-control">
				<c:forEach items="${listbrand}" var="brand">
					<option value="<c:out value="${brand.brandID}" />"><c:out value="${brand.brandID}" /> - <c:out value="${brand.brandName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		 <!-- /.col-md-3 -->
		<div class="col-md-3">
       		<button id="btnSort" name="btnSort" type="button" class="btn btn-gg-small2" onclick="FuncButtonSort()">SHOW</button>
       	</div>
		<!-- /.col-md-3 -->
		</div>
		<!-- /.row -->
	<!-- </h1> -->
		<br>
	</section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">
    
    	<div class="box-body">
    	<c:if test="${condition == 'SuccessCloseIssue'}">
	  		<div class="alert alert-success alert-dismissible">
		      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-check"></i> Success</h4>
       			  	Save and Close Issue Success.
     		</div>
		</c:if>
<%-- 		<c:if test="${condition == 'Failed'}"> --%>
<!-- 			<div class="alert alert-danger alert-dismissible"> -->
<!--        			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--        			<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--        				Save and Close Issue Failed. <c:out value="${errorDescription}"/>. --%>
<!--      		</div> -->
<%-- 		</c:if> --%>
		
		<table id="tb_issue" class="table table-bordered table-striped table-hover">
			<thead style="color:#7d0d08;">
				<!-- background-color: #d2d6de; -->
				<tr>
					<th>ISSUE LIST</th>
				</tr>
			</thead>

			<tbody>
			<c:forEach items="${listissue}" var="issue">
			<tr>
			<td>
				 <!-- Default box -->
			      <div class="box collapsed-box">
			        <div class="box-header with-border">
			          <h3 class="box-title" style="font-weight: bold;"><c:out value="${issue.merchantName}" /></h3>
			          &nbsp&nbsp&nbsp&nbsp
			          Issued <c:out value="${issue.issuedDate}" /> By <c:out value="${issue.issuedBy}" />
			          <div class="box-tools pull-right">
			            <button type="button" class="btn btn-box-tool" data-widget="collapse" title="expand">
			              <i class="fa fa-plus"></i></button>
			          </div>
			        </div>
			        <div class="box-body">
			          <label style="color:#919191;"><c:out value="${fn:substring(issue.message,0,69)}" />.....</label>
			        </div>
			        <!-- /.box-body -->
			        <div class="box-footer">
			          <a href="${pageContext.request.contextPath}/issue_detail?merchantID=<c:out value="${issue.merchantID}" />&merchantName=<c:out value="${issue.merchantName}" />&message=<c:out value="${issue.message}" />&datetime=<c:out value="${issue.date}" />&messageType=<c:out value="${issue.messageType}" />&merchantPIC=<c:out value="${issue.merchantPIC}" />&redeemValue=<c:out value="${issue.transactionPrice}" />&refNo=<c:out value="${issue.refNo}" />&status=<c:out value="${issue.status}" />&statusDetail=<c:out value="${issue.statusDetail}" />&messageID=<c:out value="${issue.messageID}" />&merchantusername=<c:out value="${issue.userName}" />" 
			          	id="getissuedetail" name="getissuedetail" style="color:#7d0d08;font-weight: bold;">
						More
					  </a>
			        </div>
			        <!-- /.box-footer-->
			      </div>
			      <!-- /.box -->
			      </td>
			      </tr>
			</c:forEach>
			</tbody>
			</table> 
		
			</div>
			<!-- /.box-body -->
			</div>
			 <!-- /.box -->
			</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
	
	<!-- Info boxes -->
<!-- 		<div class="row"> -->
<!-- 		 <div class="col-md-12"> -->
<!-- 		 <div style="margin-left:31.2vw;margin-top:35vh;"> -->
<%-- 		 	<label for="user" class="control-label" style="font-size:30px;color:#f6f6f6"><c:out value="${pagenotif}" /></label> --%>
<!-- 		 	</div> -->
<!-- 		 </div> -->
		 <!-- /.col-md-12 -->
<!-- 		</div> -->
		<!-- /.row -->
		
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
 	$(function () {
 		$("#tb_issue").DataTable({
 			"aaSorting": []
 		});
  		$('#M007').addClass('active');
  		
  		var selectedBrand = document.getElementById('selectedbrand').value;
  		$('#slBrand').val(selectedBrand);
  	});
 	
 	
 	function FuncButtonSort() {
	 	var BrandID = document.getElementById('slBrand').value;
	 	
	 	jQuery.ajax({
		        url:'${pageContext.request.contextPath}/issue',	
		        type:'POST',
		        data:{"BrandID":BrandID},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	var url = '${pageContext.request.contextPath}/issue';  
	 	        	$(location).attr('href', url);
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    return true;
	}
</script>

</body>
</html>
