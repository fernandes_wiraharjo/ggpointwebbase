<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:forEach items="${listmerchant}" var="merchant">
	<div class="checkbox" id="cbMerchant">
		<label><input type="checkbox"
			id="chk"
			name="<c:out value="${merchant.merchantName}" /> - <c:out value="${merchant.merchantTypeName}" />"
			value="<c:out value="${merchant.merchantID}" />">
		<c:out value="${merchant.merchantName}" /> - <c:out value="${merchant.merchantTypeName}" /></label>
	</div>
</c:forEach>