<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Mobile Config</title>
<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="formMobileConfig" name="formMobileConfig" action = "${pageContext.request.contextPath}/mobileconfig" method="post">

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">Mobile Config</label>
	</h1>
	</section>
	
	<!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessInsertMobileConfig'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Insert Mobile Config Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedInsert'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Insert Mobile Config Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					<c:if test="${condition == 'SuccessUpdateMobileConfig'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Update Mobile Config Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedUpdate'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Update Mobile Config Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					<c:if test="${condition == 'SuccessDelete'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Delete Mobile Config Success.
        				</div>
 					</c:if>
 					<c:if test="${condition == 'FailedDelete'}">
 						<div class="alert alert-danger alert-dismissible">
          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
          				Delete Mobile Config Failed. <c:out value="${errorDescription}"/>.
        				</div>
					</c:if>
					
					<!-- modal update insert merchant -->
					<div class="modal fade" id="ModalUpdateInsert"  role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							
							<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
	     					</div>
	     					
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalUpdateInsert" name="lblTitleModalUpdateInsert"></label></h4>
<!-- 									<label id="lblBrandId" name="lblBrandId" style="color:red;"></label> -->
								</div>
   								<div class="col-xs-12">
   								<div class="col-xs-12">
   								<br>
   								</div>
   								<div id="dvApkVersion" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">Configuration APK Version</label><label id="mrkApkVersion" for="recipient-name" class="control-label"><small>*</small></label>
         								<input class="form-control" type="text" id="txtApkVersion" name="txtApkVersion">
       							</div>
   								<div id="dvIsConfFeedback" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">Configuration Feedback Menu</label>
 										<select id="slIsConfFeedback" name="slIsConfFeedback"
														class="form-control select2" style="width: 100%;">
															<option id="optIsConfFeedback" name="optIsConfFeedback"
																value="true">Active</option>
															<option id="optIsConfFeedback" name="optIsConfFeedback"
																value="false">Non Active</option>
										</select>	
       							</div>
       							<div id="dvIsConfOneDay" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">Configuration Max Transaction Per Day</label><label id="mrkIsConfOneDay" for="recipient-name" class="control-label"><small>*</small></label>
         								<input class="form-control" type="number" id="txtIsConfOneDay" name="txtIsConfOneDay" data-toggle="modal" data-target="#ModalGetMobileConfig">
 
       							</div>
       							<div id="dvIsConfOneProduct" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">Configuration Max Product Per Transaction</label><label id="mrkIsConfOneProduct" for="recipient-name" class="control-label"><small>*</small></label>
         								<input  class="form-control" type="number" id="txtIsConfOneProduct" name="txtIsConfOneProduct" data-toggle="modal" data-target="#ModalGetMobileConfig">
       							</div>
       							<div id="dvIsConfQty" class="form-group col-xs-12" >
         								<label for="message-text" class="control-label">Configuration Max Qty Per Product</label><label id="mrkIsConfQty" for="recipient-name" class="control-label"><small>*</small></label>
         								<input  class="form-control" type="number" id="txtIsConfQty" name="txtIsConfQty" data-toggle="modal" data-target="#ModalGetMobileConfig">
       							</div>
       							<div id="dvSummaryRange" class="form-group col-xs-12">
         								<label for="message-text" class="control-label">Configuration Summary Transaction Date Range (Week)</label><label id="mrkSummaryRange" for="recipient-name" class="control-label"><small>*</small></label>
										<!-- <input class="form-control" type="number" id="txtSummaryRange" name="txtSummaryRange" data-toggle="modal" data-target="#ModalGetMobileConfig"> -->
       									<select id="txtSummaryRange" name="txtSummaryRange"
														class="form-control select2" style="width: 100%;">
															<option id="1" name="1"
																value="1">1 Week</option>
															<option id="2" name="2"
																value="2">2 Week</option>
															<option id="4" name="4"
																value="4">4 Week</option>
										</select>
       							</div>
       								
   								</div>
   								<div class="modal-footer">
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
    									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end modal update & Insert -->
			
					<!--modal Delete -->
				<div class="example-modal">
				<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
						<div class="modal-content">
									<div class="modal-header">
 										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
   										<span aria-hidden="true">&times;</span></button>
 										<h4 class="modal-title">Alert Delete Mobile Config</h4>
									</div>
								<div class="modal-body">
								
	 									<p>Are you sure to delete this Mobile Config?</p>
								</div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			                <button <c:out value="${buttonstatus}"/> type="submit" id="btnDelete" name="btnDelete" class="btn btn-outline" >Delete</button>
			              </div>
			            </div>
			            <!-- /.modal-content -->
			          </div>
			          <!-- /.modal-dialog -->
			        </div>
			        <!-- /.modal -->
			      </div>
			    <!--modal Delete -->
				
				<div class="col-xs-8" class="form-horizontal" style="margin-top: 10px">
				<div id="dvBrandId" class="form-group">
	         		<label for="inputEmail3" class="col-sm-2 control-label" style="Height: 34px;text-align:center; margin-top: 7px">Brand Id</label><label id="mrkBrandId"  ></label>
         			<div class="col-sm-10">

					<select id="slBrandId" name="slBrandId"
														class="form-control select2" 
														data-placeholder="Select Brand" style="width: 200px;">
														<c:forEach items="${listbrand}" var="brand">
															<option id="optbrand" name="optbrand"
																value="<c:out value="${brand.brandID}" />"><c:out
																	value="${brand.brandID}" /> - <c:out
																	value="${brand.brandName}" /></option>
														</c:forEach>
										</select>	
					
					<small><label id="lblBrandName" name="lblBrandName" ></label></small>
					
					</div>
       			</div>
       			</div>
       			<div class="col-xs-4"  style="margin-top: 10px">
       			<div class="form-group">
<!--        			<table> -->
<!--        			<tr> -->
<!--        				<td style="width:150px;"> -->
					<div class="col-sm-4">
       					<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" style="width:100px;" class="btn btn-gg-small pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> ADD NEW</button>
					</div>
<!--        				</td> -->
<!--        				<td style="width:120px;"> -->
					<div class="col-sm-4">	
       					<button <c:out value="${buttonstatus}"/> id="btnModalUpdate" name="btnModalUpdate" type="button" style="width:100px;" class="btn btn-gg-small pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonUpdate()"><i class="fa fa-copy"></i> UPDATE</button>
					</div>	
<!--        				</td> -->
<!--        				<td style="width:100px;"> -->
					<div class="col-sm-4">	
       					<button <c:out value="${buttonstatus}"/> id="btnModalDelete" name="btnModalDelete" type="button" style="width:100px;" class="btn btn-gg-small pull-right" data-toggle="modal" data-target="#ModalDelete" ><i class="fa fa-remove"></i> DELETE</button>
					</div>
<!--        				</td> -->
<!--        			</tr> -->
<!--        			</table> -->
       			</div>
				</div>
				<div class="col-xs-12">
				<hr>
				<table id="tb_mobileconfig" class="table table-bordered table-striped table-hover">
								<thead style="color:#7d0d08;">
									<!-- background-color: #d2d6de; -->
									<tr>
										<th>Brand Id</th>
										<th>Mobile Config Description</th>
										<th>Value</th>

									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listmobileconfig}" var="mobileconfig">
										<tr>
											<td><c:out value="${mobileconfig.brandId}" /></td>
											<td><c:out value="${mobileconfig.desc}" /></td>
											
											<c:choose>
											<c:when  test="${mobileconfig.value == 'true'}">
												<td>Active</td>
											</c:when>
											<c:when  test="${mobileconfig.value == 'false'}">
												<td>Non Active</td>
											</c:when>
											<c:otherwise>
												<td><c:out value="${mobileconfig.value}" /></td>
											</c:otherwise>
											</c:choose>
											
											

										</tr>

									</c:forEach>						
								</tbody>
							</table> 
				</div>
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
$(function() {
	//Initialize Select2 Elements
	$(".select2").select2();
	$("#tb_mobileconfig").DataTable();
	$('#M024').addClass('active');
	
	$('#slIsConfFeedback').val("true").trigger("change");
	$('#txtSummaryRange').val("1").trigger("change");
	$('#slBrandId').val(<c:out value="${gBrandId}"/>).trigger("change");
	//FuncLoadBrand($('#slBrandId').val);
	

	var table = $("#tb_mobileconfig").DataTable();
 
	if (table.column( 0 ).data().length != 0) {
		$("#btnModalNew").hide();
	}
	if (table.column( 0 ).data().length == 0) {
		//shortcut for button 'new'
		Mousetrap.bind('n', function() {
			FuncButtonNew(), $('#ModalUpdateInsert').modal('show')
		});
		$("#btnModalUpdate").hide();
		$("#btnModalDelete").hide();
	}
	
	$("#dvErrorAlert").hide();
});

$('#slBrandId').on("select2:select", function(e) {
	var slBrandId = $('#slBrandId').val();
	
	FuncLoadBrand(slBrandId);
	
	$('#slBrandId').val(slBrandId).trigger("change.select2");
})

function FuncLoadBrand(slBrandId) {
	var slBrandId_temp = slBrandId;
	jQuery.ajax({
        url:'${pageContext.request.contextPath}/mobileconfig',	
        type:'POST',
        data:{"key":"LoadMobileConfig","slBrandId":slBrandId},
        dataType : 'text',
        success:function(data, textStatus, jqXHR){
        	var url = '${pageContext.request.contextPath}/mobileconfig';  
        	$(location).attr('href', url);
        	$('#slBrandId').val(slBrandId_temp).trigger("change");
        },
        error:function(data, textStatus, jqXHR){
            console.log('Service call failed!');
        }
    });
	
	return true;
	
}

function FuncClear() {

	$('#mrkIsConfQty').hide();
	$('#mrkIsConfOneDay').hide();
	$('#mrkIsConfOneProduct').hide();
	$('#mrkSummaryRange').hide();
	$('#mrkApkVersion').hide();

	$('#dvIsConfQty').removeClass('has-error');
// 	$('#dvIsConfFeedback').removeClass('has-error');
	$('#dvIsConfOneDay').removeClass('has-error');
	$('#dvIsConfOneProduct').removeClass('has-error');
	$('#dvApkVersion').removeClass('has-error');
// 	$('#dvSummaryRange').removeClass('has-error');
	
	
}

function FuncButtonNew() {

	$('#txtIsConfQty').val('');
	$('#txtIsConfOneDay').val('');
	$('#txtIsConfOneProduct').val('');
	$('#txtApkVersion').val('');
// 	$('#txtSummaryRange').val('');
	
	var slBrandId = $('#slBrandId').val();
	document.getElementById("lblTitleModalUpdateInsert").innerHTML = "Mobile Config - ADD NEW ";
// 	document.getElementById("lblBrandId").innerHTML =  slBrandId;
	
	$('#btnSave').show();
 	$('#btnUpdate').hide();
	FuncClear();
}

function FuncButtonUpdate() {
	var slBrandId = $('#slBrandId').val();
	//<<Javascript For 1 json
	$.ajax({
	url : '${pageContext.request.contextPath}/mobileconfigcontroller',
	type : 'POST',
	data : {
		slBrandId : slBrandId,
		command : "getMobileConfig"
	},
	dataType : 'json'
})
		.done(
				function(data) {
					console.log(data);
					
					for ( var i in data) {

						if (data[i].ID == "APKVersion") {
							$('#txtApkVersion').val(data[i].Value);
							
						}
						else if(data[i].ID == "isConfFeedback"){
							$('#slIsConfFeedback').val(data[i].Value).trigger("change");
						}
						else if (data[i].ID == "isConfOneDay") {
							$('#txtIsConfOneDay').val(data[i].Value);
							
						} 
						else if(data[i].ID == "isConfOneProduct") {
							$('#txtIsConfOneProduct').val(data[i].Value);
						}
						else if(data[i].ID == "isConfQty") {
							$('#txtIsConfQty').val(data[i].Value);
						}
						else{
							$('#txtSummaryRange').val(data[i].Value).trigger("change");
						}
					}
				}).fail(function() {
					console.log('Service call failed!');
				})
				
	
	
	
	document.getElementById("lblTitleModalUpdateInsert").innerHTML = "Mobile Config - UPDATE";
// 	document.getElementById("lblBrandId").innerHTML =  slBrandId;
	
	$('#btnSave').hide();
 	$('#btnUpdate').show();
	FuncClear();
}

$('#ModalUpdateInsert').on(
		'shown.bs.modal',
		function(event) {
			$("#dvErrorAlert").hide();
			var button = $(event.relatedTarget);
			var modal = $(this);
			


		})
		
// $('#ModalDelete').on('show.bs.modal', function (event) {
// 		var button = $(event.relatedTarget);
// 		$("#temp_txtProductId").val(lproductid);
// 		$("#temp_txtMerchantId").val(lmerchantid);
// 	})
			


function FuncValEmptyInput(lParambtn) {
		var slBrandId = $('#slBrandId').val();
		var slIsConfFeedback = $('#slIsConfFeedback').val();
		var txtIsConfQty = $('#txtIsConfQty').val();
		var txtIsConfOneProduct = $('#txtIsConfOneProduct').val();
		var txtIsConfOneDay = $('#txtIsConfOneDay').val();
		var txtSummaryRange = $('#txtSummaryRange').val();
		var txtApkVersion = $('#txtApkVersion').val();
		
		FuncClear();
		
		if(!txtIsConfOneDay.match(/\S/)) {
	    	$("#txtIsConfOneDay").focus();
	    	$('#dvIsConfOneDay').addClass('has-error');
	    	$('#mrkIsConfOneDay').show();
	        return false;
	    }	
		
		if(!txtIsConfOneProduct.match(/\S/)) {
	    	$("#txtIsConfOneProduct").focus();
	    	$('#dvIsConfOneProduct').addClass('has-error');
	    	$('#mrkIsConfOneProduct').show();
	        return false;
	    }
		
		if(!txtIsConfQty.match(/\S/)) {
	    	$("#txtIsConfQty").focus();
	    	$('#dvIsConfQty').addClass('has-error');
	    	$('#mrkIsConfQty').show();
	        return false;
	    }
	    
	    if(!txtApkVersion.match(/\S/)) {
	    	$("#txtApkVersion").focus();
	    	$('#dvApkVersion').addClass('has-error');
	    	$('#mrkApkVersion').show();
	        return false;
	    }
	   	
// 	    if(!txtSummaryRange.match(/\S/)) {
// 	    	$("#txtSummaryRange").focus();
// 	    	$('#dvSummaryRange').addClass('has-error');
// 	    	$('#mrkSummaryRange').show();
// 	        return false;
// 	    }
	   
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/mobileconfig',	
	        type:'POST',
	        data:{"key":lParambtn,"slBrandId":slBrandId,"slIsConfFeedback":slIsConfFeedback,"txtIsConfQty":txtIsConfQty,"txtIsConfOneProduct":txtIsConfOneProduct,"txtIsConfOneDay":txtIsConfOneDay,"txtSummaryRange":txtSummaryRange,"txtApkVersion":txtApkVersion},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertMobileConfig')
 	        	{
 	        		$("#dvErrorAlert").show();
 	        		document.getElementById("lblAlert").innerHTML = "Insert Mobile Config Failed";
 	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
 	        		$("#slIsConfFeedback").focus();
 	        		return false;
 	        	}
 	        	else if(data.split("--")[0] == 'FailedUpdateMobileConfig')
 	        	{
 	        		$("#dvErrorAlert").show();
 	        		document.getElementById("lblAlert").innerHTML = "Update Mobile Config Failed";
 	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
 	        		$("#slIsConfFeedback").focus();
 	        		return false;
 	        	}
 	        	else
 	        	{
		        	var url = '${pageContext.request.contextPath}/mobileconfig';  
		        	$(location).attr('href', url);
 	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
	}
	
</script>			
</body>
</html>