<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Approval Doc</title>

<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="formApprovalDoc" name="formApprovalDoc" action = "${pageContext.request.contextPath}/approval_doc" method="post">

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">APPROVAL DOCUMENT</label>
	</h1>
	</section>
	
	<!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessApproval'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Approval Document Success.
        				</div>
 					</c:if>
 					<c:if test="${condition == 'FailedApproval'}">
 						<div class="alert alert-danger alert-dismissible">
          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
          				Approval Document Failed. <c:out value="${errorDescription}"/>.
        				</div>
					</c:if>
					<c:if test="${condition == 'SuccessReject'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Reject Payment Document Success.
        				</div>
 					</c:if>
 					<c:if test="${condition == 'FailedReject'}">
 						<div class="alert alert-danger alert-dismissible">
          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
          				Reject Payment Document Failed. <c:out value="${errorDescription}"/>.
        				</div>
					</c:if>
					
		<input id="selectedbrand" type="hidden" value="<c:out value="${selectedbrand}"/>">
		<input id="selectedarea" type="hidden" value="<c:out value="${selectedarea}"/>">
		<div id="dvSort" class="row">	
		<div class="col-md-3">
		<label for="message-text" class="control-label">BRAND</label>
			<select id="slBrand" name="slBrand" class="form-control">
				<c:forEach items="${listbrand}" var="brand">
					<option value="<c:out value="${brand.brandID}" />"><c:out value="${brand.brandID}" /> - <c:out value="${brand.brandName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		 <!-- /.col-md-3 -->
		<div class="col-md-3">
		<label for="message-text" class="control-label">AREA</label>
		<select id="slArea" name="slArea" class="form-control">
				<c:forEach items="${listarea}" var="area">
					<option value="<c:out value="${area.areaID}" />"><c:out value="${area.areaID}" /> - <c:out value="${area.areaName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		<!-- /.col-md-3 -->
		<div class="col-md-3">
		<button id="btnSort" name="btnSort" type="button" class="btn btn-gg-small2" onclick="FuncButtonSort('sort')" style="margin-top: 25px">SHOW</button>
		</div>
		<!-- /.col-md-3 -->
		</div>
		<!-- /.row -->
       	<br><br>
					
					<!-- modal Approval Document -->
					<div class="modal modal-success" id="ModalApprovalDoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">            											
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Approval Payment Document Confirmation</h4>
						</div>
						<div class="modal-body">
							<input type="hidden" id="temp_txtPaymentID" name="temp_txtPaymentID"  />
							<p>Are you sure to approve this payment document ?</p>
						</div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">CLOSE</button>
			                <button id="btnApprovalDoc" name="btnApprovalDoc"  class="btn btn-outline" >APPROVE</button>
			              </div>
            		</div>
           			 <!-- /.modal-content -->
		          </div>
		          <!-- /.modal-dialog -->
		       </div>
		       <!-- /.modal -->
				
				<!-- modal Reject Approval Document -->
					<div class="modal modal-danger" id="ModalRejectApprovalDoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">            											
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Reject Payment Document Confirmation</h4>
						</div>
						<div class="modal-body">
							<input type="hidden" id="temp_txtPaymentID" name="temp_txtPaymentID"  />
							<input type="hidden" id="temp_txtMerchantID" name="temp_txtMerchantID"  />
							<input type="hidden" id="temp_txtPaymentAmount" name="temp_txtPaymentAmount"  />
							<p>Are you sure to reject this payment document ?</p>
						</div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">CLOSE</button>
			                <button id="btnRejectApprovalDoc" name="btnRejectApprovalDoc"  class="btn btn-outline" >REJECT</button>
			              </div>
            		</div>
           			 <!-- /.modal-content -->
		          </div>
		          <!-- /.modal-dialog -->
		       </div>
		       <!-- /.modal reject-->
				
			<table id="tb_approval_doc" class="table table-bordered table-striped table-hover">
				<thead style="color:#7d0d08;">
					<tr>
						<th style="width:120px"></th>
						<th>PAYMENT DOC ID</th>
						<th>PAYMENT DATE</th>
						<!-- <th>TRANSACTION ID</th> -->
						<th>AREA</th>
						<th>MERCHANT</th>
						<th>PIC</th>
						<th>PAYMENT AMOUNT</th>
						<th>STATUS</th>
						<th>APPROVED BY</th>
						<th>APPROVED DATE</th>
						<!-- style="width:20px" -->
					</tr>
				</thead>

				<tbody>

					<c:forEach items="${listapproval}" var="approval">
						<tr>
							<td>
							<table>
							<td><button style="display:<c:out value="${buttonstatus}"/>" 
										id="btnModalApprovalDoc" name="btnModalApprovalDoc" type="button" class="btn btn-info" data-toggle="modal"
										data-target="#ModalApprovalDoc" 
										data-lpaymentdocid='<c:out value="${approval.paymentID}" />'
										<c:out value="${approval.buttonStatus}" />
										>
										Approve</button></td>
							<td>&nbsp</td>
							<td><button style="display:<c:out value="${buttonstatus}"/>"
										id="btnRejectApprovalDoc" name="btnRejectApprovalDoc" type="button" class="btn btn-danger" data-toggle="modal"
										data-target="#ModalRejectApprovalDoc" 
										data-lpaymentdocid='<c:out value="${approval.paymentID}" />'
										data-lmerchantid='<c:out value="${approval.merchantID}" />'
										data-lpaymentamount='<c:out value="${approval.paymentAmount}" />'
										<c:out value="${approval.buttonStatusReject}" />
										>
										Reject</button></td>
							</table>
							</td>
							<td><c:out value="${approval.paymentID}" /></td>
							<td><c:out value="${approval.paymentDate}" /></td>
							<%-- <td><c:out value="${approval.transactionID}" /></td> --%>
							<td><c:out value="${approval.merchantArea}" /></td>
							<td><c:out value="${approval.merchantName}" /></td>
							<td><c:out value="${approval.merchantPIC}" /></td>
							<td><c:out value="${approval.sPaymentAmount}" /></td>
							<td><c:out value="${approval.status}" /></td>
							<td><c:out value="${approval.lastUpdateBy}" /></td>
							<td><c:out value="${approval.lastDate}" /></td>
						</tr>

					</c:forEach>						
				</tbody>
			</table> 
			
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
		
    </section>
    <!-- /.content -->
	
</div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
$(function () {
	$("#tb_approval_doc").DataTable({
		"aaSorting": [],
		"scrollX": true
	});
	$('#M008').addClass('active');
	$('#M013').addClass('active');
	
	var selectedBrand = document.getElementById('selectedbrand').value;
		$('#slBrand').val(selectedBrand);
		var selectedArea = document.getElementById('selectedarea').value;
		$('#slArea').val(selectedArea);
});

$('#ModalApprovalDoc').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var lPaymentDocID = button.data('lpaymentdocid');
	$("#temp_txtPaymentID").val(lPaymentDocID);
})

$('#ModalRejectApprovalDoc').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget);
	var lPaymentDocID = button.data('lpaymentdocid');
	var lMerchantID = button.data('lmerchantid');
	var lPaymentAmount = button.data('lpaymentamount');
	$("#temp_txtPaymentID").val(lPaymentDocID);
	$("#temp_txtMerchantID").val(lMerchantID);
	$("#temp_txtPaymentAmount").val(lPaymentAmount);
})

function FuncButtonSort(lParambtn) {
	 	var BrandID = document.getElementById('slBrand').value;
	 	var AreaID = document.getElementById('slArea').value;
	 	
	 	jQuery.ajax({
		        url:'${pageContext.request.contextPath}/approval_doc',	
		        type:'POST',
		        data:{"key":lParambtn,"BrandID":BrandID,"AreaID":AreaID},
		        dataType : 'text',
		        success:function(data, textStatus, jqXHR){
		        	var url = '${pageContext.request.contextPath}/approval_doc';  
	 	        	$(location).attr('href', url);
		        },
		        error:function(data, textStatus, jqXHR){
		            console.log('Service call failed!');
		        }
		    });
		    return true;
	}
</script>

</body>
</html>