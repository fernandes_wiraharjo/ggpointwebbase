package model;

import java.util.List;

public class mdlProvince {

	public String error;
	public String message;
	public List<model.mdlProvinceDetail> semuaprovinsi;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<model.mdlProvinceDetail> getSemuaprovinsi() {
		return semuaprovinsi;
	}
	public void setSemuaprovinsi(List<model.mdlProvinceDetail> semuaprovinsi) {
		this.semuaprovinsi = semuaprovinsi;
	}
	
	
}
