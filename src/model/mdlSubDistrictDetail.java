package model;

public class mdlSubDistrictDetail {

	public String id;
	public String nama;
	public String id_kabupaten;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getId_kabupaten() {
		return id_kabupaten;
	}
	public void setId_kabupaten(String id_kabupaten) {
		this.id_kabupaten = id_kabupaten;
	}
	
}
