package model;

import java.util.List;

public class mdlDistrict {

	public String error;
	public String message;
	public List<model.mdlDistrictDetail> daftar_kecamatan;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<model.mdlDistrictDetail> getDaftar_kecamatan() {
		return daftar_kecamatan;
	}
	public void setDaftar_kecamatan(List<model.mdlDistrictDetail> daftar_kecamatan) {
		this.daftar_kecamatan = daftar_kecamatan;
	}
	
	
}
