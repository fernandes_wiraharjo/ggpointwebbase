package model;

public class mdlMerchant {

	public String MerchantID;
	public String MerchantName;
	public String MerchantPIC;
	public String MerchantEmail;
	public String MerchantContact;
	public String MerchantBrand;
	public String MerchantType;
	public String MerchantTypeName;
	public String MerchantArea;
	public String MerchantAreaName;
	public String MerchantAddress;
	public String MerchantProvinceID;
	public String MerchantProvince;
	public String MerchantDistrict;
	public String MerchantDistrictName;
	public String MerchantSubDistrict;
	public String MerchantSubDistrictName;
	public String MerchantBank;
	public String MerchantBankName;
	public String AccountName;
	public String MerchantAccountNumber;
	
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getMerchantName() {
		return MerchantName;
	}
	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}
	public String getMerchantPIC() {
		return MerchantPIC;
	}
	public void setMerchantPIC(String merchantPIC) {
		MerchantPIC = merchantPIC;
	}
	public String getMerchantEmail() {
		return MerchantEmail;
	}
	public void setMerchantEmail(String merchantEmail) {
		MerchantEmail = merchantEmail;
	}
	public String getMerchantContact() {
		return MerchantContact;
	}
	public void setMerchantContact(String merchantContact) {
		MerchantContact = merchantContact;
	}
//	public String[] getMerchantBrand() {
//		return MerchantBrand;
//	}
//	public void setMerchantBrand(String[] merchantBrand) {
//		MerchantBrand = merchantBrand;
//	}
		public String getMerchantBrand() {
		return MerchantBrand;
	}
	public void setMerchantBrand(String merchantBrand) {
		MerchantBrand = merchantBrand;
	}
	public String getMerchantType() {
		return MerchantType;
	}
	public void setMerchantType(String merchantType) {
		MerchantType = merchantType;
	}
	public String getMerchantTypeName() {
		return MerchantTypeName;
	}
	public void setMerchantTypeName(String merchantTypeName) {
		MerchantTypeName = merchantTypeName;
	}
	public String getMerchantArea() {
		return MerchantArea;
	}
	public void setMerchantArea(String merchantArea) {
		MerchantArea = merchantArea;
	}
	public String getMerchantAreaName() {
		return MerchantAreaName;
	}
	public void setMerchantAreaName(String merchantAreaName) {
		MerchantAreaName = merchantAreaName;
	}
	public String getMerchantAddress() {
		return MerchantAddress;
	}
	public void setMerchantAddress(String merchantAddress) {
		MerchantAddress = merchantAddress;
	}
	public String getMerchantProvince() {
		return MerchantProvince;
	}
	public String getMerchantProvinceID() {
		return MerchantProvinceID;
	}
	public void setMerchantProvinceID(String merchantProvinceID) {
		MerchantProvinceID = merchantProvinceID;
	}
	public void setMerchantProvince(String merchantProvince) {
		MerchantProvince = merchantProvince;
	}
	public String getMerchantDistrict() {
		return MerchantDistrict;
	}
	public void setMerchantDistrict(String merchantDistrict) {
		MerchantDistrict = merchantDistrict;
	}
	public String getMerchantSubDistrict() {
		return MerchantSubDistrict;
	}
	public void setMerchantSubDistrict(String merchantSubDistrict) {
		MerchantSubDistrict = merchantSubDistrict;
	}
	public String getMerchantBank() {
		return MerchantBank;
	}
	public void setMerchantBank(String merchantBank) {
		MerchantBank = merchantBank;
	}
	public String getMerchantBankName() {
		return MerchantBankName;
	}
	public void setMerchantBankName(String merchantBankName) {
		MerchantBankName = merchantBankName;
	}
	public String getMerchantAccountNumber() {
		return MerchantAccountNumber;
	}
	public void setMerchantAccountNumber(String merchantAccountNumber) {
		MerchantAccountNumber = merchantAccountNumber;
	}
	public String getMerchantDistrictName() {
		return MerchantDistrictName;
	}
	public void setMerchantDistrictName(String merchantDistrictName) {
		MerchantDistrictName = merchantDistrictName;
	}
	public String getMerchantSubDistrictName() {
		return MerchantSubDistrictName;
	}
	public void setMerchantSubDistrictName(String merchantSubDistrictName) {
		MerchantSubDistrictName = merchantSubDistrictName;
	}
	public String getAccountName() {
		return AccountName;
	}
	public void setAccountName(String accountName) {
		AccountName = accountName;
	}
	
	
}
