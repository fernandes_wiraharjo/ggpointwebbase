package model;

public class mdlProduct {
	public String ProductID;
	public String ProductName;
	public String Price;

	public String Status;
	public String BrandID;

	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
//	public String getPoint() {
//		return Point;
//	}
//	public void setPoint(String point) {
//		Point = point;
//	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getBrandID() {
		return BrandID;
	}
	public void setBrandID(String brandID) {
		BrandID = brandID;
	}
	
	
	
}
