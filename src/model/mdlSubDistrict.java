package model;

import java.util.List;

public class mdlSubDistrict {
	public String error;
	public String message;
	public List<model.mdlSubDistrictDetail> daftar_kecamatan;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<model.mdlSubDistrictDetail> getDaftar_kecamatan() {
		return daftar_kecamatan;
	}
	public void setDaftar_kecamatan(List<model.mdlSubDistrictDetail> daftar_kecamatan) {
		this.daftar_kecamatan = daftar_kecamatan;
	}
	
	
}
