package model;

import java.util.List;

public class mdlPush {

	 public mdlNotification notification;
	 public List<String> registration_ids;
	 public String priority;
	 public mdlData data;
	 public Boolean delay_while_idle;
	 public Boolean content_available;
	 
	public mdlNotification getNotification() {
		return notification;
	}
	public void setNotification(mdlNotification notification) {
		this.notification = notification;
	}
	public List<String> getRegistration_ids() {
		return registration_ids;
	}
	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public mdlData getData() {
		return data;
	}
	public void setData(mdlData data) {
		this.data = data;
	}
	public Boolean getDelay_while_idle() {
		return delay_while_idle;
	}
	public void setDelay_while_idle(Boolean delay_while_idle) {
		this.delay_while_idle = delay_while_idle;
	}
	public Boolean getContent_available() {
		return content_available;
	}
	public void setContent_available(Boolean content_available) {
		this.content_available = content_available;
	}

}
