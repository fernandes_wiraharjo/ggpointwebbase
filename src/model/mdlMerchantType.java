package model;

public class mdlMerchantType {

	public String MerchantTypeID;
	public String MerchantTypeName;
	public String MerchantTypeDescription;
	
	public String getMerchantTypeID() {
		return MerchantTypeID;
	}
	public void setMerchantTypeID(String merchantTypeID) {
		MerchantTypeID = merchantTypeID;
	}
	public String getMerchantTypeName() {
		return MerchantTypeName;
	}
	public void setMerchantTypeName(String merchantTypeName) {
		MerchantTypeName = merchantTypeName;
	}
	public String getMerchantTypeDescription() {
		return MerchantTypeDescription;
	}
	public void setMerchantTypeDescription(String merchantTypeDescription) {
		MerchantTypeDescription = merchantTypeDescription;
	}
	
	
}
