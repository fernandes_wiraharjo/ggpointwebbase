package model;

public class mdlRoleProduct {
	public String RoleProductID;
	public String RoleProductName;
	public String BrandID;
	
	public String getBrandID() {
		return BrandID;
	}
	public void setBrandID(String brandID) {
		BrandID = brandID;
	}
	public String getRoleProductID() {
		return RoleProductID;
	}
	public void setRoleProductID(String roleProductID) {
		RoleProductID = roleProductID;
	}
	public String getRoleProductName() {
		return RoleProductName;
	}
	public void setRoleProductName(String roleProductName) {
		RoleProductName = roleProductName;
	}
	
	
}
