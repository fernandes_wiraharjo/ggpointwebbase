package model;

public class mdlTransaction {

	public String TransactionID;
	public String TransactionDate;
	public String MerchantID;
	public String MerchantName;
	public String MerchantPIC;
	public String MerchantArea;
	public String CustomerID;
	public String TransactionPrice;
	public Integer TransactionPoint;
	public String LastDate;
	public String sLastPrice;
	public Integer iLastPrice;
	public String ButtonStatus;
	
	public String getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}
	public String getTransactionDate() {
		return TransactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getMerchantName() {
		return MerchantName;
	}
	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}
	public String getMerchantPIC() {
		return MerchantPIC;
	}
	public void setMerchantPIC(String merchantPIC) {
		MerchantPIC = merchantPIC;
	}
	public String getMerchantArea() {
		return MerchantArea;
	}
	public void setMerchantArea(String merchantArea) {
		MerchantArea = merchantArea;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public String getTransactionPrice() {
		return TransactionPrice;
	}
	public void setTransactionPrice(String transactionPrice) {
		TransactionPrice = transactionPrice;
	}
	public Integer getTransactionPoint() {
		return TransactionPoint;
	}
	public void setTransactionPoint(Integer transactionPoint) {
		TransactionPoint = transactionPoint;
	}
	public String getLastDate() {
		return LastDate;
	}
	public void setLastDate(String lastDate) {
		LastDate = lastDate;
	}
	public String getsLastPrice() {
		return sLastPrice;
	}
	public void setsLastPrice(String sLastPrice) {
		this.sLastPrice = sLastPrice;
	}
	public Integer getiLastPrice() {
		return iLastPrice;
	}
	public void setiLastPrice(Integer iLastPrice) {
		this.iLastPrice = iLastPrice;
	}
	public String getButtonStatus() {
		return ButtonStatus;
	}
	public void setButtonStatus(String buttonStatus) {
		ButtonStatus = buttonStatus;
	}
	
	
}
