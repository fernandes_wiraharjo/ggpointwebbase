package model;

public class mdlRoleProductDetail {
	public String RoleProductID;	
	public String MerchantID;
	public String ProductID;
	public String Price;
	public String Point;

	public String getRoleProductID() {
		return RoleProductID;
	}
	public void setRoleProductID(String roleProductID) {
		RoleProductID = roleProductID;
	}
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
	public String getPoint() {
		return Point;
	}
	public void setPoint(String point) {
		Point = point;
	}
}
