package model;

import java.util.List;

public class mdlProductMerchant {

	public String ProductID;
	public String ProductName;
	public String[] listMerchantID;
	public String MerchantID;
	public String MerchantName;
	public String MerchantType;
	
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getMerchantName() {
		return MerchantName;
	}
	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}
	public String[] getListMerchantID() {
		return listMerchantID;
	}
	public void setListMerchantID(String[] listMerchantID) {
		this.listMerchantID = listMerchantID;
	}
	public String getMerchantType() {
		return MerchantType;
	}
	public void setMerchantType(String merchantType) {
		MerchantType = merchantType;
	}
	
	
}
