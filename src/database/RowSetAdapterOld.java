package database;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.eclipse.jdt.internal.compiler.ast.ThrowStatement;

/** Documentation
 * 
 */
public class RowSetAdapterOld {
	
	public static JdbcRowSet getJDBCRowSet() throws Exception{
			Class.forName("com.mysql.jdbc.Driver");
			String databaseUrl = "jdbc:mysql://localhost:3306/inventorywarehouse";
		    String username = "root";
		    String password = "";
			
			RowSetFactory rsFactory = RowSetProvider.newFactory();
			JdbcRowSet jRowSet = rsFactory.createJdbcRowSet(); 
			
			jRowSet.setUrl(databaseUrl);
			jRowSet.setUsername(username);
			jRowSet.setPassword(password);
			jRowSet.setReadOnly(false);
		
		return jRowSet;
	}

}
