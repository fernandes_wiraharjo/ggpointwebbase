package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BrandAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.MobileConfigAdapter;
import adapter.ProductAdapter;
import adapter.RoleAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns={"/mobileconfig"} , name="mobileconfig")
public class MobileConfigServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MobileConfigServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String brandID, result, resultDesc="";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		Boolean CheckMenu;
    	String MenuURL = "mobileconfig";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		//brand		
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);

    		model.mdlBrand mdlBrandFirst = BrandList.get(0);
    		if(brandID == null || brandID.equals("")){
    			brandID = mdlBrandFirst.BrandID;
    		}

    		List<model.mdlMobileConfig> MobileConfigList = new ArrayList<model.mdlMobileConfig>();
    		MobileConfigList.addAll(MobileConfigAdapter.LoadMobileConfigbyBrand(brandID, user));
    		request.setAttribute("listmobileconfig", MobileConfigList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		request.setAttribute("condition", result);
    		request.setAttribute("gBrandId", brandID);
    		request.setAttribute("errorDescription", resultDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/mobile_config.jsp");
    		dispacther.forward(request, response);
    		
    		brandID = "";
    	}
    	
		result = "";
		resultDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	String lResult = "";
    	
    	if (user == null || user == "")
    	{
    		return;
    	}

		//Declare Variable
    	String btnDelete = request.getParameter("btnDelete");
    	String keyBtn =  ValidateNull.NulltoStringEmpty(request.getParameter("key"));
    	
    	brandID = request.getParameter("slBrandId");
    	String slIsConfFeedback = request.getParameter("slIsConfFeedback");
    	String txtIsConfOneDayValue = request.getParameter("txtIsConfOneDay");
		String txtIsConfOneProductValue = request.getParameter("txtIsConfOneProduct");
		String txtIsConfQtyValue = request.getParameter("txtIsConfQty");
		String txtSummaryRange = request.getParameter("txtSummaryRange");
		String txtApkVersion = request.getParameter("txtApkVersion");
		
		List<model.mdlMobileConfig> listMbConfig = new ArrayList<model.mdlMobileConfig>();
		
		//IsConfFeedback
		listMbConfig.add(MobileConfigAdapter.addValueMdlMobileConfig(brandID, "APKVersion", txtApkVersion,"Versi APK"));
				
		//IsConfFeedback
		listMbConfig.add(MobileConfigAdapter.addValueMdlMobileConfig(brandID, "isConfFeedback", slIsConfFeedback,"Menu feedback pada mobile"));
		
		//IsConfOneDay
		listMbConfig.add(MobileConfigAdapter.addValueMdlMobileConfig(brandID, "isConfOneDay", txtIsConfOneDayValue,"Jumlah transaksi maksimal dalam 1 hari"));

		//IsConfOneProduct
		listMbConfig.add(MobileConfigAdapter.addValueMdlMobileConfig(brandID, "isConfOneProduct", txtIsConfOneProductValue,"Jumlah produk dalam 1 transaksi"));
		
		//IsConfQty
		listMbConfig.add(MobileConfigAdapter.addValueMdlMobileConfig(brandID, "isConfQty", txtIsConfQtyValue,"Jumlah kuantiti maksimal per produk"));
	
		//Summary Tx Range Date
		listMbConfig.add(MobileConfigAdapter.addValueMdlMobileConfig(brandID, "SummaryTxDateRange", txtSummaryRange,"Penentuan date range period untuk summary transaction (minggu)"));
		
		if(keyBtn.equals("LoadMobileConfig")){
			//Mobile Config
//			List<model.mdlMobileConfig> MobileConfigList = new ArrayList<model.mdlMobileConfig>();
//			MobileConfigList.addAll(MobileConfigAdapter.LoadMobileConfigbyBrand(Globals.gBrandId));
//			request.setAttribute("listmobileconfig", MobileConfigList);
//			
//			RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/mobile_config.jsp");
//			dispacther.forward(request, response);
			return;
		}
		
		if (keyBtn.equals("save")){
			
			lResult =  MobileConfigAdapter.InsertMobileConfig(listMbConfig, user);
			
			if(lResult.contains("Success Insert Mobile Config")) {  
				result = "SuccessInsertMobileConfig";
		    }  
		    else {
		        result = "FailedInsertMobileConfig"; 
		        resultDesc = lResult;
		    }
		}
		
		if (keyBtn.equals("update")){
			lResult = MobileConfigAdapter.UpdateMobileConfig(listMbConfig, user);
			if(lResult.contains("Success Update Mobile Config")) {  
				result = "SuccessUpdateMobileConfig";
		    }  
		    else {
		        result = "FailedUpdateMobileConfig"; 
		        resultDesc = lResult;
		    }
		}
		
		if (btnDelete != null){
			lResult = MobileConfigAdapter.DeleteMobileConfig(brandID, user);
			
			if(lResult.contains("SuccessDelete")) {  
				result = "SuccessDelete";
		    }  
		    else {
		        result = "FailedDelete"; 
		        resultDesc = lResult;
		    }
			
			doGet(request, response);
			return;
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
	}
}
