package com.ggmerchant;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.DateRangeAdapter;
import adapter.ProvinceAdapter;
import helper.ConvertDateTimeHelper;

@WebServlet(urlPatterns={"/getdaterange"} , name="getdaterange")
public class GetDateRangeServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetDateRangeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		String lWeek = request.getParameter("week");
    	String dateNow = LocalDateTime.now().toString();
    	model.mdlDateRange lDateRange = new model.mdlDateRange();
    	String lStartDate = "";
    	String lEndDate = "";
    	
    	if(lWeek.contentEquals("1"))
		{
			lDateRange = DateRangeAdapter.LoadDateRange(dateNow, user);
			lStartDate = ConvertDateTimeHelper.formatDate(lDateRange.StartDate, "yyyy-MM-dd", "dd MMM yyyy");
			lEndDate = ConvertDateTimeHelper.formatDate(lDateRange.EndDate, "yyyy-MM-dd", "dd MMM yyyy");
		}
		else if(lWeek.contentEquals("2"))
		{
			lDateRange = DateRangeAdapter.LoadDateRange2(dateNow, user);
			lStartDate = ConvertDateTimeHelper.formatDate(lDateRange.StartDate, "yyyy-MM-dd", "dd MMM yyyy");
			lEndDate= ConvertDateTimeHelper.formatDate(lDateRange.EndDate, "yyyy-MM-dd", "dd MMM yyyy");
		}
		else if(lWeek.contentEquals("4"))
		{
			lDateRange = DateRangeAdapter.LoadDateRange1Month();
			lStartDate = lDateRange.StartDate;
			lEndDate = lDateRange.EndDate;
		}
    	
    	response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write("("+lStartDate+" - "+lEndDate+")");
	}
	
}
