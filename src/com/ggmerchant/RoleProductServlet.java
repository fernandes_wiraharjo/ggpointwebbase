package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BrandAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.ProductAdapter;
import adapter.RoleAdapter;
import adapter.RoleProductAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

/** Documentation
 * 001 nanda - Pembuatan Product Role Module
 */
@WebServlet(urlPatterns={"/roleproduct"} , name="roleproduct")
public class RoleProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RoleProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String result,resultDesc;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		model.mdlUser mdlUser = new model.mdlUser();
		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
		mdlUser.setUserId(user);
		
		//Product Role
		List<model.mdlRoleProduct> RoleProductList = new ArrayList<model.mdlRoleProduct>();
		RoleProductList.addAll(RoleProductAdapter.LoadRoleProduct(user));
		request.setAttribute("listroleproduct", RoleProductList);	
		
		//-- load all Brand list
		List<model.mdlBrand> mdlBrand = new ArrayList<model.mdlBrand>();
		mdlBrand.addAll(BrandAdapter.LoadBrand(mdlUser));
		request.setAttribute("listbrand", mdlBrand);
		//end of load all Brand list --

		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/role_product.jsp");
		dispacther.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    		
    	//Declare 
    	String keyBtn = ValidateNull.NulltoStringEmpty(request.getParameter("key"));
    	String btnDelete = request.getParameter("btnDelete");
    	String txtRoleProductId = request.getParameter("txtRoleProductId");
    	String txtRoleProductName = request.getParameter("txtRoleProductName");
    	String temp_txtRoleProductID = request.getParameter("temp_txtRoleProductID");
    	
		if (keyBtn.equals("saveroleproduct")){
			//Declare parameter
	    	
	    	String slBrand = request.getParameter("slBrand");
	    	
	    	
	    	model.mdlRoleProduct mdlRoleProduct = new model.mdlRoleProduct();
	    	mdlRoleProduct.setRoleProductID(txtRoleProductId);
	    	mdlRoleProduct.setRoleProductName(txtRoleProductName);
	    	mdlRoleProduct.setBrandID(slBrand);
	    	String lResult = RoleProductAdapter.InsertRoleProduct(mdlRoleProduct, user);
	    	
	    	if(lResult.contains("Success Insert Role Product")) {  
				result = "SuccessInsertRoleProduct";
            }  
            else {
            	result = "FailedInsertRoleProduct";
            	resultDesc = lResult;
            }	
	    	return;
		}
		
		if (keyBtn.equals("updaterule")){
			//Declare parameter
	    	String[] listMechant = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMerchant[]"));
	    	String[] listProduct = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedProduct[]"));

	    	String lResult = "";
	    	
	    	List<model.mdlRoleProductDetail> mdlRoleProductDetailList = new ArrayList<model.mdlRoleProductDetail>();
	    	
	    	if (listMechant.length == 0) {
	    		lResult = RoleProductAdapter.UpdateRoleProductDetail(mdlRoleProductDetailList,txtRoleProductId,txtRoleProductName,user);
	    	}
	    	else
	    	{
	    		for (String lMechantParam : listMechant) {           
	    			for (String lProductParam: listProduct) {
	    				model.mdlRoleProductDetail mdlRoleProductDetail = new model.mdlRoleProductDetail();
	    				mdlRoleProductDetail.setRoleProductID(txtRoleProductId);
	    				mdlRoleProductDetail.setMerchantID(lMechantParam);
	    				mdlRoleProductDetail.setProductID(lProductParam);
	    				mdlRoleProductDetail.setPrice("0");
	    				mdlRoleProductDetail.setPoint("0");

	    				mdlRoleProductDetailList.add(mdlRoleProductDetail);
	    			}
	    			lResult = RoleProductAdapter.UpdateRoleProductDetail(mdlRoleProductDetailList,txtRoleProductId,txtRoleProductName,user);
	    		}
	    	}
					
					
	    	if(lResult.contains("Success Update Role Product")) {  
				result = "SuccessUpdateRoleProduct";
            }  
            else {
            	result = "FailedUpdateRoleProduct";
            	resultDesc = lResult;
            }	
	    	return;
		}
		
		if (btnDelete != null){
			String temp_txtRoleID = ValidateNull.NulltoStringEmpty(request.getParameter("temp_txtRoleProductID"));
			String lResult = RoleProductAdapter.DeleteRoleProduct(temp_txtRoleID, user);
			if(lResult.contains("Success Delete User Rule")) {  
				result =  "SuccessDelete";
            }  
            else {
            	result = "FailedDelete"; 
            	resultDesc = lResult;
            }
			
			doGet(request, response);
			
			return;
		}
	}

}
