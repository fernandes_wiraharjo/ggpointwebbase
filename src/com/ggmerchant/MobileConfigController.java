package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.MobileConfigAdapter;
import adapter.ProductAdapter;
import adapter.ProductMerchantAdapter;
import adapter.RoleProductAdapter;
import adapter.ValidateNull;
import model.Globals;
import model.mdlProductMerchant;

@WebServlet(urlPatterns={"/mobileconfigcontroller"} , name="mobileconfigcontroller")
public class MobileConfigController extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public MobileConfigController() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	String slBrandId = ValidateNull.NulltoStringEmpty(request.getParameter("slBrandId"));
    	String lCommand = request.getParameter("command");
    	List<model.mdlMobileConfig> mdlMobileConfigList = new ArrayList<model.mdlMobileConfig>();
    	
    	Gson gson = new Gson();
    	String json = "";
    	
        if (lCommand.equals("getMobileConfig")){
        	//mdlProductBrandList.addAll(ProductAdapter.LoadProduct_BrandbyProductID(lProductID));
        	mdlMobileConfigList=MobileConfigAdapter.LoadMobileConfigbyBrand(slBrandId, user);
        	json = gson.toJson(mdlMobileConfigList);
    	}
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}
	
}
