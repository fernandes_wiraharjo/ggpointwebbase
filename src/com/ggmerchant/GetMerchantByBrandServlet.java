package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MerchantAdapter;
import adapter.UserAdapter;

@WebServlet(urlPatterns={"/getmerchantbybrand"} , name="getmerchantbybrand")
public class GetMerchantByBrandServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetMerchantByBrandServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		model.mdlUser mdlUser = new model.mdlUser();
		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
		mdlUser.setUserId(user);
		
		String lBrandID = request.getParameter("brandid");
		String lProductID = request.getParameter("productid");
//		String lMerchantName = request.getParameter("merchantname");
//		String lMerchantType = request.getParameter("merchanttype");
		
		List<model.mdlMerchant> mdlMerchantList = new ArrayList<model.mdlMerchant>();
		mdlMerchantList.addAll(MerchantAdapter.LoadMerchantbyBrandID(lBrandID,lProductID,mdlUser));
//		,lMerchantName,lMerchantType
		
		request.setAttribute("listmerchant", mdlMerchantList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getMerchantByBrand.jsp");
		dispacther.forward(request, response);
	}
	
}
