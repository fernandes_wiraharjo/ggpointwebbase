package com.ggmerchant;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.IssueAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/dashboard"} , name="dashboard")
public class DashboardServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public DashboardServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//set regard for welcome page
    	String lRegard = "";
    	Calendar c = Calendar.getInstance();
    	int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

    	if(timeOfDay >= 0 && timeOfDay < 12){
    	   lRegard = "Good Morning";       
    	}else if(timeOfDay >= 12 && timeOfDay < 16){
    		lRegard = "Good Afternoon";  
    	}else if(timeOfDay >= 16 && timeOfDay < 21){
    		lRegard = "Good Evening";  
    	}else if(timeOfDay >= 21 && timeOfDay < 24){
    		lRegard = "Good Night";  
    	}
    	
    	request.setAttribute("Regard", lRegard);
    	
    	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/dashboard.jsp");
		dispacther.forward(request, response);
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
    
}
