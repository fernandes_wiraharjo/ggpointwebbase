package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BrandAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.RoleAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

/** Documentation
 * 001 nanda - Pembuatan User Module
 */
@WebServlet(urlPatterns={"/user"} , name="user")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String result, resultDesc;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	
		Boolean CheckMenu;
    	String MenuURL = "user";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand2(user);
    		mdlUser.setUserId(user);
    		//User
    		List<model.mdlUser> UserList = new ArrayList<model.mdlUser>();
    		UserList.addAll(UserAdapter.LoadUser(mdlUser));
    		request.setAttribute("listuser", UserList);
    		
    		//Area
    		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
    		AreaList.addAll(AreaAdapter.LoadAreaMenuUser(user));
    		request.setAttribute("listarea", AreaList);		
    		
    		//Brand
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrandMenuUser(user));
    		request.setAttribute("listbrand", BrandList);
    		
    		//Role
    		List<model.mdlRole> RoleList = new ArrayList<model.mdlRole>();
    		RoleList.addAll(RoleAdapter.LoadRole(user));
    		request.setAttribute("listrole", RoleList);
    		
    		mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		//Merchant
    		List<model.mdlMerchant> MerchantList = new ArrayList<model.mdlMerchant>();
    		MerchantList.addAll(MerchantAdapter.LoadMerchant(mdlUser));
    		
    		request.setAttribute("listmerchant", MerchantList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		
    		request.setAttribute("condition", result);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		request.setAttribute("errorDescription", resultDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/user.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	String lResult = "";
    	
    	if (user == null || user == "")
    	{ 
    		return;
    	}

		
		//Declare Variable
    	String btnDelete = request.getParameter("btnDelete");
    	String keyBtn =  ValidateNull.NulltoStringEmpty(request.getParameter("key"));
    	String temp_txtUserId = request.getParameter("temp_txtUserId");
    	String txtUserID = request.getParameter("txtUserID");
		String txtUserName = request.getParameter("txtUserName");
		String txtPassword = request.getParameter("txtPassword");
		String slRole = request.getParameter("slRole");
		String UserType = request.getParameter("DropDown");
		
		
		String[] listArea = request.getParameterValues("arealist[]");
		String arealist = String.join(",",ValidateNull.NulltoStringArrayEmpty(listArea));
		
		String[] listBrand = request.getParameterValues("brandlist[]");
		String brandlist = String.join(",",ValidateNull.NulltoStringArrayEmpty(listBrand));

		
		model.mdlUser mdlUser = new model.mdlUser();
		mdlUser.setUserId(txtUserID);
		mdlUser.setUsername(txtUserName);
		mdlUser.setPassword(txtPassword);
		mdlUser.setRole(slRole);
		mdlUser.setArea(arealist);
		mdlUser.setBrand(brandlist);
		mdlUser.setType(UserType);
		
		if (keyBtn.equals("save")){
			lResult = UserAdapter.InsertUser(mdlUser, user);
			
			if(lResult.contains("SuccessInsert")) {  
				result = "SuccessInsertUser";
            }  
            else {
            	result = "FailedInsertUser";
            	resultDesc = lResult;
            }
		}
		
		if (keyBtn.equals("update")){
			lResult = UserAdapter.UpdateUser(mdlUser, user);
			
			if(lResult.contains("SuccessUpdate")) {  
				result = "SuccessUpdateUser";
            }  
            else {
            	result = "FailedUpdateUser";
            	resultDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			lResult = UserAdapter.DeleteUser(temp_txtUserId, user);
			
			if(lResult.contains("SuccessDelete")) {  
				result =  "SuccessDelete";
            }  
            else {
            	result = "FailedDelete"; 
            	resultDesc = lResult;
            }
			
			doGet(request, response);
			return;
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
	}
}
