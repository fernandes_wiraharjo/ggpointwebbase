package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.TransactionAdapter;

@WebServlet(urlPatterns={"/getPaymentInfoMerchant"} , name="getPaymentInfoMerchant")
public class GetPaymentInfoMerchantServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetPaymentInfoMerchantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		String lMerchantID = request.getParameter("merchantid");
    	List<model.mdlTransactionPayment> mdlTransactionPaymentList = new ArrayList<model.mdlTransactionPayment>();
    	
    	mdlTransactionPaymentList.addAll(TransactionAdapter.LoadTransactionPaymentByMerchant(lMerchantID,user));
		
		request.setAttribute("listpaymentinfo", mdlTransactionPaymentList);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/getPaymentInfo.jsp");
		dispacther.forward(request, response);
		
	}
	
}
