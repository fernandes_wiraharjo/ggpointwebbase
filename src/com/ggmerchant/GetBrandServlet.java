package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.BrandAdapter;
import adapter.ProvinceAdapter;

@WebServlet(urlPatterns={"/getbrand"} , name="getbrand")
public class GetBrandServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetBrandServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		String lProductID = request.getParameter("productid");
    	List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    	BrandList = BrandAdapter.LoadBrandbyProductID(lProductID, user);
    	
    	Gson gson = new Gson();
    	
    	String jsonlistBrand = gson.toJson(BrandList);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistBrand);
	}
	
}
