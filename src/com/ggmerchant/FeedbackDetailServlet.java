package com.ggmerchant;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import adapter.FeedbackAdapter;
import adapter.IssueAdapter;
import adapter.MerchantAdapter;
import adapter.PushNotificationAdapter;
import adapter.RefreshFolderAdapter;
import adapter.ValidateNull;
import adapter.WriteFileAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlData;

@WebServlet(urlPatterns={"/feedback_detail"} , name="feedback_detail")
@MultipartConfig
public class FeedbackDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public FeedbackDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    

    String lFeedbackID,lFile,lDate,lDescription,lStatus,lType,lMerchantName,lPIC,lUsername;
    
    //temp variable for failed condition, because it will be loaded at the same page so we store the variable above to here at the post method
    String templFeedbackID,templFile,templDate,templDescription,
    	   templStatus,templType,templMerchantName,templPIC,templUsername,
    	   result,resultDesc;
   
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//declare variable
    	lFeedbackID = request.getParameter("FeedbackID");
    	
    	if(lFeedbackID==null)
    	{
    		lFeedbackID = templFeedbackID;
    		lFile = templFile;
    		lDate = templDate;
    		lDescription = templDescription;
    		lStatus = templStatus;
    		lType = templType;
    		lMerchantName = templMerchantName;
    		lPIC = templPIC;
    		lUsername = templUsername;
    	}
    	else
    	{
    		lFeedbackID = request.getParameter("FeedbackID");
    		lFile = request.getParameter("File");
    		lDate = ConvertDateTimeHelper.formatDate(request.getParameter("Date"), "dd-MM-yyyy HH:mm", "dd/MM/yyyy HH:mm");
    		lDescription = request.getParameter("Description");
    		lStatus = request.getParameter("Status");
    		lType = request.getParameter("Type");
    		lMerchantName = request.getParameter("MerchantName");
    		lPIC = request.getParameter("PIC");
    		lUsername = request.getParameter("Username");
    	}
    	
    	//send variable to jsp jstl via servlet 
    	request.setAttribute("feedbackid", lFeedbackID);
    	request.setAttribute("file", lFile);
    	request.setAttribute("date", lDate);
    	
    	//request.setAttribute("description", lDescription);
    	HttpSession session = request.getSession();
		session.setAttribute("description", lDescription);
		session.setMaxInactiveInterval(30*60);
		
    	request.setAttribute("status", lStatus);
    	request.setAttribute("type", lType);
    	request.setAttribute("merchantname", lMerchantName);
    	request.setAttribute("pic", lPIC);
    
		
    	//for the failed condition
    	request.setAttribute("condition", result);
		request.setAttribute("errorDescription", resultDesc);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/feedback_detail.jsp");
		dispacther.forward(request, response);
		
		result = "";
		resultDesc = "";
		
		//declare temporary variable
    	templFeedbackID = lFeedbackID;
    	templFile = lFile;
    	templDate = lDate;
    	templDescription = lDescription;
    	templStatus = lStatus;
    	templType = lType;
    	templMerchantName = lMerchantName;
    	templPIC = lPIC;
    	templUsername = lUsername;
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	//Declare Parameter from ajax
		String FeedbackID = request.getParameter("feedbackid");
		String Reason = request.getParameter("txtReason").replace("\r\n", "<br>");
		
		//Declare mdlMerchantInbox for global
		model.mdlFeedback mdlFeedback2 = new model.mdlFeedback();
		mdlFeedback2.setFeedbackID(FeedbackID);
		mdlFeedback2.setContestReason(Reason);
				
		String lResult = "";
		lResult = FeedbackAdapter.UpdateFeedback(mdlFeedback2,user);
			
		if(lResult.contains("Success Close Feedback")) {  
			result = "SuccessCloseFeedback";
				
			//-- push notif to mobile
//			String apiKey = "AIzaSyCfHAaH-vkJwA_Tf_ffDDckvhBMG4dAbKc";
			String apiKey = "AAAANA_Uduk:APA91bETeqh3rCeuLxlKrhsBVMFpjzZ2PiBuW-Enqt0Im3t2HlSKPOYUqMfsCyDchVMRYKnKVchQljpBaYk4LJjMTpwrWHZKCmwISrE035hdFXkVZoIVbdmC_uxutqlRwxQxZ_43zJm1";
			String notificationID = user+"_PushContestFeedback_"+lMerchantName+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
			model.mdlPush mdlPush = new model.mdlPush();
				
			model.mdlNotification mdlNotif = new model.mdlNotification();
			mdlNotif.setTitle("Send Feedback Contest Message"); 
			mdlNotif.setBody("You got a new message");
				
			mdlData mdlData = new model.mdlData();
			mdlData.setTitle("Send Feedback Contest Message");
			mdlData.setMsg("You got a new message"); 
			mdlData.setNotificationid(notificationID);
			List<String> userToken = new ArrayList<String>();
			userToken = PushNotificationAdapter.LoadTokenByUsername(lUsername,user);
				
			mdlPush.setRegistration_ids(userToken);
			mdlPush.setPriority("high");
			mdlPush.setDelay_while_idle(false);
			mdlPush.setContent_available(true);
			mdlPush.setData(mdlData);
				
			PushNotificationAdapter.PostToGCMGeneral(apiKey, mdlPush);
			//end of push notif --
			
			//if success, back to the issue menu
			response.sendRedirect(request.getContextPath() + "/feedback");
	    }  
	    else {
	        result = "FailedCloseFeedback"; 
	        resultDesc = lResult;
		        
	      //if failed, stay in the same page
	        doGet(request, response);
	    } 
		return;
	}
    
}
