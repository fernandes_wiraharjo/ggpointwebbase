package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BrandAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.ProductAdapter;
import adapter.RoleAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns={"/product"} , name="product")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String result,resultDesc;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		Boolean CheckMenu;
    	String MenuURL = "product";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		//Product
    		List<model.mdlProduct> ProductList = new ArrayList<model.mdlProduct>();
    		ProductList.addAll(ProductAdapter.LoadProduct(mdlUser));
    		request.setAttribute("listproduct", ProductList);
    		
    		//Brand
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		request.setAttribute("condition", result);
//    		request.setAttribute("errorDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/product.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	String lResult = "";
    	
    	if (user == null || user == "")
    	{
    		return;
    	}

		//Declare Variable
    	String btnDelete = request.getParameter("btnDelete");
    	String keyBtn =  ValidateNull.NulltoStringEmpty(request.getParameter("key"));
    	
    	String temp_txtProductId = request.getParameter("temp_txtProductId");
    	String txtProductID = request.getParameter("txtProductID");
		String txtProductName = request.getParameter("txtProductName");
		String txtPrice = request.getParameter("txtPrice").replace("Rp. ", "").replace(".", "").replace(" ,-", "");
		String slStatus = request.getParameter("slStatus");
		String [] slBrandId = request.getParameterValues("slBrandId[]");
//		String txtPoint = request.getParameter("txtPoint"); 
//		String [] slMerchantId = request.getParameterValues("slMerchantId[]");
//		String merchantlist = String.join(",",ValidateNull.NulltoStringArrayEmpty(slMerchantId));

		
		model.mdlProduct mdlProduct = new model.mdlProduct();
		mdlProduct.setProductID(txtProductID);
		mdlProduct.setProductName(txtProductName);
		mdlProduct.setPrice(txtPrice);
		mdlProduct.setStatus(slStatus);
		
//		mdlProduct.setPoint(txtPoint);
//		mdlProduct.setBrandID(slBrand);
//		mdlProduct.setMerchantID(merchantlist);
		
		if (keyBtn.equals("save")){
			lResult =  ProductAdapter.InsertProduct(mdlProduct,slBrandId, user);
			
			if(lResult.contains("SuccessInsert")) {  
				result = "SuccessInsertProduct";
		    }  
		    else {
		        result = "FailedInsertProduct"; 
		        resultDesc = lResult;
		    }
		}
		
		if (keyBtn.equals("update")){
			lResult = ProductAdapter.UpdateProduct(mdlProduct,slBrandId, user);
			if(lResult.contains("SuccessUpdate")) {  
				result = "SuccessUpdateProduct";
		    }  
		    else {
		        result = "FailedUpdateProduct"; 
		        resultDesc = lResult;
		    }
		}
		
		if (btnDelete != null){
			lResult = ProductAdapter.DeleteProduct(temp_txtProductId, user);
			
			if(lResult.contains("SuccessDelete")) {  
				result = "SuccessDelete";
		    }  
		    else {
		    	result = "FailedDelete"; 
		    	resultDesc = lResult;
		    }
			
			doGet(request, response);
			return;
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
	}
}
