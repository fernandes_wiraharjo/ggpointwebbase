package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.RoleAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns={"/userrole"} , name="userrole")
public class UserRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserRoleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    String result, resultDesc;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		Boolean CheckMenu;
    	String MenuURL = "userrole";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		//User Role
    		List<model.mdlRole> UserRoleList = new ArrayList<model.mdlRole>();
    		UserRoleList.addAll(RoleAdapter.LoadRole(user));
    		request.setAttribute("listuserrole", UserRoleList);	
    		
    		//-- load all menu list
    		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
    		mdlMenuList.addAll(MenuAdapter.LoadMenu(user));

    		request.setAttribute("listmenu", mdlMenuList);
    		//end of load all menu list --
    		
    		//-- load editable menu list
    		List<model.mdlMenu> EditableMenulist = new ArrayList<model.mdlMenu>();
    		EditableMenulist.addAll(MenuAdapter.LoadEditableMenu(user));

    		request.setAttribute("listeditablemenu", EditableMenulist);
    		//end of load editable menu list --
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		
    		request.setAttribute("condition", result);
    		request.setAttribute("buttonstatus", ButtonStatus);
//    		request.setAttribute("errorDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/user_role.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	String lResult = "";
    	
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    		
    	//Declare button
    	String keyBtn = ValidateNull.NulltoStringEmpty(request.getParameter("key"));
    	String btnDelete = request.getParameter("btnDelete");
    	
    	
		if (keyBtn.equals("saverule")){
			//Declare parameter
			String[] listAllMenu = request.getParameterValues("listAllMenu[]");
	    	String[] listAllowedMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMenu[]"));
	    	String[] listEditableMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listEditableMenu[]"));
	    	String txtRoleId = request.getParameter("txtRoleId");
	    	String txtRoleName = request.getParameter("txtRoleName");
	    	
			lResult = MenuAdapter.InsertUserRule(txtRoleId, txtRoleName, listAllMenu, listAllowedMenu, listEditableMenu, user);

	    	
	    	if(lResult.contains("SuccessInsert")) {  
				result = "SuccessInsertUserRole";
            }  
            else {
            	result = "FailedInsertUserRole";
            	resultDesc = lResult;
            }
		}
		
		if (keyBtn.equals("updaterule")){
			//Declare parameter
			String[] listAllMenu = request.getParameterValues("listAllMenu[]");
	    	String[] listAllowedMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listAllowedMenu[]"));
	    	String[] listEditableMenu = ValidateNull.NulltoStringArrayEmpty(request.getParameterValues("listEditableMenu[]"));
	    	String txtRoleId = request.getParameter("txtRoleId");
	    	String txtRoleName = request.getParameter("txtRoleName");

			lResult = MenuAdapter.UpdateUserRule(txtRoleId, txtRoleName, listAllMenu, listAllowedMenu, listEditableMenu, user);

	    	if(lResult.contains("SuccessUpdate")) {  
				result = "SuccessUpdateUserRole";
            }  
            else {
            	result = "FailedUpdateUserRole";
            	resultDesc = lResult;
            }
		}
		
		if (btnDelete != null){
			String temp_txtRoleID = ValidateNull.NulltoStringEmpty(request.getParameter("temp_txtRoleID"));
			lResult = MenuAdapter.DeleteUserRule(temp_txtRoleID, user);
			
			if(lResult.contains("SuccessDelete")) {  
				result =  "SuccessDelete";
            } 
            else {
            	result = "FailedDelete"; 
            	resultDesc = lResult;
            }
			
			doGet(request, response);
			
			return;
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
	}

}
