package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.ProductAdapter;
import adapter.ProductMerchantAdapter;
import adapter.RoleProductAdapter;
import adapter.ValidateNull;
import model.Globals;
import model.mdlProductMerchant;

@WebServlet(urlPatterns={"/productcontroller"} , name="productcontroller")
public class ProductController extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public ProductController() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		String lRoleProductID = ValidateNull.NulltoStringEmpty(request.getParameter("roleproductid"));
    	String lMerchantID = ValidateNull.NulltoStringEmpty(request.getParameter("merchantid"));
    	String lProductID = ValidateNull.NulltoStringEmpty(request.getParameter("productid"));
    	String lPrice = ValidateNull.NulltoStringEmpty(request.getParameter("price"));
    	String lBrandID = ValidateNull.NulltoStringEmpty(request.getParameter("brand"));
    	
    	String lCommand = request.getParameter("command");
    	List<model.mdlProductBrand> mdlProductBrandList = new ArrayList<model.mdlProductBrand>();

    	model.mdlProductMerchant mdlProductMerchant = new model.mdlProductMerchant();
//    	model.mdlResult mdlResult = new model.mdlResult();
    	
    	Gson gson = new Gson();
    	String json = "";
    	

        if (lCommand.equals("Update")){
        	mdlProductBrandList.addAll(ProductAdapter.LoadProduct_BrandbyProductID(lProductID, user));
        	json = gson.toJson(mdlProductBrandList);
    	}
        
        if (lCommand.equals("CheckProductMerchantbyBrand")){
        	String lResult = ProductMerchantAdapter.CheckProductMerchantbyBrand(lProductID, lBrandID, user);
        	mdlProductMerchant.setMerchantID(lResult);
        	json = gson.toJson(mdlProductMerchant);
    	}
        
//    	if (lCommand.equals("LoadMerchantbyBrand")){
//		mdlMerchantList.addAll(MerchantAdapter.LoadMerchantbyBrandID(slbrand));
//		json = gson.toJson(mdlMerchantList);
//	}
        
//        if (lCommand.equals("ConvertPoint")){
//        	lPoint = RoleProductAdapter.GetPointPrice(slbrand, Double.parseDouble(lPrice));
//        	mdlRoleProductDetail.setPoint(lPoint);
//        	json = gson.toJson(mdlRoleProductDetail);
//    	}
        
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}
	
}
