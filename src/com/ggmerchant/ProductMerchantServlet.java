package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.ProductAdapter;
import adapter.ProductMerchantAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;

@WebServlet(urlPatterns={"/product_merchant"} , name="product_merchant")
public class ProductMerchantServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public ProductMerchantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String result, resultDesc;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	Boolean CheckMenu;
    	String MenuURL = "product_merchant";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		//Product
    		List<model.mdlProduct> ProductList = new ArrayList<model.mdlProduct>();
    		ProductList.addAll(ProductAdapter.LoadProduct(mdlUser));
    		request.setAttribute("listproduct", ProductList);
    		
    		//Merchant Product
    		List<model.mdlProductMerchant> MerchantProductList = new ArrayList<model.mdlProductMerchant>();
    		MerchantProductList.addAll(ProductMerchantAdapter.LoadMerchantProduct(mdlUser));
    		request.setAttribute("listmerchantproduct", MerchantProductList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		request.setAttribute("condition", result);
//    		request.setAttribute("errorDescription", Globals.gConditionDesc);
    		
    		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/product_merchant.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
    	
    	if (user == null || user == "")
    	{ 
    		return;
    	}
		
		//Declare Parameter
		String btnDelete = request.getParameter("btnDelete");
		String keyBtn =  ValidateNull.NulltoStringEmpty(request.getParameter("key"));
		String ProductID = request.getParameter("ProductID");
		String BrandID = request.getParameter("BrandID");
		String[] listMerchant = request.getParameterValues("listMerchant[]");
		String listMerchantProduct = request.getParameter("temp_ID");
//		String tempProductID = request.getParameter("temp_txtProductId");
//		String tempMerchantID = request.getParameter("temp_txtMerchantId");
		
		String lResult = "";
		
		if (keyBtn.equals("save")){
			lResult = ProductMerchantAdapter.InsertMerchantProduct(ProductID,listMerchant, user);
			
			if(lResult.contains("Success Insert Merchant Product")) {  
				result = "SuccessInsertMerchantProduct";
		    }  
		    else {
		        result = "FailedInsertMerchantProduct"; 
		        resultDesc = lResult;
		    }
		}
		
		if (btnDelete != null){
			lResult = ProductMerchantAdapter.DeleteMerchantProduct(listMerchantProduct, user);
			
			if(lResult.contains("Success Delete Merchant Product")) {  
				result = "SuccessDelete";
		    }  
		    else {
		        result = "FailedDelete"; 
		        resultDesc = lResult;
		    }

			doGet(request, response);
			return;
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
    }

}
