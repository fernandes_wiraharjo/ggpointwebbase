package com.ggmerchant;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BrandAdapter;
import adapter.MenuAdapter;
import adapter.PaymentAdapter;
import adapter.PushNotificationAdapter;
import adapter.UserAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlData;

@WebServlet(urlPatterns={"/verification_doc"} , name="verification_doc")
public class VerificationDocServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
    
    public VerificationDocServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    String BrandID="",AreaID="",tempBrandID="",tempAreaID="",result="",resultDesc="";
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	
    	Boolean CheckMenu;
    	String MenuURL = "verification_doc";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		List<model.mdlTransactionPayment> PaymentApprovedList = new ArrayList<model.mdlTransactionPayment>();
        	if(BrandID.contentEquals("") && AreaID.contentEquals(""))
        		request.setAttribute("listverification", PaymentApprovedList);
        	else
        	{
        		PaymentApprovedList.addAll(PaymentAdapter.LoadPaymentApprovedByParam(BrandID,AreaID,mdlUser));
        		request.setAttribute("listverification", PaymentApprovedList);
        	}
    		
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);
    		
    		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
    		AreaList.addAll(AreaAdapter.LoadArea(mdlUser));
    		request.setAttribute("listarea", AreaList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		if(ButtonStatus.contentEquals("disabled"))
    			ButtonStatus = "none";
    		else
    			ButtonStatus = "";
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		request.setAttribute("selectedbrand", BrandID);
    		request.setAttribute("selectedarea", AreaID);
    		request.setAttribute("condition", result);
//    		request.setAttribute("errorDescription", Globals.gConditionDesc);
    		
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/verification_doc.jsp");
    		dispacther.forward(request, response);
    		
    		tempBrandID=BrandID;
    		tempAreaID=AreaID;
    		BrandID="";
    		AreaID="";
    	}
		
		result = "";
		resultDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
    	String user = (String) session.getAttribute("user");
    	
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    	
    	String lResult = "";
//    	String lResultPushInbox = "";
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		if(keyBtn.equals("sort")) {
			//Declare slBrand and slArea
	    	BrandID = request.getParameter("BrandID");
	    	AreaID = request.getParameter("AreaID");
	    	
	    	return;
		}
		
		if (keyBtn.equals("verify")){
			String RefNo = request.getParameter("refno");
			String PaymentDocID = request.getParameter("paymentdocid");
			String TransactionID = request.getParameter("transactionid");
			String MerchantID = request.getParameter("merchantid");
			String MerchantName = request.getParameter("merchantname");
			String MerchantUserName = request.getParameter("merchantusername");
			
			//Declare mdlTransactionPayment for global
			model.mdlTransactionPayment PaymentApproved = new model.mdlTransactionPayment();
			PaymentApproved.setPaymentID(PaymentDocID);
			PaymentApproved.setRefNo(RefNo);
			PaymentApproved.setTransactionID(TransactionID);
			PaymentApproved.setMerchantID(MerchantID);
			PaymentApproved.setMerchantName(MerchantName);
			PaymentApproved.setMerchantUserName(MerchantUserName);
			
			lResult = PaymentAdapter.VerifyTransactionPayment(PaymentApproved, user);
			
			if(lResult.contains("Success Verify Payment Document")) {  
				result =  "SuccessVerification";
			}
            else {
            	result = "FailedVerification";
            	resultDesc = lResult;
            }
			
			BrandID=tempBrandID;
			AreaID=tempAreaID;
			
			response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(result+"--"+resultDesc);
			
			return;
		}
		
    }
    
}
