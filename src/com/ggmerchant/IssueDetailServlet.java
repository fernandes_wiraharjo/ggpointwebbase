package com.ggmerchant;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import adapter.IssueAdapter;
import adapter.MerchantAdapter;
import adapter.PushNotificationAdapter;
import adapter.RefreshFolderAdapter;
import adapter.ValidateNull;
import adapter.WriteFileAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlData;

@WebServlet(urlPatterns={"/issue_detail"} , name="issue_detail")
@MultipartConfig
public class IssueDetailServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public IssueDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    String MessageID,MerchantID,MerchantName,Message,DateTime,MessageType,MerchantPIC,RefNo,Status,StatusDetail,MerchantUserName;
    Integer RedeemValue;
    
    //temp variable for failed condition, because it will be loaded at the same page so we store the variable above to here at the post method
    String tempMessageID,tempMerchantID,tempMerchantName,tempMessage,
    	   tempDateTime,tempMessageType,tempMerchantPIC,tempRefNo,tempStatus,
    	   tempStatusDetail,tempMerchantUserName,result,resultDesc;
    Integer tempRedeemValue;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	//declare variable
    	MessageID = request.getParameter("messageID");
    	
    	if(MessageID==null)
    	{
    		MessageID = tempMessageID;
        	MerchantID = tempMerchantID;
        	MerchantName = tempMerchantName;
        	Message = tempMessage;
        	DateTime = tempDateTime;
        	MessageType = tempMessageType;
        	MerchantPIC = tempMerchantPIC;
        	RefNo = tempRefNo;
        	Status = tempStatus;
        	StatusDetail = tempStatusDetail;
        	RedeemValue = tempRedeemValue;
        	MerchantUserName = tempMerchantUserName;
    	}
    	else
    	{
        	MerchantID = request.getParameter("merchantID");
        	MerchantName = request.getParameter("merchantName");
        	Message = request.getParameter("message");
        	DateTime = ConvertDateTimeHelper.formatDate(request.getParameter("datetime"), "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss");
        	MessageType = request.getParameter("messageType");
        	MerchantPIC = request.getParameter("merchantPIC");
        	RefNo = request.getParameter("refNo");
        	Status = request.getParameter("status");
        	StatusDetail = request.getParameter("statusDetail");
        	RedeemValue = Integer.parseInt(request.getParameter("redeemValue").trim());
        	MerchantUserName = request.getParameter("merchantusername");
    	}
    	
    	//send variable to jsp jstl via servlet 
    	request.setAttribute("messageid", MessageID);
    	request.setAttribute("merchantid", MerchantID);
    	request.setAttribute("merchantname", MerchantName);
    	request.setAttribute("message", Message);
    	request.setAttribute("datetime", DateTime);
    	request.setAttribute("messagetype", MessageType);
    	request.setAttribute("merchantpic", MerchantPIC);
    	request.setAttribute("refno", RefNo);
    	request.setAttribute("status", Status);
    	
//    	request.setAttribute("statusdetail", StatusDetail);
    	HttpSession session = request.getSession();
		session.setAttribute("statusdetail", StatusDetail);
		session.setMaxInactiveInterval(30*60);
    	
    	request.setAttribute("iredeemvalue", RedeemValue);
    	request.setAttribute("redeemvalue", "Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(RedeemValue) + " ,-");
		
    	//for the failed condition
    	request.setAttribute("condition", result);
		request.setAttribute("errorDescription", resultDesc);
		
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/issue_detail.jsp");
		dispacther.forward(request, response);
		
		result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
    	
    	//declare temporary variable to load the same data at the same page after error
		tempMessageID = this.MessageID;
    	tempMerchantID = this.MerchantID;
    	tempMerchantName = MerchantName;
    	tempMessage = Message;
    	tempDateTime = DateTime;
    	tempMessageType = MessageType;
    	tempMerchantPIC = MerchantPIC;
    	tempRefNo = RefNo;
    	tempStatus = Status;
    	tempStatusDetail = StatusDetail;
    	tempRedeemValue = this.RedeemValue;
    	tempMerchantUserName = MerchantUserName;
    	
    	//get the file location path from web.xml
    	String paramFilePath = getServletContext().getInitParameter("param_file_location");
    	
    	//path for client(mobile) access
    	String paramFileAccess = getServletContext().getInitParameter("param_file_access");
				
		//Declare Parameter
		String MessageID = request.getParameter("messageid");
		String MerchantID = request.getParameter("merchantid");
		String RedeemValue = request.getParameter("redeemvalue");
		String Reason = request.getParameter("txtReason").replace("\r\n", "<br>");
		Part File = request.getPart("InputFile");
		String FileName = Paths.get(File.getSubmittedFileName()).getFileName().toString();
		InputStream originalFile = File.getInputStream();
		String newFile = paramFilePath + MessageID + "_" + FileName;
		
		//write attachment to certain location
		String resultWriteFile = WriteFileAdapter.Write(originalFile , newFile);
		
		//refresh folder image
//		RefreshFolderAdapter.RefreshFolder();
		
		if(!resultWriteFile.equals("Error Write File")) 
		{
			//Declare mdlMerchantInbox for global
			model.mdlMerchantInbox CloseIssue = new model.mdlMerchantInbox();
			CloseIssue.setMessageID(MessageID);
			CloseIssue.setContestReason(Reason);
			CloseIssue.setContestFile(paramFileAccess+MessageID+"_"+FileName);
			CloseIssue.setTransactionPrice(Integer.parseInt(RedeemValue));
			CloseIssue.setMerchantID(MerchantID);
				
			String lResult = "";
			lResult = IssueAdapter.UpdateIssueMessage(CloseIssue, user);
			
			if(lResult.contains("Success Close Issue")) {  
				result = "SuccessCloseIssue";
				
				//-- push notif to mobile
//				String apiKey = "AIzaSyCfHAaH-vkJwA_Tf_ffDDckvhBMG4dAbKc";
				String apiKey = "AAAANA_Uduk:APA91bETeqh3rCeuLxlKrhsBVMFpjzZ2PiBuW-Enqt0Im3t2HlSKPOYUqMfsCyDchVMRYKnKVchQljpBaYk4LJjMTpwrWHZKCmwISrE035hdFXkVZoIVbdmC_uxutqlRwxQxZ_43zJm1";
				String notificationID = user+"_PushContest_"+MerchantName+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
				model.mdlPush mdlPush = new model.mdlPush();
				
				model.mdlNotification mdlNotif = new model.mdlNotification();
				mdlNotif.setTitle("Send Contest Message"); 
				mdlNotif.setBody("You got a new message");
				
				mdlData mdlData = new model.mdlData();
				mdlData.setTitle("Send Contest Message");
				mdlData.setMsg("You got a new message"); 
				mdlData.setNotificationid(notificationID);
				List<String> userToken = new ArrayList<String>();
				userToken = PushNotificationAdapter.LoadTokenByUsername(MerchantUserName, user);
				
				mdlPush.setRegistration_ids(userToken);
				mdlPush.setPriority("high");
				mdlPush.setDelay_while_idle(false);
				mdlPush.setContent_available(true);
				mdlPush.setData(mdlData);
				
				PushNotificationAdapter.PostToGCMGeneral(apiKey, mdlPush);
				//end of push notif --
				
				//if success, back to the issue menu
				response.sendRedirect(request.getContextPath() + "/issue");
		    }  
		    else {
		        result = "FailedCloseIssue"; 
		        resultDesc = lResult;
		        String resultDeleteFile = WriteFileAdapter.Delete(newFile);
		        if(resultDeleteFile.equals("Error Delete File"))
		        {
		        	result = "FailedCloseIssue"; 
			        resultDesc = lResult + " , but the attachment file may have been saved";
		        }
		        
		      //if failed, stay in the same page
		        doGet(request, response);
		    } 
		}
		else
		{
			result = "FailedCloseIssue"; 
	        resultDesc = "Attachment Error";
	        
	      //if failed, stay in the same page
	        doGet(request, response);
		}
		
		
		return;
	}
    
}
