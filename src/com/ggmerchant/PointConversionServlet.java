package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BankAdapter;
import adapter.BrandAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.ProvinceAdapter;
import adapter.UserAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/point_conversion"} , name="point_conversion")
public class PointConversionServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public PointConversionServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    String result,resultDesc;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	Boolean CheckMenu;
    	String MenuURL = "point_conversion";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		List<model.mdlPointConversion> PointConversionList = new ArrayList<model.mdlPointConversion>();
        	PointConversionList.addAll(BrandAdapter.LoadPointConversion(mdlUser));
    		request.setAttribute("listpointconversion", PointConversionList);
    		
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		
        	request.setAttribute("condition", result);
        	request.setAttribute("buttonstatus", ButtonStatus);
        	//request.setAttribute("errorDescription", Globals.gConditionDesc);
    		
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/point_conversion.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
				
		//Declare TextBox
		String BrandID = request.getParameter("BrandID");
		Integer Price = Integer.parseInt(request.getParameter("Price").replace(".", ""));
				
		//Declare mdlPointConversion for global
		model.mdlPointConversion PointConversion = new model.mdlPointConversion();
		PointConversion.setBrandID(BrandID);
		PointConversion.setPoint(1);
		PointConversion.setPrice(Price);
			
		String lResult = "";
		
		if (keyBtn.equals("save")){
			lResult = BrandAdapter.InsertPointConversion(PointConversion, user);
			
			if(lResult.contains("Success Insert Point Conversion")) {  
				result = "SuccessInsertPointConversion";
		    }  
		    else {
		        result = "FailedInsertPointConversion"; 
		        resultDesc = lResult;
		    }
		}
		
		if (keyBtn.equals("update")){
			lResult = BrandAdapter.UpdatePointConversion(PointConversion, user);
			
			if(lResult.contains("Success Update Point Conversion")) {  
				result = "SuccessUpdatePointConversion";
		    }  
		    else {
		        result = "FailedUpdatePointConversion"; 
		        resultDesc = lResult;
		    }
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
	}

}
