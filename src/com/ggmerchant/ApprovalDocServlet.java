package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BrandAdapter;
import adapter.MenuAdapter;
import adapter.PaymentAdapter;
import adapter.TransactionAdapter;
import adapter.UserAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/approval_doc"} , name="approval_doc")
public class ApprovalDocServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public ApprovalDocServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    String BrandID="", AreaID="", tempBrandID="", tempAreaID="", result="", resultDesc="";
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	Boolean CheckMenu;
    	String MenuURL = "approval_doc";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		List<model.mdlTransactionPayment> TransactionPaymentList = new ArrayList<model.mdlTransactionPayment>();
        	if(BrandID.contentEquals("") && AreaID.contentEquals(""))
        		request.setAttribute("listapproval", TransactionPaymentList);
        	else
        	{
        		TransactionPaymentList.addAll(PaymentAdapter.LoadTransactionPaymentByParam(BrandID,AreaID,mdlUser));
        		request.setAttribute("listapproval", TransactionPaymentList);
        	}
    		
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);
    		
    		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
    		AreaList.addAll(AreaAdapter.LoadArea(mdlUser));
    		request.setAttribute("listarea", AreaList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		if(ButtonStatus.contentEquals("disabled"))
    			ButtonStatus = "none";
    		else
    			ButtonStatus = "";
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		request.setAttribute("selectedbrand", BrandID);
    		request.setAttribute("selectedarea", AreaID);
    		request.setAttribute("condition", result);
    		request.setAttribute("errorDescription", resultDesc);
    		
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/approval_doc.jsp");
    		dispacther.forward(request, response);
    		
    		tempBrandID=BrandID;
    		tempAreaID=AreaID;
    		BrandID="";
    		AreaID="";
    	}
		
		result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	String btnApprovalDoc = request.getParameter("btnApprovalDoc");
    	String btnRejectApprovalDoc = request.getParameter("btnRejectApprovalDoc");
    	
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		if(keyBtn.equals("sort")) {
			//Declare slBrand and slArea
	    	BrandID = request.getParameter("BrandID");
	    	AreaID = request.getParameter("AreaID");
	    	
	    	return;
		}
		
		if (btnApprovalDoc != null){
			String lPaymentID = request.getParameter("temp_txtPaymentID");
			String lResult = PaymentAdapter.ApproveTransactionPayment(lPaymentID,user);
			if(lResult.contains("Success Approve Payment Document")) {  
				result =  "SuccessApproval";
            }  
            else {
            	result = "FailedApproval";
            	resultDesc = lResult;
            }
			
			BrandID=tempBrandID;
			AreaID=tempAreaID;
			
			doGet(request, response);
			return;
		}
		
		if (btnRejectApprovalDoc != null){
			String lPaymentID = request.getParameter("temp_txtPaymentID");
			String lMerchantID = request.getParameter("temp_txtMerchantID");
			Integer lPaymentAmount = Integer.parseInt(request.getParameter("temp_txtPaymentAmount"));
			
			String lResult = PaymentAdapter.RejectTransactionPayment(lPaymentID,lMerchantID,lPaymentAmount,user);
			if(lResult.contains("Success Reject Payment Document")) {  
				result =  "SuccessReject";
            }  
            else {
            	result = "FailedReject";
            	resultDesc = lResult;
            }
			
			BrandID=tempBrandID;
			AreaID=tempAreaID;
			
			doGet(request, response);
			return;
		}
		
    }

}
