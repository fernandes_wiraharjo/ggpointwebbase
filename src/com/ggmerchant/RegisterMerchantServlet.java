package com.ggmerchant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BankAdapter;
import adapter.BrandAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.ProvinceAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import model.Globals;
import model.mdlProvince;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@WebServlet(urlPatterns={"/register_merchant"} , name="register_merchant")
public class RegisterMerchantServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public RegisterMerchantServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    String result,resultDesc;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	Boolean CheckMenu;
    	String MenuURL = "register_merchant";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		List<model.mdlMerchant> MerchantList = new ArrayList<model.mdlMerchant>();
    		MerchantList.addAll(MerchantAdapter.LoadMerchant(mdlUser));
    		request.setAttribute("listmerchant", MerchantList);
    		
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);
    		
    		List<model.mdlMerchantType> MerchantTypeList = new ArrayList<model.mdlMerchantType>();
    		MerchantTypeList.addAll(MerchantAdapter.LoadMerchantType(user));
    		request.setAttribute("listtype", MerchantTypeList);
    		
    		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
    		AreaList.addAll(AreaAdapter.LoadArea(mdlUser));
    		request.setAttribute("listarea", AreaList);
    		
    		List<model.mdlBank> BankList = new ArrayList<model.mdlBank>();
    		BankList.addAll(BankAdapter.LoadBank(user));
    		request.setAttribute("listbank", BankList);
    		
//    		ProvinceAdapter.GetProvinceAPI();
    		List<model.mdlProvinceDetail> ProvinceList = new ArrayList<model.mdlProvinceDetail>();
    		ProvinceList.addAll(ProvinceAdapter.LoadProvince(user));
    		request.setAttribute("listprovince", ProvinceList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		
    		request.setAttribute("condition", result);
    		request.setAttribute("buttonstatus", ButtonStatus);
//    		request.setAttribute("errorDescription", Globals.gConditionDesc);
    		
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/register_merchant.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
	}
    
    //@SuppressWarnings("deprecation")
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		String command = "";
		
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		//-- export process --
		if (keyBtn.equals("pdf")){
			try {
				//Declare Parameter
				String rAreaName = request.getParameter("area");
				String rMerchantID = request.getParameter("merchantid");
				String rMerchantName = request.getParameter("merchantname");
				String rType = request.getParameter("type");
				String rAddress = request.getParameter("address");
				String rProvince = request.getParameter("province");
				String rDistrict = request.getParameter("district");
				String rSubDistrict = request.getParameter("subdistrict");
				String rPic = request.getParameter("pic");
				String rContact = request.getParameter("contact");
				String rBank = request.getParameter("bank");
				String rAccountName = request.getParameter("accountname");
				String rAccountNumber = request.getParameter("accountnumber");
				command = rMerchantName;
				
				JasperPrint jasperPrint = null;
				JasperReport jasperReport;
				String paramJasperReport = getServletContext().getInitParameter("param_jasper_report");
				String paramJasperPrint = getServletContext().getInitParameter("param_jasper_print");
				String paramLogoPath = getServletContext().getInitParameter("param_logo");
				
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("larea", ValidateNull.NulltoStringEmpty(rAreaName));
				params.put("lmerchantid", ValidateNull.NulltoStringEmpty(rMerchantID));
				params.put("lmerchantname", ValidateNull.NulltoStringEmpty(rMerchantName));
				params.put("ltype", ValidateNull.NulltoStringEmpty(rType));
				params.put("laddress", ValidateNull.NulltoStringEmpty(rAddress));
				params.put("lprovince", ValidateNull.NulltoStringEmpty(rProvince));
				params.put("ldistrict", ValidateNull.NulltoStringEmpty(rDistrict));
				params.put("lsubdistrict", ValidateNull.NulltoStringEmpty(rSubDistrict));
				params.put("lpic", ValidateNull.NulltoStringEmpty(rPic));
				params.put("lcontact", ValidateNull.NulltoStringEmpty(rContact));
				params.put("lbank", ValidateNull.NulltoStringEmpty(rBank));
				params.put("laccountname", ValidateNull.NulltoStringEmpty(rAccountName));
				params.put("laccountnumber", ValidateNull.NulltoStringEmpty(rAccountNumber));
				params.put("limage", ValidateNull.NulltoStringEmpty(paramLogoPath));
				
				jasperReport = JasperCompileManager.compileReport(paramJasperReport+"rpt_merchant_confirmation.jrxml");
				jasperPrint = JasperFillManager.fillReport(jasperReport, params,new JREmptyDataSource());
				
				//coding for exporting to Pdf
				JasperExportManager.exportReportToPdfFile(jasperPrint, paramJasperPrint+"rpt_merchant_confirmation.pdf");
				
				String pdfFileName = "rpt_merchant_confirmation.pdf";
				File pdfFile = new File(paramJasperPrint+"rpt_merchant_confirmation.pdf");
				
				response.setContentType("application/pdf");
				response.addHeader("Content-Disposition: inline;", "filename=" + pdfFileName);
				response.setContentLength((int) pdfFile.length());
				FileInputStream fileInputStream = new FileInputStream(pdfFile);
				OutputStream responseOutputStream = response.getOutputStream();
				int bytes;
				
				while ((bytes = fileInputStream.read()) != -1) {
					responseOutputStream.write(bytes);
				}
			}
			catch(Exception ex)
			{
				LogAdapter.InsertLogExc(ex.toString(), "EXPORT DATA MERCHANT", command, user);
				//Globals.gCondition = "FailedExport";
				//Globals.gConditionDesc = "Export data merchant '" + Globals.gCommand + "' failed. Please try again or contact admin for further help";
				
				//doGet(request, response);
			}
			
			return;
		}
		//-- end of export process --
				
		
		//-- save and update process --
		//Declare TextBox
		String StoreID = request.getParameter("StoreID");
		String StoreName = request.getParameter("StoreName");
		String PIC = request.getParameter("PIC");
		String Phone = request.getParameter("Phone");
		String Email = request.getParameter("Email");
		String Brand = request.getParameter("Brand");
		String Type = request.getParameter("Type");
		String Area = request.getParameter("Area");
		String Address = request.getParameter("Address");
		String Province = request.getParameter("Province");
		String District = request.getParameter("District");
		String SubDistrict = request.getParameter("SubDistrict");
		String Bank = request.getParameter("Bank");
		String AccountName = request.getParameter("AccountName");
		String AccountNumber = request.getParameter("AccountNumber");
//		String[] listBrand = request.getParameterValues("listBrand[]");
//		String Brand = String.join(",",listBrand);
				
		//Declare mdlMerchant for global
		model.mdlMerchant Merchant = new model.mdlMerchant();
		Merchant.setMerchantID(StoreID);
		Merchant.setMerchantName(StoreName);
		Merchant.setMerchantPIC(PIC);
		Merchant.setMerchantContact(Phone);
		Merchant.setMerchantEmail(Email);
//		Merchant.setMerchantBrand(listBrand);
		Merchant.setMerchantBrand(Brand);
		Merchant.setMerchantType(Type);
		Merchant.setMerchantArea(Area);
		Merchant.setMerchantAddress(Address);
		Merchant.setMerchantProvince(Province);
		Merchant.setMerchantDistrict(District);
		Merchant.setMerchantSubDistrict(SubDistrict);
		Merchant.setMerchantBank(Bank);
		Merchant.setAccountName(AccountName);
		Merchant.setMerchantAccountNumber(AccountNumber);
			
		String lResult = "";
		
		if (keyBtn.equals("save")){
			lResult = MerchantAdapter.InsertMerchant(Merchant, user);
			
			if(lResult.contains("Success Insert Merchant")) {  
				result = "SuccessInsertMerchant";
		    }  
		    else {
		        result = "FailedInsertMerchant"; 
		        resultDesc = lResult;
		    } 
		}
		
		if (keyBtn.equals("update")){
			lResult = MerchantAdapter.UpdateMerchant(Merchant, user);
			
			if(lResult.contains("Success Update Merchant")) {  
				result = "SuccessUpdateMerchant";
		    }  
		    else {
		        result = "FailedUpdateMerchant"; 
		        resultDesc = lResult;
		    }
		}
		
		response.setContentType("application/text");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(result+"--"+resultDesc);
        
        return;
        //-- end of save and update process --
	}

}
