package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;
import java.util.Locale;
import java.text.NumberFormat;

public class MobileConfigAdapter {
	
	public static List<model.mdlMobileConfig> LoadMobileConfigbyBrand(String lBrandId, String user){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;	
		String command = "";
		List<model.mdlMobileConfig> mdlMobileConfigList = new ArrayList<model.mdlMobileConfig>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadMobileConfig(?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lBrandId));
			command = pstm.toString();
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlMobileConfig mdlMobileConfig = new model.mdlMobileConfig();				
				mdlMobileConfig.setBrandId(rs.getString("BrandId"));
				mdlMobileConfig.setID(rs.getString("ID"));
				mdlMobileConfig.setDesc(rs.getString("Desc"));
				mdlMobileConfig.setValue(rs.getString("Value"));
				mdlMobileConfigList.add(mdlMobileConfig);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMobileConfig", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadMobileConfig", "close opened connection protocol", user);
			}
		}

		return mdlMobileConfigList;
	}
	
	public static String InsertMobileConfig(List<model.mdlMobileConfig> lParamlist, String user)
	{
		Connection connection = null;
		PreparedStatement pstmProduct = null;
		PreparedStatement pstmProductBrand = null;
		String command = "";
		String result = "";
		
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmProduct = connection.prepareStatement("{call wb_InsertMobileConfig(?,?,?,?)}");
			
			for(model.mdlMobileConfig lParam : lParamlist)
			{
				pstmProduct.setString(1, ValidateNull.NulltoStringEmpty(lParam.getBrandId()));
				pstmProduct.setString(2, ValidateNull.NulltoStringEmpty(lParam.getID()));
				pstmProduct.setString(3, ValidateNull.NulltoStringEmpty(lParam.getDesc()));
				pstmProduct.setString(4, ValidateNull.NulltoStringEmpty(lParam.getValue()));


				command += pstmProduct.toString();
				pstmProduct.executeUpdate();
			}
			result = "Success Insert Mobile Config";
		}
		catch (Exception e) {
		    LogAdapter.InsertLogExc(e.toString(), "InsertMobileConfig", command , user);
		    result = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstmProduct != null) {
					pstmProduct.close();
				}
				if (pstmProductBrand != null) {
					pstmProductBrand.close();
				}

				if (connection != null) {
					connection.close();
				}
			
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertMobileConfig", "close opened connection protocol", user);
			}
		}
		return result;
	}
	
	public static String UpdateMobileConfig(List<model.mdlMobileConfig> lParamlist, String user)
	{
		Connection connection = null;
		PreparedStatement pstmMobileConfig = null;
		
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();

			for(model.mdlMobileConfig lParam : lParamlist)
			{
				pstmMobileConfig = connection.prepareStatement("{call wb_UpdateMobileConfig(?,?,?,?)}");	
				pstmMobileConfig.setString(1, ValidateNull.NulltoStringEmpty(lParam.getID()));
				pstmMobileConfig.setString(2, ValidateNull.NulltoStringEmpty(lParam.getDesc()));
				pstmMobileConfig.setString(3, ValidateNull.NulltoStringEmpty(lParam.getValue()));
				pstmMobileConfig.setString(4, ValidateNull.NulltoStringEmpty(lParam.getBrandId()));
				command += pstmMobileConfig.toString();
			
				pstmMobileConfig.executeUpdate();			

			}
			result = "Success Update Mobile Config";
		}
		catch (Exception e) {
		    LogAdapter.InsertLogExc(e.toString(), "InsertMobileConfig", command , user);
		  	result = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstmMobileConfig != null) {
					pstmMobileConfig.close();
				}
				
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateMobileConfig", "close opened connection protocol", user);
			}
		}
		return result;	
	}

	public static String DeleteMobileConfig(String lBrandId, String user){
		Connection connection = null;
		PreparedStatement pstmMobileConfig = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			pstmMobileConfig = connection.prepareStatement("{call wb_DeleteMobileConfig(?)}");
			pstmMobileConfig.setString(1, ValidateNull.NulltoStringEmpty(lBrandId));		
			command += pstmMobileConfig.toString();
			pstmMobileConfig.executeUpdate();	
			
			connection.commit();
			result = "SuccessDelete";
		}
		catch (Exception e) {
		    LogAdapter.InsertLogExc(e.toString(), "DeleteMobileConfig", command , user);
		    result = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstmMobileConfig != null) {
					pstmMobileConfig.close();
				}

				if (connection != null) {
					connection.close();
				}
				
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteProduct", "close opened connection protocol", user);
			}
		}
		
		return result;	
	}
	
	//<<GG-69
	public static model.mdlMobileConfig addValueMdlMobileConfig(String lBrandId, String lId, String lValue, String lDesc)
	{		
		model.mdlMobileConfig mdlMobileConfig = new model.mdlMobileConfig();
		mdlMobileConfig.setBrandId(lBrandId);
		mdlMobileConfig.setID(lId);
		mdlMobileConfig.setDesc(lDesc);
		mdlMobileConfig.setValue(lValue);
		//Globals.gmdlMobileConfiglist.add(mdlMobileConfig);
		return mdlMobileConfig;
	}
	//>>
}
