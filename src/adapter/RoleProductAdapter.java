package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;

import com.google.android.gcm.server.Result;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;

public class RoleProductAdapter {
	//---------------------------------------- Role Product --------------------------------------------------------//
	
	public static List<model.mdlRoleProduct> LoadRoleProduct(String user){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlRoleProduct> mdlRoleProductList = new ArrayList<model.mdlRoleProduct>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadRoleProduct()}");
			rs = pstm.executeQuery();
		
			while(rs.next())
			{
				model.mdlRoleProduct mdlRoleProduct = new model.mdlRoleProduct();				
				mdlRoleProduct.setRoleProductID(rs.getString("RoleProductID"));
				mdlRoleProduct.setRoleProductName(rs.getString("RoleProductName"));
				mdlRoleProduct.setBrandID(rs.getString("BrandID"));
				mdlRoleProductList.add(mdlRoleProduct);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRoleProduct", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadRoleProduct", "close opened connection protocol", user);
			}
		}
		return mdlRoleProductList;
	}
	
	public static String InsertRoleProduct(model.mdlRoleProduct lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_InsertRoleProduct(?,?,?,?,?,?,?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getRoleProductID()));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getRoleProductName()));
			pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getBrandID()));
			pstm.setString(4, user);
			pstm.setString(5, user);
			pstm.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstm.setTimestamp(7, new Timestamp(System.currentTimeMillis()));

			command = pstm.toString();
			rs = pstm.executeQuery();
			result = "SuccessInsert";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertRoleProduct", command, user);
			result = "FailedInsert";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertRoleProduct", "close opened connection protocol", user);
			}
		}
		return result;
	}
	
	public static String UpdateRoleProduct(model.mdlRoleProduct lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_UpdateRoleProduct(?,?,?,?,?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getRoleProductID()));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getRoleProductName()));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getBrandID()));
			pstm.setString(3, user);
			pstm.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
		
			command = pstm.toString();
			rs = pstm.executeQuery();
			result = "SuccessUpdate";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateRoleProduct", command, user);
			result = "FailedUpdate";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateRoleProduct", "close opened connection protocol", user);
			}
		}
		return result;	
	}

	public static String DeleteRoleProduct(String lRoleProduct, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_DeleteRoleProduct(?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lRoleProduct));
		
			command = pstm.toString();
			rs = pstm.executeQuery();	
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteRoleProduct", command, user);
			result = "FailedUpdate";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteRoleProduct", "close opened connection protocol", user);
			}
		}
		
		return result;	
	}
	
	//---------------------------------------- Role Product Detail-------------------------------------------------//
	
	public static List<model.mdlRoleProductDetail> LoadRoleProductDetail(String user){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlRoleProductDetail> mdlRoleProductDetailList = new ArrayList<model.mdlRoleProductDetail>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadRoleProductDetail()}");
			rs = pstm.executeQuery();
		
			while(rs.next())
			{
				model.mdlRoleProductDetail mdlRoleProductDetail = new model.mdlRoleProductDetail();				
				mdlRoleProductDetail.setRoleProductID(rs.getString("RoleProductID"));
				mdlRoleProductDetail.setMerchantID(rs.getString("MerchantID"));
				mdlRoleProductDetail.setProductID(rs.getString("ProductID"));
				mdlRoleProductDetail.setPrice(rs.getString("Price"));
				mdlRoleProductDetail.setPoint(rs.getString("Point"));
				mdlRoleProductDetailList.add(mdlRoleProductDetail);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRoleProductDetail", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadRoleProductDetail", "close opened connection protocol", user);
			}
		}
		return mdlRoleProductDetailList;
	}
	
	public static String InsertRoleProductDetail(model.mdlRoleProductDetail lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_InsertRoleProductDetail(?,?,?,?,?,?,?,?,?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getRoleProductID()));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getMerchantID()));
			pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
			pstm.setString(4, ValidateNull.NulltoStringEmpty(lParam.getPoint()));
			pstm.setString(5, ValidateNull.NulltoStringEmpty(lParam.getPrice()));
			pstm.setString(6, user);
			pstm.setString(7, user);
			pstm.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
			pstm.setTimestamp(9, new Timestamp(System.currentTimeMillis()));

			command = pstm.toString();
			rs = pstm.executeQuery();
			result = "SuccessInsert";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertRoleProductDetail", command, user);
			result = "FailedInsert";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertRoleProductDetail", "close opened connection protocol", user);
			}
		}
		return result;
	}
	
	public static String UpdateRoleProductDetail(List<model.mdlRoleProductDetail> lParamlist,String lRoleProductID,String lRoleProductName, String user)
	{
		Connection connection = null;
		PreparedStatement pstmUpdateEnable = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmDeleteEnable = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			
			pstmUpdateEnable = connection.prepareStatement("{call wb_UpdateRoleProductbyIsEnabled(?,?)}");
			pstmUpdateEnable.setString(1, ValidateNull.NulltoStringEmpty(lRoleProductID));
			pstmUpdateEnable.setString(2, ValidateNull.NulltoStringEmpty(lRoleProductName));
			
			pstmUpdateEnable.executeUpdate();
			command += pstmUpdateEnable.toString();
			
			pstm = connection.prepareStatement("{call wb_UpdateRoleProductDetail(?,?,?,?,?,?,?,?,?)}");
			

			for(model.mdlRoleProductDetail lParam : lParamlist){				
				pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getRoleProductID()));
				pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getMerchantID()));
				pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
				pstm.setString(4, lParam.getPrice());
				pstm.setString(5, lParam.getPoint());
				pstm.setString(6, user);
				pstm.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
				pstm.setString(8, user);
				pstm.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
				command += pstm.toString();
				pstm.executeUpdate();
			}
			
			pstmDeleteEnable = connection.prepareStatement("{call wb_DeleteRoleProductbyIsEnabled()}");
			pstmDeleteEnable.executeUpdate();
			command += pstmDeleteEnable.toString();

			result = "SuccessUpdate";
		}
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateRoleProductDetail", command ,user);
		  			result = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "UpdateRoleProductDetail", command , user);
    			result = "Database Error";
			}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				connection.setAutoCommit(true);
		        
				if (connection != null) {
					connection.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateRoleProductDetail", "close opened connection protocol", user);
			}
		}
		return result;	
	}
	
	public static String UpdatePricePoint(model.mdlRoleProductDetail lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			pstm = connection.prepareStatement("{call wb_UpdatePricePoint(?,?,?,?,?,?)}");
				pstm.setString(1, lParam.getPrice());
				pstm.setString(2, lParam.getPoint());
				pstm.setString(3, user);
				pstm.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				pstm.setString(5, ValidateNull.NulltoStringEmpty(lParam.getRoleProductID()));
				pstm.setString(6, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
				command += pstm.toString();
				
				pstm.executeQuery();
			result = "SuccessUpdate";
		}
		catch (Exception e) {
		    LogAdapter.InsertLogExc(e.toString(), "UpdatePricePoint", command , user);
    		result = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				connection.setAutoCommit(true);
		        
				if (connection != null) {
					connection.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateRoleProductDetail", "close opened connection protocol", user);
			}
		}
		return result;	
	}

	public static String DeleteRoleProductDetail(model.mdlRoleProductDetail lParam, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_DeleteRoleProduct(?,?,?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getRoleProductID()));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getMerchantID()));
			pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
		
			command = pstm.toString();
			rs = pstm.executeQuery();	
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteRoleProductDetail", command, user);
			result = "FailedUpdate";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteRoleProductDetail", "close opened connection protocol", user);
			}
		}
		
		return result;	
	}
	
	public static String CheckRoleProductDetail(String lRoleProductID, String lProductID, String user){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String lResult = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_CheckRoleProduct(?,?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lRoleProductID));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lProductID));
			rs = pstm.executeQuery();
		
			while(rs.next())
			{
				lResult = rs.getString("RoleProductID");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckRoleProductDetail", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "CheckRoleProductDetail", "close opened connection protocol", user);
			}
		}
		return lResult;
	}
	
	public static List<model.mdlMerchant> GetMerchantListbyRoleProductId(String lRoleProductID, String user) {
		//define connection
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		//define local variable
		List<model.mdlMerchant> MerchantList = new ArrayList<model.mdlMerchant>();
		
		try{
		//execute store procedure
		connection =  database.RowSetAdapter.getConnection();
		pstm = connection.prepareStatement("{call wb_GetMerchantList_byRoleProductId(?)}");
		pstm.setString(1, ValidateNull.NulltoStringEmpty(lRoleProductID));
		command = pstm.toString();
		jrs = pstm.executeQuery();
		
		while(jrs.next()){
			model.mdlMerchant Merchant = new model.mdlMerchant();
			
			Merchant.setMerchantID(jrs.getString("MerchantID"));
			Merchant.setMerchantName(jrs.getString("MerchantName"));
			
			MerchantList.add(Merchant);
		}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetMerchantListbyRoleProductId", command, user);
		}
		finally{
			//close the opened connection
			try{
				//close the opened connection
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "GetMerchantListbyRoleProductId", "close opened connection protocol", user);
			}
		}

		return MerchantList;
	}
	
	public static List<model.mdlProduct> GetProductListbyRoleProductId(String lRoleProductID, String user) {
		//define connection
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		//define local variable
		List<model.mdlProduct> ProductList = new ArrayList<model.mdlProduct>();
		
		try{
		//execute store procedure
		connection =  database.RowSetAdapter.getConnection();
		pstm = connection.prepareStatement("{call wb_GetProductList_byRoleProductId(?)}");
		pstm.setString(1, ValidateNull.NulltoStringEmpty(lRoleProductID));
		command = pstm.toString();
		jrs = pstm.executeQuery();
		
		while(jrs.next()){
			model.mdlProduct mdlProduct = new model.mdlProduct();
			
			mdlProduct.setProductID(jrs.getString("ProductID"));
			mdlProduct.setProductName(jrs.getString("ProductName"));
			
			ProductList.add(mdlProduct);
		}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetProductListbyRoleProductId", command, user);
		}
		finally{
			//close the opened connection
			try{
				//close the opened connection
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "GetProductListbyRoleProductId", "close opened connection protocol", user);
			}
		}

		return ProductList;
	}
	
	public static model.mdlRoleProductDetail GetPricePointbyRoleProductId(String lRoleProductID, String lProductId, String user) {
		//define connection
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		model.mdlRoleProductDetail mdlRoleProductDetail = new model.mdlRoleProductDetail();
		//define local variable
		
		try{
		//execute store procedure
		connection =  database.RowSetAdapter.getConnection();
		pstm = connection.prepareStatement("{call wb_GetPricePoint_byRoleProductId(?,?)}");
		pstm.setString(1, ValidateNull.NulltoStringEmpty(lRoleProductID));
		pstm.setString(2, ValidateNull.NulltoStringEmpty(lProductId));
		command = pstm.toString();
		jrs = pstm.executeQuery();
		
		while(jrs.next()){
			
			
			mdlRoleProductDetail.setPrice(jrs.getString("Price"));
			mdlRoleProductDetail.setPoint(jrs.getString("Point"));
		}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetPricePointbyRoleProductId", command, user);
		}
		finally{
			//close the opened connection
			try{
				//close the opened connection
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "GetPricePointbyRoleProductId", "close opened connection protocol", user);
			}
		}

		return mdlRoleProductDetail;
	}
	
	public static String  GetPointPrice(String lBrandID, double lPrice, String user){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String lPointResult = "";
		double lConv_Price = 0;
		double lConv_Point = 0;

		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_GetPointPrice(?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lBrandID));
			rs = pstm.executeQuery();
		
			while(rs.next())
			{
				lConv_Price =  Double.parseDouble(rs.getString("Price"));
				lConv_Point = Double.parseDouble(rs.getString("Point"));
			}
			
			//<<Calculate Point
				lPointResult = String.valueOf(Math.round((lPrice / lConv_Price) * lConv_Point));
				
			//>>
			
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetPricePointbyRoleProductId", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "GetPricePointbyRoleProductId", "close opened connection protocol", user);
			}
		}
		return lPointResult;
	}
	
	
}
