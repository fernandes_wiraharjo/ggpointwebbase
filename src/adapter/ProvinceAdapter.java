package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlDistrict;
import model.mdlDistrictDetail;
import model.mdlProvince;
import model.mdlProvinceDetail;
import model.mdlSubDistrict;

public class ProvinceAdapter {

	public static model.mdlProvince GetProvinceAPI(String user){
		model.mdlProvince Province = new model.mdlProvince();
//		model.mdlToken mdlToken = TokenAdapter.GetToken();
		String lAPILink = "";
		String result = "";
		try {
			
			Client client = Client.create();
			
			lAPILink = "http://dev.farizdotid.com/api/daerahindonesia/provinsi";
			WebResource webResource = client.resource(lAPILink);
			
			Gson gson = new Gson();
//			String json = gson.toJson("");
			
//			ClientResponse response  = webResource.type("application/json").post(ClientResponse.class, json);
			ClientResponse response  = webResource.type("application/json").get(ClientResponse.class);
			
			result = response.getEntity(String.class);
			Province = gson.fromJson(result, mdlProvince.class);
			
			InsertProvince(Province.getSemuaprovinsi(), user);
			GetDistrictAPI(Province.getSemuaprovinsi(), user);
		  } 
			catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetProvinceAPI", lAPILink , user);
//			Globals.gReturn_Status = "Error GetToken";
		  }
		
		return Province;
		}
	
	public static model.mdlDistrict GetDistrictAPI(List<model.mdlProvinceDetail> lParamlist, String user){
		model.mdlDistrict District = new model.mdlDistrict();
//		model.mdlToken mdlToken = TokenAdapter.GetToken();
		String lAPILink = "";
		String result = "";
		try {
			
			Client client = Client.create();
			
			for(mdlProvinceDetail lParam : lParamlist)
			{
				lAPILink = "http://dev.farizdotid.com/api/daerahindonesia/provinsi/"+lParam.getId()+"/kabupaten";
				WebResource webResource = client.resource(lAPILink);
				Gson gson = new Gson();
				ClientResponse response  = webResource.type("application/json").get(ClientResponse.class);
				
				result = response.getEntity(String.class);
				District = gson.fromJson(result, mdlDistrict.class);
				
				InsertDistrict(District.getDaftar_kecamatan(), user);
				GetSubDistrictAPI(District.getDaftar_kecamatan(), user);
			}
		  } 
			catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDistrictAPI", lAPILink , user);
//			Globals.gReturn_Status = "Error GetToken";
		  }
		
		return District;
		}
	
	public static model.mdlSubDistrict GetSubDistrictAPI(List<model.mdlDistrictDetail> lParamlist, String user){
		model.mdlSubDistrict SubDistrict = new model.mdlSubDistrict();
		String lAPILink = "";
		String result = "";
		try {
			
			Client client = Client.create();
			
			for(mdlDistrictDetail lParam : lParamlist)
			{
				lAPILink = "http://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/"+lParam.getId()+"/kecamatan";
				WebResource webResource = client.resource(lAPILink);
				
				Gson gson = new Gson();
				ClientResponse response  = webResource.type("application/json").get(ClientResponse.class);
				
				result = response.getEntity(String.class);
				SubDistrict = gson.fromJson(result, mdlSubDistrict.class);
				
				InsertSubDistrict(SubDistrict.getDaftar_kecamatan(), user);
			}
		  } 
			catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetSubDistrictAPI", lAPILink , user);
		  }
		
		return SubDistrict;
		}

	public static String InsertProvince(List<model.mdlProvinceDetail> lParamlist, String user) throws Exception
	{
		Connection connection = database.RowSetAdapter.getConnection();
		String sql = "INSERT INTO `ms_province`(ProvinceID, ProvinceName) VALUES (?,?)";
		PreparedStatement pstm = connection.prepareStatement(sql);
		String result = "";
		try{
			
			for(model.mdlProvinceDetail lParam : lParamlist)
			{	
				pstm.setString(1,  lParam.getId());
				pstm.setString(2,  lParam.getNama());
				
				//pstm.executeUpdate();
				pstm.addBatch();
			}
			pstm.executeBatch();
			
//				pstm.close();
//				connection.close();	
			result = "Success Insert Province";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertProvince", sql , user);
			result = "Error Insert Province";
		}
		finally {
			 if (pstm != null) {
				 pstm.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
		 }
		
		return result;
	}
	
	public static String InsertDistrict(List<model.mdlDistrictDetail> lParamlist, String user) throws Exception
	{
		Connection connection = database.RowSetAdapter.getConnection();
		String sql = "INSERT INTO `ms_district`(DistrictID, DistrictName, ProvinceID) VALUES (?,?,?)";
		PreparedStatement pstm = connection.prepareStatement(sql);
		String result = "";
		try{
			
			for(model.mdlDistrictDetail lParam : lParamlist)
			{	
				pstm.setString(1,  lParam.getId());
				pstm.setString(2,  lParam.getNama());
				pstm.setString(3, lParam.getId_prov());
				
				//pstm.executeUpdate();
				pstm.addBatch();
			}
			pstm.executeBatch();
			
//				pstm.close();
//				connection.close();	
			result = "Success Insert District";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertDistrict", sql , user);
			result = "Error Insert District";
		}
		finally {
			 if (pstm != null) {
				 pstm.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
		 }
		
		return result;
	}

	public static String InsertSubDistrict(List<model.mdlSubDistrictDetail> lParamlist, String user) throws Exception
	{
		Connection connection = database.RowSetAdapter.getConnection();
		String sql = "INSERT INTO `ms_subdistrict`(SubDistrictID, SubDistrictName, DistrictID) VALUES (?,?,?)";
		PreparedStatement pstm = connection.prepareStatement(sql);
		String result = "";
		try{
			
			for(model.mdlSubDistrictDetail lParam : lParamlist)
			{	
				pstm.setString(1,  lParam.getId());
				pstm.setString(2,  lParam.getNama());
				pstm.setString(3, lParam.getId_kabupaten());
				
				//pstm.executeUpdate();
				pstm.addBatch();
			}
			pstm.executeBatch();
			
//				pstm.close();
//				connection.close();	
			result = "Success Insert SubDistrict";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertSubDistrict", sql , user);
			result = "Error Insert SubDistrict";
		}
		finally {
			 if (pstm != null) {
				 pstm.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
		 }
		
		return result;
	}
	
	public static List<model.mdlProvinceDetail> LoadProvince(String user) {
		
		//define local variable
		List<model.mdlProvinceDetail> ProvinceList = new ArrayList<model.mdlProvinceDetail>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadProvince()}";
			pstm = connection.prepareStatement(sql);
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlProvinceDetail Province = new model.mdlProvinceDetail();
				
				Province.setId(jrs.getString("ProvinceID"));
				Province.setNama(jrs.getString("ProvinceName"));
				
				ProvinceList.add(Province);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadProvince", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadProvince", "close opened connection protocol", user);
			}
		}

		return ProvinceList;
	}

	public static List<model.mdlDistrictDetail> LoadDistrict(String lProvinceID, String user) {
		
		//define local variable
		List<model.mdlDistrictDetail> DistrictList = new ArrayList<model.mdlDistrictDetail>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadDistrict(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  lProvinceID);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlDistrictDetail District = new model.mdlDistrictDetail();
				
				District.setId(jrs.getString("DistrictID"));
				District.setNama(jrs.getString("DistrictName"));
				
				DistrictList.add(District);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadDistrict", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e){
				LogAdapter.InsertLogExc(e.toString(), "LoadDistrict", "close opened connection protocol", user);
			}
		}

		return DistrictList;
	}
	
	public static List<model.mdlSubDistrictDetail> LoadSubDistrict(String lDistrictID, String user) {
		
		//define local variable
		List<model.mdlSubDistrictDetail> SubDistrictList = new ArrayList<model.mdlSubDistrictDetail>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadSubDistrict(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  lDistrictID);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlSubDistrictDetail SubDistrict = new model.mdlSubDistrictDetail();
				
				SubDistrict.setId(jrs.getString("SubDistrictID"));
				SubDistrict.setNama(jrs.getString("SubDistrictName"));
				
				SubDistrictList.add(SubDistrict);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadSubDistrict", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadSubDistrict", "close opened connection protocol", user);
			}
		}

		return SubDistrictList;
	}
}
