package adapter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import model.Globals;
import model.mdlPush;

public class PushNotificationAdapter {

	public static String PostToGCMGeneral(String apiKey, mdlPush content){
		String lResult = "";
		
		try{
	        // 1. URL
	        URL url = new URL("https://fcm.googleapis.com/fcm/send");

	        // 2. Open connection
	        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	        // 3. Specify POST method
	        conn.setRequestMethod("POST");

	        // 4. Set the headers
	        conn.setRequestProperty("Content-Type", "application/json");
	        conn.setRequestProperty("Authorization", "key="+apiKey);

	        conn.setDoOutput(true);
	        
	        Gson gson = new Gson();
	        
	        String json = gson.toJson(content);
	        
	        System.out.println(json);
	        
 
	            // 5. Add JSON data into POST request body

	            //`5.1 Use Jackson object mapper to convert Contnet object into JSON

	            // 5.2 Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            // 5.3 Copy Content "JSON" into
	            //mapper.writeValue(wr, content);
	            wr.write(json.getBytes("UTF-8"));
	            

	            // 5.4 Send the request
	            wr.flush();

	            // 5.5 close
	            wr.close();

	            // 6. Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(
	                    new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();

	            // 7. Print result
	            System.out.println(response.toString());
	            //lResult = response.toString();
	            lResult = "Push Success";

	            } catch (MalformedURLException e) {
	                e.printStackTrace();
	                lResult = "Push Failed";
	            } catch (IOException e) {
	                e.printStackTrace();
	                lResult = "Push Failed";
	            }
		
		return lResult;
	}
	
	public static List<String> LoadTokenByUsername(String lUserName, String user) {
		
		//define local variable
		List<String> listToken = new ArrayList<String>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadUserToken(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, lUserName);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				String Token = "";
				Token = jrs.getString("Androidkey");
				
				listToken.add(Token);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTokenByUsername", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadTokenByUsername", "close opened connection protocol", user);
			}
		}

		return listToken;
	}
}
