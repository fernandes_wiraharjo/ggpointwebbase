package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;

public class RoleAdapter {
	
	public static List<model.mdlRole> LoadRole(String user){	
		
		Connection connection = null;
		PreparedStatement pstmUser = null;
		ResultSet rs = null;
		List<model.mdlRole> mdlRoleList = new ArrayList<model.mdlRole>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmUser = connection.prepareStatement("{call wb_LoadRole()}");
			rs = pstmUser.executeQuery();	
		
			while(rs.next())
			{
				model.mdlRole mdlRole = new model.mdlRole();				
				mdlRole.setRoleID(rs.getString("RoleID"));
				mdlRole.setRoleName(rs.getString("RoleName"));

				mdlRoleList.add(mdlRole);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRole", pstmUser.toString(), user);
		}
		finally{
			//close the opened connection
			try{
				if (pstmUser != null) {
					pstmUser.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadRole", "close opened connection protocol", user);
			}
		}		
		return mdlRoleList;
	}
}
