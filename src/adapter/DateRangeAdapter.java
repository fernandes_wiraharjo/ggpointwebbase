package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import model.Globals;

public class DateRangeAdapter {

	//function to load date range period for 1 week
	public static model.mdlDateRange LoadDateRange(String lDate, String user) {
	//define local variable
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	String command = "";
	model.mdlDateRange mdlDateRange = new model.mdlDateRange();
		
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadSummaryTx_DateRange(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert total transaction sql parameter
			pstm.setString(1,  lDate);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute total transaction store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				mdlDateRange.StartDate = jrs.getString("Start");
				mdlDateRange.EndDate = jrs.getString("End");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDateRange", command, user);
		}
		finally {
			try{
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadDateRange", "close opened connection protocol", user);
			}
		}

		return mdlDateRange;
	}

	//function to load date range period for 2 week
	public static model.mdlDateRange LoadDateRange2(String lDate, String user) {
	//define local variable
	Connection connection = null;
	PreparedStatement pstm = null;
	PreparedStatement pstmLoadStartDate = null;
	PreparedStatement pstmLoadEndDate = null;
	ResultSet jrs = null;
	ResultSet jrsLoadStartDate = null;
	ResultSet jrsLoadEndDate = null;
	String command = "";
	
	model.mdlDateRange mdlDateRange = new model.mdlDateRange();
		
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadCurrentWeek(?)}";
			String sqlLoadStartDate = "{call wb_LoadSummaryTx_DateRange_Week2_Start(?)}";
			String sqlLoadEndDate = "{call wb_LoadSummaryTx_DateRange_Week2_End(?)}";
			pstm = connection.prepareStatement(sql);
			pstmLoadStartDate = connection.prepareStatement(sqlLoadStartDate);
			pstmLoadEndDate = connection.prepareStatement(sqlLoadEndDate);
			
			//insert load current week sql parameter
			pstm.setString(1,  lDate);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute  load current week store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				mdlDateRange.Week = jrs.getString("Week2");
			}
			
			
			//insert load start date sql parameter
			pstmLoadStartDate.setString(1,  mdlDateRange.Week);
			command = pstmLoadStartDate.toString();
			pstmLoadStartDate.addBatch();
			
			//execute  load start date store procedure
			jrsLoadStartDate = pstmLoadStartDate.executeQuery();
			
			while(jrsLoadStartDate.next()){
				mdlDateRange.StartDate = jrsLoadStartDate.getString("Start");
			}
			
			
			//insert load end date sql parameter
			pstmLoadEndDate.setString(1,  mdlDateRange.Week);
			command = pstmLoadEndDate.toString();
			pstmLoadEndDate.addBatch();
			
			//execute  load end date store procedure
			jrsLoadEndDate = pstmLoadEndDate.executeQuery();
			
			while(jrsLoadEndDate.next()){
				mdlDateRange.EndDate = jrsLoadEndDate.getString("End");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadDateRange2", command, user);
		}
		finally {
			try{
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (pstmLoadStartDate != null) {
					 pstm.close();
				 }
				 if (pstmLoadEndDate != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
				 if (jrsLoadStartDate != null) {
					 jrsLoadStartDate.close();
				 }
				 if (jrsLoadEndDate != null) {
					 jrsLoadEndDate.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadDateRange2", "close opened connection protocol", user);
			}
		}

		return mdlDateRange;
	}
		
	//Validasi 1 bulan(tanggal awal bulan dan tanggal akhir bulan)
    public static model.mdlDateRange LoadDateRange1Month() {
        model.mdlDateRange lDateRange = new model.mdlDateRange();
        //String dateMonth = null;
        Date begining, end;

        SimpleDateFormat srcDf = new SimpleDateFormat("dd MMM yyyy");
        
            Calendar calendar = getCalendarForNow();
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            setTimeToBeginningOfDay(calendar);
            begining = calendar.getTime();
            //SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            // Output "Wed Sep 26 14:23:28 EST 2012
            //dateMonth = srcDf.format(begining);
            lDateRange.StartDate = srcDf.format(begining);
       
//            Calendar calendar = getCalendarForNow();
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            setTimeToEndofDay(calendar);
            end = calendar.getTime();
            //dateMonth = srcDf.format(end);
            lDateRange.EndDate = srcDf.format(end);

        return lDateRange;
    }
    
    private static Calendar getCalendarForNow() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }
    
    private static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }
    
    private static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }
    
}
