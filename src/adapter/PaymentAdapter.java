package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlData;
import model.mdlTransactionPayment;
import model.mdlUser;

public class PaymentAdapter {

	//for approval doc menu
	public static List<model.mdlTransactionPayment> LoadTransactionPaymentByParam(String lBrandID,String lAreaID, mdlUser pUser) {
		
		//define local variable
		List<model.mdlTransactionPayment> TransactionPaymentList = new ArrayList<model.mdlTransactionPayment>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadTransactionPaymentByParam(?,?,?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, "'%"+lBrandID+"%'");
			pstm.setString(2, "'%"+lAreaID+"%'");
			pstm.setString(3, pUser.Brand);
			pstm.setString(4, pUser.Area);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlTransactionPayment TransactionPayment = new model.mdlTransactionPayment();
				
				TransactionPayment.setPaymentID(jrs.getString("PaymentID"));
				TransactionPayment.setPaymentDate(ConvertDateTimeHelper.formatDate(jrs.getString("PaymentDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				TransactionPayment.setTransactionID(jrs.getString("TransactionID"));
				TransactionPayment.setMerchantArea(jrs.getString("AreaName"));
				TransactionPayment.setMerchantID(jrs.getString("MerchantID"));
				TransactionPayment.setMerchantName(jrs.getString("MerchantName"));
				TransactionPayment.setMerchantPIC(jrs.getString("PIC"));
				TransactionPayment.setPaymentAmount(jrs.getInt("PaymentAmount"));
				TransactionPayment.setsPaymentAmount("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("PaymentAmount"))+ " ,-");
				
				if(jrs.getString("Status").contentEquals("Verified"))
					TransactionPayment.setStatus("Approved");
				else
					TransactionPayment.setStatus(jrs.getString("Status"));
				
				TransactionPayment.setLastUpdateBy(jrs.getString("LastUpdateBy"));
				
				if(jrs.getString("LastDate") == null)
					TransactionPayment.setLastDate("");
				else
					TransactionPayment.setLastDate(ConvertDateTimeHelper.formatDate(jrs.getString("LastDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				
				if(jrs.getString("Status").contentEquals("Waiting"))
				{
					TransactionPayment.setButtonStatus("enabled");
					TransactionPayment.setButtonStatusReject("enabled");
				}
				else
				{
					TransactionPayment.setButtonStatus("disabled");
					TransactionPayment.setButtonStatusReject("disabled");
				}
				
				TransactionPaymentList.add(TransactionPayment);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTransactionPaymentByParam", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadTransactionPaymentByParam", "close opened connection protocol", pUser.UserId);
			}
		}

		return TransactionPaymentList;
	}

	public static String ApproveTransactionPayment(String lPaymentID, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		//call store procedure
		String sql = "{call wb_ApproveTransactionPayment(?,?,?)}";
		pstm = connection.prepareStatement(sql);
		
		//insert sql parameter
		pstm.setString(1,  lPaymentID);
		pstm.setString(2, user);
		pstm.setString(3, LocalDateTime.now().toString());
		command = pstm.toString();
		pstm.addBatch();
		pstm.executeBatch();
		
		result = "Success Approve Payment Document";
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "ApproveTransactionPayment", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "ApproveTransactionPayment", "close opened connection protocol", user);
			}
		}
		
		return result;
	}
	
	//for verification doc menu
	public static List<model.mdlTransactionPayment> LoadPaymentApprovedByParam(String lBrandID,String lAreaID, mdlUser pUser) {
		
		//define local variable
		List<model.mdlTransactionPayment> PaymentApprovedList = new ArrayList<model.mdlTransactionPayment>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadPaymentApprovedByParam(?,?,?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, "'%"+lBrandID+"%'");
			pstm.setString(2, "'%"+lAreaID+"%'");
			pstm.setString(3, pUser.Brand);
			pstm.setString(4, pUser.Area);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlTransactionPayment PaymentApproved = new model.mdlTransactionPayment();
				
				PaymentApproved.setPaymentID(jrs.getString("PaymentID"));
				PaymentApproved.setPaymentDate(ConvertDateTimeHelper.formatDate(jrs.getString("PaymentDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				PaymentApproved.setTransactionID(jrs.getString("TransactionID"));
				PaymentApproved.setMerchantArea(jrs.getString("AreaName"));
				PaymentApproved.setMerchantID(jrs.getString("MerchantID"));
				PaymentApproved.setMerchantName(jrs.getString("MerchantName"));
				PaymentApproved.setMerchantPIC(jrs.getString("PIC"));
				PaymentApproved.setsPaymentAmount("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("PaymentAmount"))+ " ,-");
				PaymentApproved.setStatus(jrs.getString("Status"));
				PaymentApproved.setRefNo(jrs.getString("RefNo"));
				PaymentApproved.setVerifiedBy(jrs.getString("VerifiedBy"));
				
				if(jrs.getString("VerifiedDate") == null)
					PaymentApproved.setVerifiedDate("");
				else
					PaymentApproved.setVerifiedDate(ConvertDateTimeHelper.formatDate(jrs.getString("VerifiedDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				
				if(jrs.getString("Status").contentEquals("Approved"))
					PaymentApproved.setButtonStatus("enabled");
				else
					PaymentApproved.setButtonStatus("disabled");
				
				PaymentApproved.setMerchantUserName(jrs.getString("Username"));
				
				PaymentApprovedList.add(PaymentApproved);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadPaymentApprovedByParam", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadPaymentApprovedByParam", "close opened connection protocol", pUser.UserId);
			}
		}

		return PaymentApprovedList;
	}

	public static String VerifyTransactionPayment(model.mdlTransactionPayment lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstmVerifyTransactionPayment = null;
		PreparedStatement pstmInsertMerchantInbox = null;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		connection.setAutoCommit(false); //for transaction scope need
		
		//call store procedure
		String sqlVerifyTransactionPayment = "{call wb_VerifyTransactionPayment(?,?,?,?)}";
		String sqlInsertMerchantInbox = "{call wb_InsertMerchantInbox(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		pstmVerifyTransactionPayment = connection.prepareStatement(sqlVerifyTransactionPayment);
		pstmInsertMerchantInbox = connection.prepareStatement(sqlInsertMerchantInbox);
		
		//insert sql parameter for VerifyTransactionPayment
		pstmVerifyTransactionPayment.setString(1,  ValidateNull.NulltoStringEmpty(lParam.getPaymentID()));
		pstmVerifyTransactionPayment.setString(2, ValidateNull.NulltoStringEmpty(lParam.getRefNo()));
		pstmVerifyTransactionPayment.setString(3, user);
		pstmVerifyTransactionPayment.setString(4, LocalDateTime.now().toString());
		
		command = pstmVerifyTransactionPayment.toString();
		pstmVerifyTransactionPayment.executeUpdate();
		
		//insert sql parameter for InsertMerchantInbox
		pstmInsertMerchantInbox.setString(1, "Msg_Redeem_"+lParam.getPaymentID());
		pstmInsertMerchantInbox.setString(2, "Redeem GG POINT");
		pstmInsertMerchantInbox.setString(3, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
		pstmInsertMerchantInbox.setString(4, "Confirmation");
		pstmInsertMerchantInbox.setString(5, "Terima kasih telah menggunakan fasilitas GG POINT. Berikut ini adalah informasi transaksi yang telah Anda lakukan di GG POINT");
		pstmInsertMerchantInbox.setString(6, "REDEEM POINT");
		pstmInsertMerchantInbox.setString(7, "NEW");
		pstmInsertMerchantInbox.setString(8, "");
		pstmInsertMerchantInbox.setString(9, lParam.getMerchantID());
		pstmInsertMerchantInbox.setString(10, lParam.getTransactionID());
		pstmInsertMerchantInbox.setString(11, lParam.getRefNo());
		pstmInsertMerchantInbox.setString(12, "");
		pstmInsertMerchantInbox.setString(13, "");
		pstmInsertMerchantInbox.setString(14, ValidateNull.NulltoStringEmpty(user));
		pstmInsertMerchantInbox.setString(15, ValidateNull.NulltoStringEmpty(user));
		pstmInsertMerchantInbox.setString(16, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
		pstmInsertMerchantInbox.setString(17, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
		
		command = pstmVerifyTransactionPayment.toString() + pstmInsertMerchantInbox.toString();
		pstmInsertMerchantInbox.executeUpdate();
		
		//-- push notif to mobile
		//String apiKey = "AIzaSyCfHAaH-vkJwA_Tf_ffDDckvhBMG4dAbKc";
		String apiKey = "AAAANA_Uduk:APA91bETeqh3rCeuLxlKrhsBVMFpjzZ2PiBuW-Enqt0Im3t2HlSKPOYUqMfsCyDchVMRYKnKVchQljpBaYk4LJjMTpwrWHZKCmwISrE035hdFXkVZoIVbdmC_uxutqlRwxQxZ_43zJm1";
		String notificationID = user+"_PushNewRedeemMessage_"+lParam.getMerchantName()+"_"+ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
		model.mdlPush mdlPush = new model.mdlPush();
		
		model.mdlNotification mdlNotif = new model.mdlNotification();
		mdlNotif.setTitle("Send Redeem Message"); 
		mdlNotif.setBody("You got a new message");
		
		mdlData mdlData = new model.mdlData();
		mdlData.setTitle("Send Redeem Message");
		mdlData.setMsg("You got a new message"); 
		mdlData.setNotificationid(notificationID);
		List<String> userToken = new ArrayList<String>();
		userToken = PushNotificationAdapter.LoadTokenByUsername(lParam.getMerchantUserName(),user);
		
		mdlPush.setRegistration_ids(userToken);
		mdlPush.setPriority("high");
		mdlPush.setDelay_while_idle(false);
		mdlPush.setContent_available(true);
		mdlPush.setData(mdlData);
		
			String PushResult = PushNotificationAdapter.PostToGCMGeneral(apiKey, mdlPush);
			//end of push notif --
			if(PushResult.contentEquals("Push Failed")){
				if (connection != null) {
		            try {
		                System.err.print("Transaction is being rolled back");
		                connection.rollback();
		            } catch(SQLException excep) {
		            	LogAdapter.InsertLogExc(excep.toString(), "RollBackVerifyTransactionPayment", command , user);
		    			result = "Database Error";
		            }
		        }
				
				LogAdapter.InsertLogExc("Push Notif Verify Payment Failed", "VerifyTransactionPayment", command, user);
				result = "Silahkan hubungi admin";
			}
			else{
				connection.commit(); //commit transaction if all of the proccess is running well
				result = "Success Verify Payment Document";
			}
		}
		catch(Exception ex) {
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "RollBackVerifyTransactionPayment", command , user);
	    			result = "Database Error";
	            }
	        }
			
			LogAdapter.InsertLogExc(ex.toString(), "VerifyTransactionPayment", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstmVerifyTransactionPayment != null) {
					 pstmVerifyTransactionPayment.close();
				 }
				 if (pstmInsertMerchantInbox != null) {
					 pstmInsertMerchantInbox.close();
				 }
				 connection.setAutoCommit(true);
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "VerifyTransactionPayment", "close opened connection protocol", user);
			}
		}
		
		return result;
	}
	
	public static String RejectTransactionPayment(String lPaymentID, String lMerchantID, Integer lPaymentAmount, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmUpdateMerchantTransactionPaymentBalance = null;
		PreparedStatement pstmLoadMerchantTransactionPaymentBalance = null;
		ResultSet jrs = null;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		connection.setAutoCommit(false);
		
		//call store procedure
		String sql = "{call wb_RejectTransactionPayment(?)}";
		String sqlUpdateMerchantTransactionPaymentBalance = "{call wb_InsertMerchantTransactionPaymentBalance(?,?,?,?,?)}";
		String sqlLoadMerchantTransactionPaymentBalance = "{call wb_LoadTransactionPaymentBalanceByMerchant(?)}";
		
		pstm = connection.prepareStatement(sql);
		pstmUpdateMerchantTransactionPaymentBalance = connection.prepareStatement(sqlUpdateMerchantTransactionPaymentBalance);
		pstmLoadMerchantTransactionPaymentBalance = connection.prepareStatement(sqlLoadMerchantTransactionPaymentBalance);
		
		//insert sql parameter for reject transaction payment
		pstm.setString(1,  lPaymentID);
		command = pstm.toString();
		pstm.executeUpdate();
		//pstm.addBatch();
		//pstm.executeBatch();
		
		//insert sql parameter for load merchant transaction payment balance
		pstmLoadMerchantTransactionPaymentBalance.setString(1,  lMerchantID);
		command = pstmLoadMerchantTransactionPaymentBalance.toString();
		jrs = pstmLoadMerchantTransactionPaymentBalance.executeQuery();
		while(jrs.next())
		{
			Integer MerchantTransactionPaymentBalance = jrs.getInt("Balance")+lPaymentAmount;
			
			//insert sql parameter for insert merchant transaction payment balance
			pstmUpdateMerchantTransactionPaymentBalance.setString(1,  lMerchantID);
			pstmUpdateMerchantTransactionPaymentBalance.setString(2,  LocalDateTime.now().toString());
			pstmUpdateMerchantTransactionPaymentBalance.setString(3, "CANCEL PAYMENT");
			pstmUpdateMerchantTransactionPaymentBalance.setInt(4, lPaymentAmount);
			pstmUpdateMerchantTransactionPaymentBalance.setInt(5, MerchantTransactionPaymentBalance);
			command = pstmUpdateMerchantTransactionPaymentBalance.toString();
			pstmUpdateMerchantTransactionPaymentBalance.executeUpdate();
		}
		
		connection.commit(); //commit transaction if all of the proccess is running well
		
		result = "Success Reject Payment Document";
		}
		catch(Exception ex) {
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "RejectTransactionPayment", command , user);
	    			result = "Database Error";
	            }
	        }
			
			LogAdapter.InsertLogExc(ex.toString(), "RejectTransactionPayment", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
				 if (pstmUpdateMerchantTransactionPaymentBalance != null) {
					 pstmUpdateMerchantTransactionPaymentBalance.close();
				 }
				 if (pstmLoadMerchantTransactionPaymentBalance != null) {
					 pstmLoadMerchantTransactionPaymentBalance.close();
				 }
				 connection.setAutoCommit(true);
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "RejectTransactionPayment", "close opened connection protocol", user);
			}
		}
		
		return result;
	}

	//for pending approval doc menu number
	public static Integer LoadPendingApprovalDocNumber(String[] listBrandID, String[] listAreaID, String user) {
		
	//define local variable
	Integer PendingApprovalDocNumber = 0;
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	String command = "";
	
	StringBuilder builderBrand = new StringBuilder();
	StringBuilder builderArea = new StringBuilder();
	int indexParam = 1;
	
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			//String sql = "{call wb_LoadTransactionPaymentByParam(?,?)}";
			
			//set the number of parameter base on the number of brand list and area list
			for(int i=0; i<listBrandID.length; i++) {
				builderBrand.append("?,");
			}
			for(int j=0; j<listAreaID.length; j++) {
				builderArea.append("?,");
			}
			
			//			String sql = "SELECT a.Status "
			//						+ "FROM tx_transaction_payment a "
			//						+ "INNER JOIN tx_transaction b ON b.TransactionID=a.TransactionID "
			//						+ "INNER JOIN ms_merchant c ON c.MerchantID=b.MerchantID "
			//						+ "WHERE c.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") "
			//						+ "AND c.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ")";
			String sql = "SELECT a.Status,a.MerchantID "
					+ "FROM tx_transaction_payment a "
					+ "INNER JOIN ms_merchant c ON c.MerchantID=a.MerchantID "
					+ "WHERE c.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") "
					+ "AND c.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ")";
			pstm = connection.prepareStatement(sql);
			
			//set the value of parameter
			for(int x=0; x<listBrandID.length; x++) {
				pstm.setString(indexParam++, listBrandID[x]);
			}
			for(int y=0; y<listAreaID.length; y++) {
				pstm.setString(indexParam++, listAreaID[y]);
			}
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				if(jrs.getString("Status").contentEquals("Waiting"))
					PendingApprovalDocNumber++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadPendingApprovalDocNumber", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadPendingApprovalDocNumber", "close opened connection protocol", user);
			}
		}

	return PendingApprovalDocNumber;
	}

	//for pending verification doc menu number
	public static Integer LoadPendingVerificationDocNumber(String[] listBrandID, String[] listAreaID, String user) {
	//define local variable
	Integer PendingVerificationDocNumber = 0;
	Connection connection = null;
	PreparedStatement pstm = null;
	ResultSet jrs = null;
	String command = "";
	
	StringBuilder builderBrand = new StringBuilder();
	StringBuilder builderArea = new StringBuilder();
	int indexParam = 1;
	
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
//			String sql = "{call wb_LoadPaymentApprovedByParam(?,?)}";
			
			//set the number of parameter base on the number of brand list and area list
			for(int i=0; i<listBrandID.length; i++) {
				builderBrand.append("?,");
			}
			for(int j=0; j<listAreaID.length; j++) {
				builderArea.append("?,");
			}
			
			//			String sql = "SELECT a.Status "
			//						+ "FROM tx_transaction_payment a "
			//						+ "INNER JOIN tx_transaction b ON b.TransactionID=a.TransactionID "
			//						+ "INNER JOIN ms_merchant c ON c.MerchantID=b.MerchantID "
			//						+ "WHERE c.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") "
			//						+ "AND c.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ")";
			String sql = "SELECT a.Status,a.MerchantID "
					+ "FROM tx_transaction_payment a "
					+ "INNER JOIN ms_merchant c ON c.MerchantID=a.MerchantID "
					+ "WHERE c.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") "
					+ "AND c.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ")";
			pstm = connection.prepareStatement(sql);
			
			//set the value of parameter
			for(int x=0; x<listBrandID.length; x++) {
				pstm.setString(indexParam++, listBrandID[x]);
			}
			for(int y=0; y<listAreaID.length; y++) {
				pstm.setString(indexParam++, listAreaID[y]);
			}
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				if(jrs.getString("Status").contentEquals("Approved"))
					PendingVerificationDocNumber++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadPendingVerificationDocNumber", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadPendingVerificationDocNumber", "close opened connection protocol", user);
			}
		}

	return PendingVerificationDocNumber;
	}
	
}
