package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class LoginAdapter {

	public static boolean ValidasiLogin(String lUsername, String lPassword)
	{
		//define local variable
		boolean lCheck = false;
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_ValidasiLogin(?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  lUsername);
			pstm.setString(2,  lPasswordEnc);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				lCheck = true;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "ValidasiLogin", command, lUsername);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "ValidasiLogin", "close opened connection protocol", lUsername);
			}
		}
		
		return lCheck;
		
	}

}
