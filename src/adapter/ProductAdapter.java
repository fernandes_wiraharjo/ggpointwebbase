package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;
import model.mdlUser;

import java.util.Locale;
import java.text.NumberFormat;

public class ProductAdapter {
	
	public static List<model.mdlProduct> LoadProduct(mdlUser pUser){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlProduct> mdlProductList = new ArrayList<model.mdlProduct>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadProduct(?)}");
			pstm.setString(1, pUser.Brand);
			
			command = pstm.toString();
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlProduct mdlProduct = new model.mdlProduct();				
				mdlProduct.setProductID(rs.getString("ProductID"));
				mdlProduct.setProductName(rs.getString("ProductName"));
				mdlProduct.setPrice("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(rs.getInt("Price"))+ " ,-");
				mdlProduct.setStatus(rs.getString("Status"));
				mdlProductList.add(mdlProduct);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProduct", command, pUser.UserId);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadProduct", "close opened connection protocol", pUser.UserId);
			}
		}

		return mdlProductList;
	}
	
	public static String InsertProduct(model.mdlProduct lParam, String[] llistBrand, String user)
	{
		Connection connection = null;
		PreparedStatement pstmProduct = null;
		PreparedStatement pstmProductBrand = null;
		PreparedStatement pstmCheck = null;
		ResultSet jrsCheck = null;
		Boolean check = false;
		String command = "";
		String result = "";
		
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//check duplicate product id
			String sqlCheck = "{call wb_LoadProductById(?)}";
			pstmCheck = connection.prepareStatement(sqlCheck);
			
			//insert sql parameter and execute for check
			pstmCheck.setString(1,  lParam.getProductID());
			jrsCheck = pstmCheck.executeQuery();
			command = pstmCheck.toString();
			
			while(jrsCheck.next()){
				check = true;
			}
				//if no duplicate
				if(check==false)
				{
					connection.setAutoCommit(false);
					
					pstmProduct = connection.prepareStatement("{call wb_InsertProduct(?,?,?,?,?,?)}");
					pstmProduct.setString(1, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
					pstmProduct.setString(2, ValidateNull.NulltoStringEmpty(lParam.getProductName()));
					pstmProduct.setString(3, ValidateNull.NulltoStringEmpty(lParam.getPrice()));
					pstmProduct.setBoolean(4, StringAdapter.ChangeStringToBoolean(lParam.getStatus()));
					pstmProduct.setString(5, user);
					pstmProduct.setString(6, user);
					
//					pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getBrandID()));
//					pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getMerchantID()));
//					pstm.setString(6, ValidateNull.NulltoStringEmpty(lParam.getPoint()));

					command += pstmProduct.toString();
					pstmProduct.executeUpdate();
					
					for(String lBrand : llistBrand)
					{
						pstmProductBrand = connection.prepareStatement("{call wb_InsertProduct_Brand(?,?,?,?)}");
						pstmProductBrand.setString(1, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
						pstmProductBrand.setString(2, ValidateNull.NulltoStringEmpty(lBrand));
						pstmProductBrand.setString(3, user);
						pstmProductBrand.setString(4, user);
						command += pstmProductBrand.toString();
						pstmProductBrand.executeUpdate();
					}
					connection.commit();
					result = "SuccessInsert";
				}
				//if duplicate
				else {
					result = "The Product Id Already Exist";
				}
		}
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertProduct", command , user);
		  			result = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertProduct", command , user);
    			result = "Database Error";
			}
		finally{
			//close the opened connection
			try{
				if (pstmProduct != null) {
					pstmProduct.close();
				}
				if (pstmProductBrand != null) {
					pstmProductBrand.close();
				}
				if (pstmCheck != null) {
					 pstmCheck.close();
				 }
				connection.setAutoCommit(true);
				if (connection != null) {
					connection.close();
				}
				if (jrsCheck != null) {
					 jrsCheck.close();
				 }
			
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertProduct", "close opened connection protocol", user);
			}
		}
		return result;
	}
	
	public static String UpdateProduct(model.mdlProduct lParam, String[] llistBrand, String user)
	{
		Connection connection = null;
		PreparedStatement pstmProduct = null;
		PreparedStatement pstmProductBrandDelete = null;
		PreparedStatement pstmProductBrandInsert = null;
		
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			pstmProduct = connection.prepareStatement("{call wb_UpdateProduct(?,?,?,?,?)}");
			pstmProduct.setString(1, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
			pstmProduct.setString(2, ValidateNull.NulltoStringEmpty(lParam.getProductName()));
			pstmProduct.setString(3, ValidateNull.NulltoStringEmpty(lParam.getPrice()));
			pstmProduct.setBoolean(4, StringAdapter.ChangeStringToBoolean(lParam.getStatus()));
			pstmProduct.setString(5, user);
			
//			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getBrandID()));
//			pstm.setString(3, ValidateNull.NulltoStringEmpty(lParam.getMerchantID()));
//			pstm.setString(6, ValidateNull.NulltoStringEmpty(lParam.getPoint()));
			
			command += pstmProduct.toString();
			pstmProduct.executeUpdate();
			
			pstmProductBrandDelete = connection.prepareStatement("{call wb_DeleteProduct_Brand(?)}");
			pstmProductBrandDelete.setString(1, ValidateNull.NulltoStringEmpty(lParam.getProductID()));		
			command += pstmProductBrandDelete.toString();
			pstmProductBrandDelete.executeUpdate();
			
			for(String lBrand : llistBrand)
			{
				pstmProductBrandInsert = connection.prepareStatement("{call wb_InsertProduct_Brand(?,?,?,?)}");
				pstmProductBrandInsert.setString(1, ValidateNull.NulltoStringEmpty(lParam.getProductID()));
				pstmProductBrandInsert.setString(2, ValidateNull.NulltoStringEmpty(lBrand));
				pstmProductBrandInsert.setString(3, user);
				pstmProductBrandInsert.setString(4, user);
				command += pstmProductBrandInsert.toString();
				pstmProductBrandInsert.executeUpdate();
			}
			
			connection.commit();
			result = "SuccessUpdate";
		}
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateUserRule", command , user);
		  			result = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "UpdateProduct", command , user);
    			result = "Database Error";
			}
		finally{
			//close the opened connection
			try{
				if (pstmProduct != null) {
					pstmProduct.close();
				}
				if (pstmProductBrandDelete != null) {
					pstmProductBrandDelete.close();
				}
				if (pstmProductBrandInsert != null) {
					pstmProductBrandInsert.close();
				}
				connection.setAutoCommit(true);
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateProduct", "close opened connection protocol", user);
			}
		}
		return result;	
	}

	public static String DeleteProduct(String lProductId, String user){
		Connection connection = null;
		PreparedStatement pstmProduct = null;
		PreparedStatement pstmProductBrand = null;
		PreparedStatement pstmProductMerchant = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			pstmProduct = connection.prepareStatement("{call wb_DeleteProduct(?)}");
			pstmProduct.setString(1, ValidateNull.NulltoStringEmpty(lProductId));		
			command += pstmProduct.toString();
			pstmProduct.executeUpdate();	
			
			pstmProductBrand = connection.prepareStatement("{call wb_DeleteProduct_Brand(?)}");
			pstmProductBrand.setString(1, ValidateNull.NulltoStringEmpty(lProductId));		
			command += pstmProductBrand.toString();
			pstmProductBrand.executeUpdate();
			
			pstmProductMerchant = connection.prepareStatement("{call wb_DeleteProduct_Merchant(?)}");
			pstmProductMerchant.setString(1, ValidateNull.NulltoStringEmpty(lProductId));		
			command += pstmProductMerchant.toString();
			pstmProductMerchant.executeUpdate();
			
			connection.commit();
			result = "SuccessDelete";
		}
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionDeleteProduct", command , user);
		  			result = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "DeleteProduct", command , user);
    			result = "Database Error";
		}
		finally{
			//close the opened connection
			try{
				if (pstmProduct != null) {
					pstmProduct.close();
				}
				if (pstmProductBrand != null) {
					pstmProductBrand.close();
				}
				if (pstmProductMerchant != null) {
					pstmProductMerchant.close();
				}
				connection.setAutoCommit(true);
				if (connection != null) {
					connection.close();
				}
				
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteProduct", "close opened connection protocol", user);
			}
		}
		
		return result;	
	}

	public static List<model.mdlProduct> LoadProductByID(String lProductID,String lCommand, String user) {
		Connection connection = null;
		String command = "";
		List<model.mdlProduct> listmdlProduct = new ArrayList<model.mdlProduct>();

		PreparedStatement pstmLoadProduct = null;
		ResultSet jrs = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			String sqlLoadProduct = "{call wb_LoadProductById(?)}";
			pstmLoadProduct = connection.prepareStatement(sqlLoadProduct);
			pstmLoadProduct.setString(1, lProductID);
			command = pstmLoadProduct.toString();
			jrs = pstmLoadProduct.executeQuery();
									
			while(jrs.next()){
				model.mdlProduct mdlProduct = new model.mdlProduct();
				mdlProduct.setProductID(jrs.getString("ProductID"));
				mdlProduct.setProductName(jrs.getString("ProductName"));
				listmdlProduct.add(mdlProduct);
			}			
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadLoadProductByID", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstmLoadProduct != null) {
					pstmLoadProduct.close();
				}
				
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadLoadProductByID", "close opened connection protocol", user);
			}
		}
		return listmdlProduct;
	}

	//============================= product Brand =======================================//
	
	public static List<model.mdlProductBrand> LoadProduct_BrandbyProductID(String lProductID, String user){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlProductBrand> mdlProductBrandList = new ArrayList<model.mdlProductBrand>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadProduct_BrandbyProductId(?)}");
			pstm.setString(1, lProductID);
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlProductBrand mdlProductBrand = new model.mdlProductBrand();				
				mdlProductBrand.setBrandID(rs.getString("BrandID"));
				mdlProductBrand.setBrandName(rs.getString("BrandName"));
				mdlProductBrandList.add(mdlProductBrand);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProduct_BrandbyProductID", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadProduct_BrandbyProductID", "close opened connection protocol", user);
			}
		}

		return mdlProductBrandList;
	}
	
}
