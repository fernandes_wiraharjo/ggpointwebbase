package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;

import com.sun.jersey.core.util.StringIgnoreCaseKeyComparator;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlMerchant;
import model.mdlMerchantInbox;
import model.mdlUser;

public class IssueAdapter {

	public static List<model.mdlMerchantInbox> LoadIssueMessage(String lBrandID, mdlUser pUser) {
		
		//define local variable
		List<model.mdlMerchantInbox> IssueList = new ArrayList<model.mdlMerchantInbox>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadIssueMerchantInbox(?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, "'"+lBrandID+"'");
			pstm.setString(2, pUser.Area);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlMerchantInbox Issue = new model.mdlMerchantInbox();
				
				Issue.setMessageID(jrs.getString("MessageID"));
				Issue.setMerchantID(jrs.getString("MerchantID"));
				Issue.setDate(jrs.getString("Date"));
				Issue.setMessage(jrs.getString("Message"));
				Issue.setStatus(jrs.getString("Status"));
				Issue.setStatusDetail(jrs.getString("StatusDetail"));
				Issue.setMerchantName(jrs.getString("MerchantName"));
				Issue.setMerchantPIC(jrs.getString("PIC"));
				Issue.setRefNo(jrs.getString("RefNo"));
				Issue.setIssuedBy(jrs.getString("LastUpdateBy"));
				Issue.setIssuedDate(ConvertDateTimeHelper.formatDate(jrs.getString("LastDate"),"yyyy-MM-dd HH:mm:ss","dd-MM-yyyy HH:mm"));
				Issue.setTransactionPrice(jrs.getInt("PaymentAmount"));
				Issue.setMessageType(jrs.getString("MessageType"));
				Issue.setUserName(jrs.getString("Username"));
				
				IssueList.add(Issue);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadIssueMessage", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadIssueMessage", "close opened connection protocol", pUser.UserId);
			}
		}

		return IssueList;
	}

	public static String UpdateIssueMessage(mdlMerchantInbox lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmInsertMerchantBalance = null;
		int merchantBalance=0;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		connection.setAutoCommit(false); //for transaction scope need
		
		//call store procedure
		String sql = "{call wb_UpdateIssueMerchantInbox(?,?,?,?,?)}";
		//call store procedure for Insert Merchant Balance
		String sqlInsertMerchantBalance = "{call ws_InsertMerchantBalance(?,?,?,?,?)}";
		
		pstm = connection.prepareStatement(sql);
		pstmInsertMerchantBalance = connection.prepareStatement(sqlInsertMerchantBalance);	
		
		//insert sql parameter
		pstm.setString(1,  lParam.getContestReason());
		pstm.setString(2,  lParam.getContestFile());
		pstm.setString(3, user);
		pstm.setString(4, LocalDateTime.now().toString());
		pstm.setString(5, lParam.getMessageID());
		command = pstm.toString();
		pstm.executeUpdate();
//		pstm.addBatch();
//		pstm.executeBatch();
		
		//get the latest merchant balance
		model.mdlMerchantBalance mdlMerchantBalance = MerchantAdapter.LoadMerchantBalance(lParam.MerchantID);
		if(mdlMerchantBalance.Balance != null)
			merchantBalance = Integer.parseInt(mdlMerchantBalance.Balance);
		
		//insert sql parameter for insert merchant balance
		pstmInsertMerchantBalance.setString(1, ValidateNull.NulltoStringEmpty(lParam.MerchantID));
		pstmInsertMerchantBalance.setInt(2, ValidateNull.NulltoIntEmpty(merchantBalance - lParam.TransactionPrice));
		pstmInsertMerchantBalance.setString(3, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
		pstmInsertMerchantBalance.setInt(4, ValidateNull.NulltoIntEmpty(merchantBalance - lParam.TransactionPrice));
		pstmInsertMerchantBalance.setString(5, ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
		command = pstm.toString() + pstmInsertMerchantBalance.toString();
		pstmInsertMerchantBalance.executeUpdate();
		
		connection.commit(); //commit transaction if all of the proccess is running well
		result = "Success Close Issue";
		}
		catch(Exception ex) {
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "RollBackUpdateIssueMessage", command , user);
	    			result = "Database Error";
	            }
	        }
			
			LogAdapter.InsertLogExc(ex.toString(), "UpdateIssueMessage", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (pstmInsertMerchantBalance != null) {
					 pstmInsertMerchantBalance.close();
				 }
				 connection.setAutoCommit(true);
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "UpdateIssueMessage", "close opened connection protocol", user);
			}
		}
		
		return result;
	}
	
	public static Integer LoadIssueNumber(String[] listBrandID, String[] listAreaID, String user) {
		
		//define local variable
		int IssueNumber = 0;
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		
		StringBuilder builderBrand = new StringBuilder();
		StringBuilder builderArea = new StringBuilder();
		int indexParam = 1;
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
//			String sql = "{call wb_LoadIssueNumber(?,?)}";
			
			//set the number of parameter base on the number of brand list and area list
			for(int i=0; i<listBrandID.length; i++) {
				builderBrand.append("?,");
			}
			for(int j=0; j<listAreaID.length; j++) {
				builderArea.append("?,");
			}
			
			String sql = "SELECT a.MessageID FROM tx_merchant_inbox a "
					+ "INNER JOIN ms_merchant b ON b.MerchantID = a.MerchantID "
					+ "WHERE a.Status='ISSUED' AND b.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") " 
					+ "AND b.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ")";
			pstm = connection.prepareStatement(sql);
//			pstm.addBatch();
//			pstm.executeBatch();
			
			//set the value of parameter
			for(int x=0; x<listBrandID.length; x++) {
				pstm.setString(indexParam++, listBrandID[x]);
			}
			for(int y=0; y<listAreaID.length; y++) {
				pstm.setString(indexParam++, listAreaID[y]);
			}
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			command = pstm.toString();
			
			while(jrs.next()){
				IssueNumber++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadIssueNumber", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadIssueNumber", "close opened connection protocol", user);
			}
		}

		return IssueNumber;
	}

}
