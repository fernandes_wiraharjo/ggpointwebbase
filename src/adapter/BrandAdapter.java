package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlMerchant;
import model.mdlPointConversion;
import model.mdlUser;

public class BrandAdapter {

	public static List<model.mdlBrand> LoadBrand(mdlUser pUser) {
		
		//define local variable
		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
					
			//call store procedure
			String sql = "{call wb_LoadBrand(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  pUser.Brand);
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlBrand Brand = new model.mdlBrand();
				
				Brand.setBrandID(jrs.getString("BrandID"));
				Brand.setBrandName(jrs.getString("BrandName"));
				
				BrandList.add(Brand);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadBrand", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBrand", "close opened connection protocol", pUser.UserId);
			}
		}

		return BrandList;
	}
	
	public static List<model.mdlBrand> LoadBrandMenuUser(String user) {
		
		//define local variable
		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
					
			//call store procedure
			String sql = "{call wb_LoadBrandMenuUser()}";
			pstm = connection.prepareStatement(sql);
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlBrand Brand = new model.mdlBrand();
				
				Brand.setBrandID(jrs.getString("BrandID"));
				Brand.setBrandName(jrs.getString("BrandName"));
				
				BrandList.add(Brand);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadBrandMenuUser", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBrandMenuUser", "close opened connection protocol", user);
			}
		}

		return BrandList;
	}

	public static List<model.mdlPointConversion> LoadPointConversion(mdlUser pUser) {
		
		//define local variable
		List<model.mdlPointConversion> PointConversionList = new ArrayList<model.mdlPointConversion>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
					
			//call store procedure
			String sql = "{call wb_LoadPointConversion(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  pUser.Brand);
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlPointConversion PointConversion = new model.mdlPointConversion();
				
				PointConversion.setBrandID(jrs.getString("BrandID"));
				PointConversion.setPoint(jrs.getInt("Point"));
				PointConversion.setPrice(jrs.getInt("Price"));
				PointConversion.setsPrice("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("Price")));
				
				PointConversionList.add(PointConversion);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadPointConversion", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadPointConversion", "close opened connection protocol", pUser.UserId);
			}
		}

		return PointConversionList;
	}

	public static String InsertPointConversion(mdlPointConversion lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmCheck = null;
		ResultSet jrs = null;
		Boolean check = false;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		//check duplicate brand id
		String sqlCheck = "{call wb_LoadPointConversionByBrand(?)}";
		pstmCheck = connection.prepareStatement(sqlCheck);
		
		//insert sql parameter and execute for check
		pstmCheck.setString(1,  lParam.getBrandID());
		jrs = pstmCheck.executeQuery();
		command = pstmCheck.toString();
		
		while(jrs.next()){
			check = true;
		}
			//if no duplicate
			if(check==false)
			{
				//call store procedure
				String sql = "{call wb_InsertPointConversion(?,?,?,?,?,?,?)}";
				pstm = connection.prepareStatement(sql);
				
				//insert sql parameter
				pstm.setString(1,  lParam.getBrandID());
				pstm.setInt(2,  lParam.getPoint());
				pstm.setInt(3, lParam.getPrice());
				pstm.setString(4, user);
				pstm.setString(5, user);
				pstm.setString(6, LocalDateTime.now().toString());
				pstm.setString(7, LocalDateTime.now().toString());
				command = pstm.toString();
				pstm.addBatch();
				pstm.executeBatch();
				
				result = "Success Insert Point Conversion";
			}
			else 
			{
				result = "The Brand Id Already Exist";
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "InsertPointConversion", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (pstmCheck != null) {
					 pstmCheck.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "InsertPointConversion", "close opened connection protocol", user);
			}
		}
		
		return result;
	}

	public static String UpdatePointConversion(mdlPointConversion lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		//call store procedure
		String sql = "{call wb_UpdatePointConversion(?,?,?,?)}";
		pstm = connection.prepareStatement(sql);
		
		//insert sql parameter
		pstm.setInt(1,  lParam.getPrice());
		pstm.setString(2, user);
		pstm.setString(3, LocalDateTime.now().toString());
		pstm.setString(4, lParam.getBrandID());
		command = pstm.toString();
		pstm.addBatch();
		pstm.executeBatch();
		
		result = "Success Update Point Conversion";
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "UpdatePointConversion", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "UpdatePointConversion", "close opened connection protocol", user);
			}
		}
		
		return result;
	}

	public static List<model.mdlBrand> LoadBrandbyProductID(String lProductID, String user){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlBrand> mdlBrandList = new ArrayList<model.mdlBrand>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadBrandbyProduct(?)}");
			pstm.setString(1, lProductID);
			command = pstm.toString();
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlBrand mdlBrand = new model.mdlBrand();				
				mdlBrand.setBrandID(rs.getString("BrandID"));
				mdlBrand.setBrandName(rs.getString("BrandName"));
				mdlBrandList.add(mdlBrand);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadBrandByProductID", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadBrandByProductID", "close opened connection protocol", user);
			}
		}

		return mdlBrandList;
	}
	
}
