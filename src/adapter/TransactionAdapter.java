package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlMerchant;
import model.mdlTransactionPayment;
import model.mdlUser;

public class TransactionAdapter {

	public static List<model.mdlTransaction> LoadTransaction(String lBrandID,String lAreaID, mdlUser pUser) {
		
		//define local variable
		List<model.mdlTransaction> TransactionList = new ArrayList<model.mdlTransaction>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		Integer payAmount;
		Boolean flagStatusPayment;
//		Connection connection2 = null;
		PreparedStatement pstm2 = null;
		ResultSet jrs2 = null;
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadTransaction(?,?,?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, "'%"+lBrandID+"%'");
			pstm.setString(2, "'%"+lAreaID+"%'");
			pstm.setString(3, pUser.Brand);
			pstm.setString(4, pUser.Area);
			
			command = pstm.toString();
//			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				
				//LOAD TRANSACTION PAYMENT
				payAmount = 0;
				flagStatusPayment = true;
//				try{
					//define connection
//					connection2 = database.RowSetAdapter.getConnection();
					
					//call store procedure
					String sql2 = "{call wb_LoadTransactionPayment(?)}";
					pstm2 = connection.prepareStatement(sql2);
					
					//insert parameter
					pstm2.setString(1, jrs.getString("TransactionID"));
					command = pstm2.toString();
//					pstm2.addBatch();
					
					//execute store procedure
					jrs2 = pstm2.executeQuery();
					
					while(jrs2.next()){
						payAmount += jrs2.getInt("PaymentAmount");
						if(jrs2.getString("Status").contentEquals("Waiting"))
							flagStatusPayment = false;
					}
//				}
//				catch(Exception ex) {
//					LogAdapter.InsertLogExc(ex.toString(), "LoadTransactionPayment", Globals.gCommand, Globals.user);
//					return TransactionList;
//				}
//				finally {
//					try {
//						//close the opened connection
//						 if (pstm2 != null) {
//							 pstm2.close();
//						 }
//						 if (connection2 != null) {
//							 connection2.close();
//						 }
//						 if (jrs2 != null) {
//							 jrs2.close();
//						 }
//					}
//					catch(Exception e) {
//						LogAdapter.InsertLogExc(e.toString(), "LoadTransactionPayment", "close opened connection protocol", Globals.user);
//						return TransactionList;
//					}
//				}
				
				
				//LOAD TRANSACTION
				model.mdlTransaction Transaction = new model.mdlTransaction();
				Transaction.setTransactionID(jrs.getString("TransactionID"));
				Transaction.setMerchantName(jrs.getString("MerchantName"));
				Transaction.setMerchantArea(jrs.getString("AreaName"));
				Transaction.setTransactionDate(ConvertDateTimeHelper.formatDate(jrs.getString("TransactionDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				Transaction.setTransactionPrice("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("TransactionPrice"))+ " ,-");
				Transaction.setMerchantPIC(jrs.getString("PIC"));
				Transaction.setiLastPrice(jrs.getInt("TransactionPrice")-payAmount);
				Transaction.setsLastPrice("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(Transaction.getiLastPrice()) + " ,-");
				if(Transaction.getiLastPrice() == 0)
					Transaction.setButtonStatus("disabled");
				else if(flagStatusPayment==true)
					Transaction.setButtonStatus("enabled");
				else
					Transaction.setButtonStatus("disabled");
				
				TransactionList.add(Transaction);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTransaction", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (pstm2 != null) {
					 pstm2.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
				 if (jrs2 != null) {
					 jrs2.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadTransaction", "close opened connection protocol", pUser.UserId);
			}
		}

		return TransactionList;
	}

	public static List<model.mdlTransaction> LoadTransactionPaymentBalance(String lBrandID,String lAreaID, mdlUser pUser) {
		
		//define local variable
		List<model.mdlTransaction> TransactionList = new ArrayList<model.mdlTransaction>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";

		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadTransactionPaymentBalance(?,?,?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, "'%"+lBrandID+"%'");
			pstm.setString(2, "'%"+lAreaID+"%'");
			pstm.setString(3, pUser.Brand);
			pstm.setString(4, pUser.Area);
			
			command = pstm.toString();
			//pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				
				//LOAD TRANSACTION
				model.mdlTransaction Transaction = new model.mdlTransaction();
				Transaction.setMerchantID(jrs.getString("MerchantID"));
				Transaction.setMerchantName(jrs.getString("MerchantName"));
				Transaction.setMerchantArea(jrs.getString("AreaName"));
				//Transaction.setTransactionPrice("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("TotalTransaction"))+ " ,-");
				Transaction.setMerchantPIC(jrs.getString("PIC"));
				Transaction.setsLastPrice("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("Balance")) + " ,-");
				if(jrs.getInt("Balance") == 0)
					Transaction.setButtonStatus("disabled");
				else
					Transaction.setButtonStatus("enabled");
				
				TransactionList.add(Transaction);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTransactionPaymentBalance", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadTransactionPaymentBalance", "close opened connection protocol", pUser.UserId);
			}
		}

		return TransactionList;
	}
	
	public static String InsertTransactionPayment(mdlTransactionPayment lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmUpdateMerchantTransactionPaymentBalance = null;
		String command = "";
		String result = "";
		Integer MerchantTransactionPaymentBalance = Integer.parseInt(lParam.getTransactionPrice())-lParam.getPaymentAmount();
		//Integer MerchantTransactionPaymentTotalPayment = lParam.getTotalTransaction()-MerchantTransactionPaymentBalance;
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		connection.setAutoCommit(false);
		
		//call store procedure
		//String sql = "{call wb_InsertPaymentApproval(?,?,?,?,?,?,?,?,?,?)}";
		String sql = "{call wb_InsertPaymentApprovalByMerchant(?,?,?,?,?,?,?,?,?,?)}";
		String sqlUpdateMerchantTransactionPaymentBalance = "{call wb_InsertMerchantTransactionPaymentBalance(?,?,?,?,?)}";
		
		pstm = connection.prepareStatement(sql);
		pstmUpdateMerchantTransactionPaymentBalance = connection.prepareStatement(sqlUpdateMerchantTransactionPaymentBalance);
		
		//insert sql parameter for insert payment approval by merchant
		pstm.setString(1,  lParam.getPaymentID());
		pstm.setString(2,  lParam.getPaymentDate());
		//pstm.setString(3, lParam.getTransactionID());
		pstm.setString(3, lParam.getMerchantID());
		pstm.setInt(4, lParam.getPaymentAmount());
		pstm.setString(5, lParam.getStatus());
		pstm.setString(6, lParam.getRefNo());
		pstm.setString(7, user);
		pstm.setString(8, "");
		pstm.setString(9, "");
		pstm.setString(10, LocalDateTime.now().toString());
		command = pstm.toString();
		pstm.executeUpdate();
		//pstm.addBatch();
		//pstm.executeBatch();
		
		//insert sql parameter for insert merchant transaction payment balance
		pstmUpdateMerchantTransactionPaymentBalance.setString(1,  lParam.getMerchantID());
		pstmUpdateMerchantTransactionPaymentBalance.setString(2, lParam.getPaymentDate());
		pstmUpdateMerchantTransactionPaymentBalance.setString(3, "PAYMENT");
		pstmUpdateMerchantTransactionPaymentBalance.setInt(4,  lParam.getPaymentAmount());
		pstmUpdateMerchantTransactionPaymentBalance.setInt(5, MerchantTransactionPaymentBalance);
		command = pstmUpdateMerchantTransactionPaymentBalance.toString();
		pstmUpdateMerchantTransactionPaymentBalance.executeUpdate();
		
		connection.commit(); //commit transaction if all of the proccess is running well
		
		result = "Success Insert Payment";
		}
		catch(Exception ex) {
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "InsertTransactionPayment", command , user);
	    			result = "Database Error";
	            }
	        }
			
			LogAdapter.InsertLogExc(ex.toString(), "InsertTransactionPayment", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (pstmUpdateMerchantTransactionPaymentBalance != null) {
					 pstmUpdateMerchantTransactionPaymentBalance.close();
				 }
				 connection.setAutoCommit(true);
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "InsertTransactionPayment", "close opened connection protocol", user);
			}
		}
		
		return result;
	}

	public static List<model.mdlTransactionPayment> LoadTransactionPayment(String lTransactionID,String lTransactionPrice, String user) {
		
		//define local variable
		List<model.mdlTransactionPayment> TransactionPaymentList = new ArrayList<model.mdlTransactionPayment>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadTransactionPayment(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, lTransactionID);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlTransactionPayment TransactionPayment = new model.mdlTransactionPayment();
				
				TransactionPayment.setPaymentID(jrs.getString("PaymentID"));
				TransactionPayment.setPaymentDate(ConvertDateTimeHelper.formatDate(jrs.getString("PaymentDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
//				TransactionPayment.setTransactionPrice(lTransactionPrice);
				TransactionPayment.setsPaymentAmount("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("PaymentAmount"))+ " ,-");
				TransactionPayment.setStatus(jrs.getString("Status"));
				
				TransactionPaymentList.add(TransactionPayment);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTransactionPayment", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadTransactionPayment", "close opened connection protocol", user);
			}
		}

		return TransactionPaymentList;
	}

	public static List<model.mdlTransactionPayment> LoadTransactionPaymentByMerchant(String lMerchantID, String user) {
		
		//define local variable
		List<model.mdlTransactionPayment> TransactionPaymentList = new ArrayList<model.mdlTransactionPayment>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadTransactionPaymentByMerchant(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, lMerchantID);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlTransactionPayment TransactionPayment = new model.mdlTransactionPayment();
				
				TransactionPayment.setPaymentID(jrs.getString("PaymentID"));
				TransactionPayment.setPaymentDate(ConvertDateTimeHelper.formatDate(jrs.getString("PaymentDate"), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss"));
				//TransactionPayment.setTransactionPrice(lTransactionPrice);
				TransactionPayment.setsPaymentAmount("Rp. "+ NumberFormat.getNumberInstance(Locale.GERMAN).format(jrs.getInt("PaymentAmount"))+ " ,-");
				TransactionPayment.setStatus(jrs.getString("Status"));
				
				TransactionPaymentList.add(TransactionPayment);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadTransactionPaymentByMerchant", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadTransactionPaymentByMerchant", "close opened connection protocol", user);
			}
		}

		return TransactionPaymentList;
	}
	
	//for pending payment approval menu number
	public static Integer LoadPendingPaymentApprovalNumber(String[] listBrandID, String[] listAreaID, String user) {
		
		//define local variable
		Integer PendingPaymentApprovalNumber = 0;
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		
		StringBuilder builderBrand = new StringBuilder();
		StringBuilder builderArea = new StringBuilder();
		int indexParam = 1;
		
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
//			String sql = "{call wb_LoadTransaction(?,?)}";
			
			//set the number of parameter base on the number of brand list and area list
			for(int i=0; i<listBrandID.length; i++) {
				builderBrand.append("?,");
			}
			for(int j=0; j<listAreaID.length; j++) {
				builderArea.append("?,");
			}
			
			String sql = "SELECT a.MerchantID,a.Balance "
						+ "FROM tx_transactionpayment_balance a "
						+ "INNER JOIN ms_merchant b ON b.MerchantID=a.MerchantID "
						+ "INNER JOIN "
							+ "(SELECT x.MerchantID,MAX(x.Date) AS LastDate "
							+ "FROM tx_transactionpayment_balance x "
							+ "INNER JOIN ms_merchant y ON y.MerchantID=x.MerchantID "
							+ "WHERE y.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") "
							+ "AND y.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ") "
							+ "GROUP BY x.MerchantID) "
							+ "c ON c.MerchantID=a.MerchantID AND c.LastDate=a.Date "
						+ "WHERE b.BrandID IN ("+builderBrand.toString()+ ") "
						+ "AND b.AreaID IN ("+builderArea.toString()+ ")";
			pstm = connection.prepareStatement(sql);
			//pstm.addBatch();
			//pstm.executeBatch();
			
			//set the value of parameter
			for(int x=0; x<listBrandID.length; x++) {
				pstm.setString(indexParam++, listBrandID[x]);
			}
			for(int y=0; y<listAreaID.length; y++) {
				pstm.setString(indexParam++, listAreaID[y]);
			}
			for(int z=0; z<listBrandID.length; z++) {
				pstm.setString(indexParam++, listBrandID[z]);
			}
			for(int w=0; w<listAreaID.length; w++) {
				pstm.setString(indexParam++, listAreaID[w]);
			}
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				if(jrs.getInt("Balance")!=0)
					PendingPaymentApprovalNumber++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadPendingPaymentApprovalNumber", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadPendingPaymentApprovalNumber", "close opened connection protocol", user);
			}
		}

		return PendingPaymentApprovalNumber;
		}
		
	//for pending payment approval menu number
//	public static Integer LoadPendingPaymentApprovalNumber(String[] listBrandID, String[] listAreaID) {
//	
//	//define local variable
//	Integer PendingPaymentApprovalNumber = 0;
//	Connection connection = null;
//	PreparedStatement pstm = null;
//	ResultSet jrs = null;
//	Globals.gCommand = "";
//			
//	Integer payAmount;
//	//Connection connection2 = null;
//	PreparedStatement pstm2 = null;
//	ResultSet jrs2 = null;
//	
//	StringBuilder builderBrand = new StringBuilder();
//	StringBuilder builderArea = new StringBuilder();
//	int indexParam = 1;
//	
//	try {
//		//define connection
//		connection = database.RowSetAdapter.getConnection();
//		
//		//call store procedure
////		String sql = "{call wb_LoadTransaction(?,?)}";
//		
//		//set the number of parameter base on the number of brand list and area list
//		for(int i=0; i<listBrandID.length; i++) {
//			builderBrand.append("?,");
//		}
//		for(int j=0; j<listAreaID.length; j++) {
//			builderArea.append("?,");
//		}
//		
//		String sql = "SELECT a.TransactionID,a.TransactionPrice "
//					+ "FROM tx_transaction a "
//					+ "INNER JOIN ms_merchant b ON b.MerchantID=a.MerchantID "
//					+ "WHERE b.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") "
//					+ "AND b.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ")";
//		pstm = connection.prepareStatement(sql);
//		//pstm.addBatch();
//		//pstm.executeBatch();
//		
//		//set the value of parameter
//		for(int x=0; x<listBrandID.length; x++) {
//			pstm.setString(indexParam++, listBrandID[x]);
//		}
//		for(int y=0; y<listAreaID.length; y++) {
//			pstm.setString(indexParam++, listAreaID[y]);
//		}
//		
//		//execute store procedure
//		jrs = pstm.executeQuery();
//		Globals.gCommand = pstm.toString();
//		
//		while(jrs.next()){
//			
//			//LOAD TRANSACTION PAYMENT
//			payAmount = 0;
////			try{
//				//define connection
////				connection2 = database.RowSetAdapter.getConnection();
//				
//				//call store procedure
//				String sql2 = "{call wb_LoadTransactionPayment(?)}";
//				pstm2 = connection.prepareStatement(sql2);
//				
//				//insert parameter
//				pstm2.setString(1, jrs.getString("TransactionID"));
//				Globals.gCommand = pstm2.toString();
////				pstm2.addBatch();
////				pstm2.executeBatch();
//				
//				//execute store procedure
//				jrs2 = pstm2.executeQuery();
//				
//				while(jrs2.next()){
//					payAmount += jrs2.getInt("PaymentAmount");
//				}
//				if(payAmount!=Integer.parseInt(jrs.getString("TransactionPrice")))
//					PendingPaymentApprovalNumber++;
//				
////			}
////			catch(Exception ex) {
////				LogAdapter.InsertLogExc(ex.toString(), "LoadTransactionPaymentForPendingPaymentApprovalNumber", Globals.gCommand, Globals.user);
////			}
////			finally {
////				try {
////					//close the opened connection
////					 if (pstm2 != null) {
////						 pstm2.close();
////					 }
////					 if (connection2 != null) {
////						 connection2.close();
////					 }
////					 if (jrs2 != null) {
////						 jrs2.close();
////					 }
////				}
////				catch(Exception e) {
////					LogAdapter.InsertLogExc(e.toString(), "LoadTransactionPaymentForPendingPaymentApprovalNumber", "close opened connection protocol", Globals.user);
////				}
////			}
//		}
//	}
//	catch(Exception ex) {
//		LogAdapter.InsertLogExc(ex.toString(), "LoadPendingPaymentApprovalNumber", Globals.gCommand, Globals.user);
//	}
//	finally {
//		try {
//			//close the opened connection
//			 if (pstm != null) {
//				 pstm.close();
//			 }
//			 if (pstm2 != null) {
//				 pstm2.close();
//			 }
//			 if (connection != null) {
//				 connection.close();
//			 }
//			 if (jrs != null) {
//				 jrs.close();
//			 }
//			 if (jrs2 != null) {
//				 jrs2.close();
//			 }
//		}
//		catch(Exception e) {
//			LogAdapter.InsertLogExc(e.toString(), "LoadPendingPaymentApprovalNumber", "close opened connection protocol", Globals.user);
//		}
//	}
//
//	return PendingPaymentApprovalNumber;
//	}
	
}
