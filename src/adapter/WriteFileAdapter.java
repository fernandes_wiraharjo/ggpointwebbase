package adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import model.Globals;


public class WriteFileAdapter {

	public static String Write(InputStream fileinputstream, String fileoutputstream) {

		InputStream inputStream = null;
		OutputStream outputStream = null;
		String result = "";

		try {
			// read this file into InputStream
//			inputStream = new FileInputStream(fileinputstream);
			inputStream = fileinputstream;

			// write the inputStream to a FileOutputStream
			outputStream = new FileOutputStream(new File(fileoutputstream));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			result = "Done";

		} catch (IOException e) {
			e.printStackTrace();
			result = "Error Write File";
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		
		return result;
	}
	
	public static String Delete(String filestream)
    {
		String result = "";
    	try{

    		File file = new File(filestream);

    		if(file.delete()){
    			result = file.getName() + " is deleted!";
    		}else{
    			result = "Delete operation is failed.";
    		}

    	}catch(Exception e){

    		e.printStackTrace();
    		result = "Error Delete File";
    	}

    	return result;
    }
	
}
