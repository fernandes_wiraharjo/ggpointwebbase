package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;

import com.sun.jersey.core.util.StringIgnoreCaseKeyComparator;

import helper.ConvertDateTimeHelper;
import model.Globals;
import model.mdlFeedback;
import model.mdlMerchant;
import model.mdlMerchantInbox;
import model.mdlUser;

public class FeedbackAdapter {

	public static List<model.mdlFeedback> LoadFeedback(String lBrandID, mdlUser pUser) {
		
		//define local variable
		List<model.mdlFeedback> lmdlFeedbackList = new ArrayList<model.mdlFeedback>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadFeedback(?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert parameter
			pstm.setString(1, "'"+lBrandID+"'");
			pstm.setString(2, pUser.Area);
			command = pstm.toString();
			pstm.addBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlFeedback lmdlFeedback = new model.mdlFeedback();
				
				lmdlFeedback.setDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"),"yyyy-MM-dd HH:mm:ss","dd-MM-yyyy HH:mm"));
				lmdlFeedback.setDescription(jrs.getString("Description"));
				lmdlFeedback.setFeedbackID(jrs.getString("FeedbackID"));
				lmdlFeedback.setFile(jrs.getString("file"));
				lmdlFeedback.setMerchantID(jrs.getString("MerchantID"));
				lmdlFeedback.setMerchantName(jrs.getString("MerchantName"));
				lmdlFeedback.setStatus(jrs.getString("Status"));
				lmdlFeedback.setType(jrs.getString("TypeName"));
				lmdlFeedback.setCreated_by(jrs.getString("Created_by"));
				lmdlFeedback.setCreatedDate(jrs.getString("createdDate"));
				lmdlFeedback.setUsername(jrs.getString("Username"));
				lmdlFeedback.setPIC(jrs.getString("PIC"));
				lmdlFeedback.setLastDate(jrs.getString("lastDate"));
				lmdlFeedback.setLastUpdateBy(jrs.getString("lastUpdateBy"));
				
				lmdlFeedbackList.add(lmdlFeedback);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadFeedback", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadFeedback", "close opened connection protocol", pUser.UserId);
			}
		}

		return lmdlFeedbackList;
	}

	public static String UpdateFeedback(mdlFeedback lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		//call store procedure
		String sql = "{call wb_UpdateFeedback(?,?,?,?)}";	
		pstm = connection.prepareStatement(sql);

		
		//insert sql parameter
		pstm.setString(1,  lParam.getContestReason());
		pstm.setString(2, user);
		pstm.setString(3, LocalDateTime.now().toString());
		pstm.setString(4,  lParam.getFeedbackID());
		
		command = pstm.toString();
		pstm.addBatch();
		pstm.executeBatch();
		
		result = "Success Close Feedback";
		}
		catch(Exception ex) {			
			LogAdapter.InsertLogExc(ex.toString(), "UpdateFeedback", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 connection.setAutoCommit(true);
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "UpdateFeedback", "close opened connection protocol", user);
			}
		}
		
		return result;
	}
	
	public static Integer LoadFeedbackNumber(String[] listBrandID, String[] listAreaID, String user) {
		
		//define local variable
		int FeedbackNumber = 0;
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		
		StringBuilder builderBrand = new StringBuilder();
		StringBuilder builderArea = new StringBuilder();
		int indexParam = 1;
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//set the number of parameter base on the number of brand list and area list
			for(int i=0; i<listBrandID.length; i++) {
				builderBrand.append("?,");
			}
			for(int j=0; j<listAreaID.length; j++) {
				builderArea.append("?,");
			}
			
			String sql = "SELECT a.FeedbackID FROM tx_feedback a "
					+ "INNER JOIN ms_merchant b ON b.MerchantID = a.MerchantID "
					+ "WHERE a.Status='ISSUED' AND b.BrandID IN ("+builderBrand.deleteCharAt( builderBrand.length() -1 ).toString()+ ") " 
					+ "AND b.AreaID IN ("+builderArea.deleteCharAt( builderArea.length() -1 ).toString()+ ")";
			pstm = connection.prepareStatement(sql);
			
			//set the value of parameter
			for(int x=0; x<listBrandID.length; x++) {
				pstm.setString(indexParam++, listBrandID[x]);
			}
			for(int y=0; y<listAreaID.length; y++) {
				pstm.setString(indexParam++, listAreaID[y]);
			}
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			command = pstm.toString();
			
			while(jrs.next()){
				FeedbackNumber++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadFeedbackNumber", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadFeedbackNumber", "close opened connection protocol", user);
			}
		}

		return FeedbackNumber;
	}

}
