package adapter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import model.Globals;
import model.mdlUser;

public class UserAdapter {
	
	public static List<model.mdlUser> LoadUser(mdlUser pUser){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlUser> mdlUserList = new ArrayList<model.mdlUser>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadUser(?,?)}");
			
			//insert sql parameter
			pstm.setString(1,  pUser.Area);
			pstm.setString(2,  pUser.Brand);
			pstm.addBatch();
			command = pstm.toString();
			pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlUser mdlUser = new model.mdlUser();				
				mdlUser.setUserId(rs.getString("UserId"));
				mdlUser.setUsername(rs.getString("Username"));
				mdlUser.setPassword(rs.getString("Password"));
				mdlUser.setArea(rs.getString("AreaId"));
				mdlUser.setRole(rs.getString("RoleId"));
				mdlUser.setBrand(rs.getString("BrandId"));
				mdlUser.setType(rs.getString("Type"));
				mdlUserList.add(mdlUser);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUser", command, pUser.UserId);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadUser", "close opened connection protocol", pUser.UserId);
			}
		}

		return mdlUserList;
	}
	
	public static String InsertUser(model.mdlUser lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmCheck = null;
		ResultSet rs = null;
		ResultSet rsCheck = null;
		Boolean check = false;
		String command = "";
		String result = "";
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//check duplicate user id
			String sqlCheck = "{call wb_LoadUserById(?)}";
			pstmCheck = connection.prepareStatement(sqlCheck);
			
			//insert sql parameter and execute for check
			pstmCheck.setString(1,  lParam.getUserId());
			rsCheck = pstmCheck.executeQuery();
			command = pstmCheck.toString();
			
			while(rsCheck.next()){
				check = true;
			}
				//if no duplicate
				if(check==false)
				{
					pstm = connection.prepareStatement("{call wb_InsertUser(?,?,?,?,?,?,?,?,?)}");
					pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getUserId()));
					pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getUsername()));
					pstm.setString(3, ValidateNull.NulltoStringEmpty(Base64Adapter.EncriptBase64(lParam.getPassword())));
					pstm.setString(4, ValidateNull.NulltoStringEmpty(lParam.getArea()));
					pstm.setString(5, ValidateNull.NulltoStringEmpty(lParam.getRole()));
					pstm.setString(6, ValidateNull.NulltoStringEmpty(lParam.getBrand()));
					pstm.setString(7, ValidateNull.NulltoStringEmpty(lParam.getType()));
					pstm.setString(8, user);
					pstm.setString(9, "");

					command = pstm.toString();
					rs = pstm.executeQuery();
					result = "SuccessInsert";
				}
				//if duplicate
				else {
					result = "The User Id Already Exist";
				}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "InsertUser", command, user);
			result = "Failed Insert";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (pstmCheck != null) {
					 pstmCheck.close();
				 }
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				 if (rsCheck != null) {
					 rsCheck.close();
				 }
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "InsertUser", "close opened connection protocol", user);
			}
		}
		return result;
	}
	
	public static String UpdateUser(model.mdlUser lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_UpdateUser(?,?,?,?,?,?,?,?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lParam.getUserId()));
			pstm.setString(2, ValidateNull.NulltoStringEmpty(lParam.getUsername()));
			pstm.setString(3, ValidateNull.NulltoStringEmpty(Base64Adapter.EncriptBase64(lParam.getPassword())));
			pstm.setString(4, ValidateNull.NulltoStringEmpty(lParam.getArea()));
			pstm.setString(5, ValidateNull.NulltoStringEmpty(lParam.getRole()));
			pstm.setString(6, ValidateNull.NulltoStringEmpty(lParam.getBrand()));
			pstm.setString(7, ValidateNull.NulltoStringEmpty(lParam.getType()));
			pstm.setString(8, user);
		
			command = pstm.toString();
			rs = pstm.executeQuery();
			result = "SuccessUpdate";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "UpdateUser", command, user);
			result = "FailedUpdate";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "UpdateUser", "close opened connection protocol", user);
			}
		}
		return result;	
	}

	public static String DeleteUser(String lUserId, String userAccess){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_DeleteUser(?)}");
			pstm.setString(1, ValidateNull.NulltoStringEmpty(lUserId));
			
			result = "SuccessDelete";
			command = pstm.toString();
			rs = pstm.executeQuery();	
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteUser", command, userAccess);
			result = "FailedDelete";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteUser", "close opened connection protocol", userAccess);
			}
		}
		
		return result;	
	}

	public static List<model.mdlMenu> LoadRestrictedMenu(String lUserID){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlMenu> mdlMenuList = new ArrayList<model.mdlMenu>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadRestrictedMenu(?)}");
			
			//insert sql parameter
			pstm.setString(1,  lUserID);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlMenu mdlMenu = new model.mdlMenu();				
				mdlMenu.setMenuID(rs.getString("MenuID"));
				mdlMenuList.add(mdlMenu);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadRestrictedMenu", command, lUserID);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadRestrictedMenu", "close opened connection protocol", lUserID);
			}
		}

		return mdlMenuList;
	}
	
	public static String LoadUserArea(String lUserID){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String AreaID = "";
//		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadUserById(?)}");
			
			//insert sql parameter
			pstm.setString(1,  lUserID);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				AreaID = rs.getString("AreaId");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserArea", command, lUserID);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadUserArea", "close opened connection protocol", lUserID);
			}
		}

		return AreaID;
	}
	
	public static String LoadUserBrand(String lUserID){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String BrandID = "";
//		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadUserById(?)}");
			
			//insert sql parameter
			pstm.setString(1,  lUserID);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				BrandID = rs.getString("BrandId");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserBrand", command, lUserID);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadUserBrand", "close opened connection protocol", lUserID);
			}
		}

		return BrandID;
	}

	public static String LoadUserId(String lUsername, String lPassword){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String UserID = "";
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadUserByLogin(?,?)}");
			
			//insert sql parameter
			pstm.setString(1,  lUsername);
			pstm.setString(2,  lPasswordEnc);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				UserID = rs.getString("UserId");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserId", command, lUsername);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadUserId", "close opened connection protocol", lUsername);
			}
		}

		return UserID;
	}
	
	public static String LoadUserType(String lUsername, String lPassword){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String UserType = "";
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadUserByLogin(?,?)}");
			
			//insert sql parameter
			pstm.setString(1,  lUsername);
			pstm.setString(2,  lPasswordEnc);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				UserType = rs.getString("Type");
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadUserType", command, lUsername);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadUserType", "close opened connection protocol", lUsername);
			}
		}

		return UserType;
	}
	
	public static model.mdlUser GetUserAreaAndBrand(String user) {
		model.mdlUser mdlUser = new model.mdlUser();
        
        //setting session for user area access
		String user_area = UserAdapter.LoadUserArea(user);
		if(user_area.contentEquals("") || user_area==null)
		{
		}
		String[] list_user_area = user_area.split(",");
		int list_user_area_length = list_user_area.length; //set the length of list
		for(int i=0; i<list_user_area_length;i++)
		{
			if(mdlUser.Area == null || mdlUser.Area.contentEquals("")) //if first index
				mdlUser.Area = "'"+list_user_area[i]+"'";
			else
				mdlUser.Area += ","+"'"+list_user_area[i]+"'";
		}
		
		//setting session for user brand access
		String user_brand = UserAdapter.LoadUserBrand(user);
		if(user_brand.contentEquals("") || user_brand==null)
		{
		}
		String[] list_user_brand = user_brand.split(",");
		int list_user_brand_length = list_user_brand.length; //set the length of list
		for(int i=0; i<list_user_brand_length;i++)
		{
			if(mdlUser.Brand == null || mdlUser.Brand.contentEquals("")) //if first index
				mdlUser.Brand = "'"+list_user_brand[i]+"'";
			else
				mdlUser.Brand += ","+"'"+list_user_brand[i]+"'";
		}
		
		return mdlUser;
	}
	
	public static model.mdlUser GetUserAreaAndBrand2(String user) {
		model.mdlUser mdlUser = new model.mdlUser();
        
        //setting session for user area access
		String user_area = UserAdapter.LoadUserArea(user);
		if(user_area.contentEquals("") || user_area==null)
		{
		}
		String[] list_user_area = user_area.split(",");
		int list_user_area_length = list_user_area.length; //set the length of list
		for(int i=0; i<list_user_area_length;i++)
		{
			if(mdlUser.Area == null || mdlUser.Area.contentEquals("")) //if first index
				mdlUser.Area = "'"+list_user_area[i]+"'";
			else
				mdlUser.Area += " OR AreaId LIKE "+"'%"+list_user_area[i]+"%'";
		}
		
		//setting session for user brand access
		String user_brand = UserAdapter.LoadUserBrand(user);
		if(user_brand.contentEquals("") || user_brand==null)
		{
		}
		String[] list_user_brand = user_brand.split(",");
		int list_user_brand_length = list_user_brand.length; //set the length of list
		for(int i=0; i<list_user_brand_length;i++)
		{
			if(mdlUser.Brand == null || mdlUser.Brand.contentEquals("")) //if first index
				mdlUser.Brand = "'"+list_user_brand[i]+"'";
			else
				mdlUser.Brand += " OR BrandId LIKE "+"'%"+list_user_brand[i]+"%'";
		}
		
		return mdlUser;
	}
	
}
